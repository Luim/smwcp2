; "special" coin
;Actually made by Ersanio. Only Barely edited by me.

incsrc CoinDefines.asm

db $42

JMP MarioAbove : JMP MarioAbove : JMP MarioAbove : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return

MarioAbove:	PHY
		PHB
		PHK
		PLB

		LDY.b #$06

MakingLoop:	REP #$10
		LDX TileNumber,y
		%change_map16()
		SEP #$10
		REP #$20
		LDA $98
		SEC
		SBC TilePositionY-$02,y
		STA $98
		LDA $9A
		SEC
		SBC TilePositionX-$02,y
		STA $9A

		DEY
		DEY
		BPL MakingLoop
		SEP #$20

NotUp:		PLB
		PLY

LDA !SFX		; Play desired Sound
STA !IObank		;



LDA.l !TempCounter+1
ORA #$01
STA.l !TempCounter+1


Return:		RTL

TilePositionX:		dw $0010,$FFF0,$0010
TilePositionY:		dw $0000,$0010,$0000
TileNumber:		dw $133C,$133D,$133E,$133F