;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Trapdoor Block
; - If the sprite's number is the skull platform/close trapdoor.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
db $42
JMP MarioBelow : JMP MarioAbove : JMP MarioSide : JMP SpriteV : JMP SpriteH : JMP MarioCape : JMP MarioFireBall : JMP DoNothing : JMP DoNothing : JMP DoNothing

SpriteV:
SpriteH:
	LDA $009E,x	;Get the current sprite's sprite number
	CMP #$61	;is a part of the skull platform
	BNE DoNothing
	LDA $14AF	;Prevents the sound effect from doubling up.
	BNE DoNothing
	LDA #$01
	STA $14AF	;Turn Off Gates
	LDA #$19
	STA $1DFC
MarioAbove:
MarioBelow:
MarioSide:
MarioCape:
MarioFireBall:
DoNothing:	
	RTL		;Done!