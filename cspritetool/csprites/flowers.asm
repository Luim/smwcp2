;-----------------------------------------------------------------------;
; Cluster Flower Petals - by Ladida					;
; Can be anything, like petals, leaves, snow; just change the graphics. ;
; Edit of Roy's original Spike Hell sprite.				;
;-----------------------------------------------------------------------;

!FlowerTile = $67	;Tile # of the flower petal. was $67

!FlowerSize = $00	;Size of flower petal. 8x8 by default

!FlowerPalPage = $7A	;YXPPCCCT of tile. XY flip handled by Properties table.;was 35, 3F


SpeedTableY:
db $01,$02,$01,$02,$01,$02,$01,$02,$01,$02,$01,$02,$01,$02,$01,$02,$01,$02,$01,$02 ; Speed table, per sprite. Amount of pixels to move down each frame. 00 = still, 80-FF = rise, 01-7F = sink.

SpeedTableX:
db $FD,$FF,$FE,$FF,$FD,$FF,$FE,$FF,$FD,$FF,$FE,$FF,$FD,$FF,$FE,$FF,$FD,$FF,$FE,$FF ; Speed table, per sprite. Amount of pixels to move down each frame. 00 = still, 80-FF = rise, 01-7F = sink.

SpeedTableXTwo:
db $FF,$FE,$FD,$FE,$FF,$FE,$FD,$FE,$FF,$FE,$FD,$FE,$FF,$FE,$FD,$FE,$FF,$FE,$FD,$FE ; Speed table, per sprite. Amount of pixels to move down each frame. 00 = still, 80-FF = rise, 01-7F = sink.

OAMStuff:
db $B0,$B4,$B8,$BC,$C0,$C4,$C8,$CC,$D0,$D4,$D8,$DC,$80,$84,$88,$8C,$40,$44,$48,$4C ; These are all in $02xx

Properties:
db $00,$40,$80,$C0,$00,$40,$80,$C0,$00,$40,$80,$C0,$00,$40,$80,$C0,$00,$40,$80,$C0 ; Properties table, per sprite. YXPPCCCT.

PropertiesTwo:
db $80,$C0,$00,$40,$80,$C0,$00,$40,$80,$C0,$00,$40,$80,$C0,$00,$40,$80,$C0,$00,$40 ; Properties table, per sprite. YXPPCCCT.

IncrementByOne:
LDA $1E02,y                     ; \ Increment Y position of sprite.
INC A                           ;  |
STA $1E02,y                     ;  |
SEC                             ;  | Check Y position relative to screen border Y position.
SBC $1C                         ;  | If equal to #$F0...
CMP #$F0                        ;  |
BNE ReturnAndSuch               ;  |
LDA #$01                        ;  | Appear.
STA $1E2A,y                     ; /

ReturnAndSuch:
RTS

Main:				;The code always starts at this label in all sprites.
LDA $1E2A,y                     ; \ If meant to appear, skip sprite intro code.
BEQ IncrementByOne              ; /

LDA $9D				; \ Don't move if sprites are supposed to be frozen.
BNE Immobile			; /
LDA $14
AND #$01
BEQ +
LDA $1E02,y                     ; \
CLC				;  |
ADC SpeedTableY,y               ;  | Movement.
STA $1E02,y                     ; /
+

LDA $14
LSR #5
AND #$01
BEQ +
LDA $1E16,y
CLC
ADC SpeedTableX,y
BRA ++
+
LDA $1E16,y
CLC
ADC SpeedTableXTwo,y
++
STA $1E16,y

Immobile:                       ; OAM routine starts here.
LDX.w OAMStuff,y 		; Get OAM index.
LDA $1E02,y			; \ Copy Y position relative to screen Y to OAM Y.
SEC                             ;  |
SBC $1C				;  |
STA $0201,x			; /
LDA $1E16,y			; \ Copy X position relative to screen X to OAM X.
SEC				;  |
SBC $1A				;  |
STA $0200,x			; /
LDA #!FlowerTile		; \ Tile
STA $0202,x                     ; /
LDA $14
LSR #2
AND #$01
BEQ +
LDA Properties,y
ORA #!FlowerPalPage
BRA ++
+
LDA PropertiesTwo,y
ORA #!FlowerPalPage
++
STA $0203,x
PHX
TXA
LSR
LSR
TAX
LDA #!FlowerSize
STA $0420,x
PLX
LDA $18BF
ORA $1493
BEQ ReturnToTheChocolateWhatever            	; Change BEQ to BRA if you don't want it to disappear at generator 2, sprite D2.
LDA $0201,x
CMP #$F0                                    	; As soon as the sprite is off-screen...
BCC ReturnToTheChocolateWhatever
LDA #$00					; Kill it.
STA $1892,y					;

ReturnToTheChocolateWhatever:
RTS
