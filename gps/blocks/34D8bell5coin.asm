db $42
JMP Main : JMP Main : JMP Main : JMP Return : JMP Return : JMP Return : JMP Return : JMP Main : JMP Main : JMP Main

Main:
	LDA #$05
	STA $13CC
	LDA #$01
	STA $1DFC

	PHY
	LDY #$03
	LDA $7F                   
        ORA $81                   
        BNE NoGlitter
        LDY #$03
	
LoopStart:
        LDA $17C0,y
        BEQ Go
	DEY
	BPL LoopStart
	BRA NoGlitter
	
Go:
	LDA #$05
        STA $17C0,y
        LDA $9A
        AND #$F0
        STA $17C8,y
        LDA $98
        AND #$F0
        STA $17C4,y
        LDA $1933
        BEQ Layer1
        LDA $9A
        SEC
        SBC $26
        AND #$F0
        STA $17C8,y
        LDA $98
        SEC
        SBC $28
        AND #$F0
        STA $17C4,y
Layer1:
        LDA #$10
        STA $17CC,y
	
NoGlitter:
	LDA #$02
	STA $9C
	JSL $00BEB0
	
	REP #$20
	LDA $98
	SEC
	SBC #$10
	STA $98
	SEP #$20
	
	LDA #$02
	STA $9C
	JSL $00BEB0
	PLY
	
Return:
	RTL