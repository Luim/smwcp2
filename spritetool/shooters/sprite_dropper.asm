;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Custom Shooter 4, based on the Bullet Bill Shooter disassembly by mikeyk and Magus code, 
;; further adapted into Sprite Tool by Davros
;;
;; Description: This will generate a custom sprite without smoke and will keep shooting.
;; Specify the actual sprite and sound that is generated below.
;;
;; NOTE: Trying to generate a sprite that doesn't exist will crash your game.
;;
;; Uses first extra bit: NO
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			!RAM_ScreenBndryXLo	= $1A
			!RAM_ScreenBndryXHi	= $1B
			!RAM_ScreenBndryYLo	= $1C
			!RAM_ScreenBndryYHi	= $1D
			!RAM_MarioXPos		= $94
			!RAM_MarioXPosHi	= $95
			!RAM_SpriteNum		= $9E
			!RAM_SpriteYLo		= $D8
			!RAM_SpriteXLo		= $E4
			!RAM_SpriteYHi		= $14D4
			!RAM_SpriteXHi		= $14E0
			!RAM_ShooterYLo		= $178B
			!RAM_ShooterYHi		= $1793
			!RAM_ShooterXLo		= $179B
			!RAM_ShooterXHi		= $17A3
			!RAM_ShooterXLo		= $179B
			!RAM_ShooterTimer	= $17AB

			!RAM_CustSpriteNum	= $7FAB9E
			!RAM_ExtraBits		= $7FAB10

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
			print "INIT ",pc
			print "MAIN ",pc
			PHB
			PHK
			PLB
			JSR MainCode
			PLB
			RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main bullet bill shooter code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;               

Return0:		RTS

MainCode:		LDA !RAM_ShooterTimer,x
			BNE Return0

			;LDA !RAM_ShooterYLo,x		;\ don't generate if off screen vertically
			;CMP !RAM_ScreenBndryYLo		; |
			;LDA !RAM_ShooterYHi,x		; |
			;SBC !RAM_ScreenBndryYHi		; |
			;BNE Return0			;/
			LDA !RAM_ShooterXLo,x		;\ don't generate if off screen horizontally
			CMP !RAM_ScreenBndryXLo		; |
			LDA !RAM_ShooterXHi,x		; |
			SBC !RAM_ScreenBndryXHi		; |
			BNE Return0			;/ 
			LDA !RAM_ShooterXLo,x		;\ ?? something else related to x position of generator??
			SEC				; | 
			SBC !RAM_ScreenBndryXLo		; |
			CLC				; |
			ADC #$10			; |
			CMP #$10			; |
			BCC Return0			;/
			LDA !RAM_MarioXPos		;\ don't fire if mario is next to generator
			SBC !RAM_ShooterXLo,x		; |
			CLC				; |
			ADC #$11			; |
			CMP #$22			; |
			BCC Return0			;/
			JSL $02A9DE			;\ get an index to an unused sprite slot, return if all slots full
			BMI Return0			;/ after: Y has index of sprite being generated

GenerateSprite:		LDA #$01			;\ set sprite status for new sprite
			STA $14C8,y			;/
			LDA $1783,x
			AND #$40
			BEQ SpawnBall
			LDA !RAM_ShooterXLo,x
			AND #$10
			BNE SpawnBomb
			
SpawnMechakoopa:
			LDA #$A2
			STA.w !RAM_SpriteNum,y
			PHX
			TYX
			JSL $07F7D2
			BRA Continue
			
SpawnBall:	PHX
			TYX
			LDA #$3A			;\ set number of new sprite
			STA !RAM_CustSpriteNum,x	;/
			JSL $07F7D2			; reset sprite properties
			JSL $0187A7			; get table values for custom sprite       
			LDA #$8C			; mark as initialized
			STA !RAM_ExtraBits,x
			BRA Continue

SpawnBomb:	PHX
			TYX
			LDA #$45			;\ set number of new sprite
			STA !RAM_CustSpriteNum,x	;/
			JSL $07F7D2			; reset sprite properties
			JSL $0187A7			; get table values for custom sprite       
			LDA #$88			; mark as initialized
			STA !RAM_ExtraBits,x
			LDA #$02
			STA $1510,x
			LDA #$68
			STA $187B,x

Continue:		PLX
			LDA !RAM_ShooterXLo,x		;\ set x position for new sprite
			STA.w !RAM_SpriteXLo,y		; |
			LDA !RAM_ShooterXHi,x		; |
			STA !RAM_SpriteXHi,y		;/
			LDA !RAM_ShooterYLo,x		;\ set y position for new sprite
			STA.w !RAM_SpriteYLo,y		; |
			LDA !RAM_ShooterYHi,x		; |
			STA !RAM_SpriteYHi,y		;/

			LDA #$60			;\ set time until next firing
			STA !RAM_ShooterTimer,x		;/

			LDA #$2B			;\ play sound effect
			STA $1DFC			;/


Return:			RTS


