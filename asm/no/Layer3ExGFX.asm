; This patch requires you insert GFX or ExGFX (anything) with Lunar Magic before you patch it.
; Lunar Magic will install an ASM hack that this patch hijacks
; if you don't do that first, this patch will do nothing.

; This patch also makes the FG1-4 OW GFX reload on submap switch via exit tile, along with the layer 3 GFX
; (SMW didn't originally do this)

; If you experience split-second graphical glitches on mid-submap-transition between certain submaps that
; have different border or font GFX, enable FBLANK for both a->b and b->a in the table UseFBlankSubmap2Submap.
; This will cause the screen to turn black for a split second while it uploads the layer 3 GFX.

; IF YOU HAVE ALREADY PATCHED A PREVIOUS VERSION OF LAYER 3 EXGFX, BE SURE TO APPLY "REVERT.asm" TO YOUR ROM BEFORE PATCHING THIS!!!


header
lorom

;========================
; DEFINES
;========================

!CodeFreeSpace = $2F8000 ; Freespace here 

!LastSubmap = $0F3E	 ; used to figure out if necessary to flicker screen

;========================
; HIJACKS
;========================

org $009471
autoclean JSL Label0
NOP

org $00A0B3
autoclean JSL Label1
NOP
NOP

org $04859B
dl Label3

org $008229
autoclean JSL S2SNMI

org $0FF780
autoclean JML HIJACK_STUFF
NOP

;==========================
; FROM LEVELASM, NOT BY ME
;==========================

;ORG $05D8B9  			REMOVED BY LUI37
;	JSR Levelnummain
;
;ORG $05DC46
;Levelnummain:	LDA $0E  		;Load level number
;		STA $010B		;Store it in free stack RAM
;		ASL A    		;Multiply A by 2, probably to recover old code?
;		RTS      		;Return from subroutine

;-----------------------
; CUSTOM CODE
;-----------------------

org !CodeFreeSpace
freecode
reset bytes



HIJACK_STUFF:

	PHB
	PHX
	PHY		; Preserve nearly everything
	PHP
	PHK
	PLB

	LDA.w $0100	; don't execute if not loading level
	CMP.b #$04
	BEQ InLevel
	CMP.b #$12
	BNE NotInLevel

InLevel:
	JSR Label2	; run code

	SEP #$20
	LDA #$0F
	PHA
	PLB

NotInLevel:

	PLP
	PLY
	PLX
	PLB

	PEA $F783
	LDA #$0C
	JML $8FF840	; return

;-----------------------;
; OW Layer 3 ExGFX Rt   ;
;-----------------------;
; Submap -> Submap      ;
;-----------------------;

Label3:
SEP #$30		;original routine located at $04DAEF
LDA $1DE8
JSL $8086FA		;ExecutePtrLong   

dl $04DB18
dl $04DCB6
dl $04DCB6
dl $04DCB6
dl $04DCB6
dl ExGraFix		;FG1 1st half
dl Wait			;FG1 2nd half
dl ExGraFix		;FG2 1st half
dl Wait			;FG2 2nd half
dl ExGraFix		;FG3 1st half
dl Wait			;FG3 2nd half
dl ExGraFix		;FG4 1st half
dl Wait			;FG4 2nd half
dl ExGraFix		;L3-1
dl ExGraFix		;L3-2
dl ExGraFix		;L3-3
dl ExGraFix		;L3-4
dl $04DB9D
dl $04DB18
dl $04DBCF

ExGraFix:		;executes when $1DE8 equals $05-$10
INC $1DE8		;next submap-change mode
STZ $00			; \
LDA #$EC		;  | destination
STA $01			;  | of data
LDA #$7E		;  |
STA $02			; /
LDA $1DE8		;$06-$0D (because it was INC'd)
CMP #$0E		;if $0E-$11, then do layer 3 GFX
BCS Layer3S2S		;else stay and do FG1-FG4 ($06-$0D)
LDA $1DE8		; \ only upload GFX file once to RAM
AND #$01		;  |
BNE ReturnB04		; /
LDX $0DB3
LDA $1F11,x
ASL #3
CLC 
ADC $1DE8
SEC
SBC #$06
LSR A
TAX
LDA $00A96F,x		;get ExGFX file number
REP #$30
AND #$00FF
PHA
JSL $8FF900		;decompress
PLA
CMP #$0008		;GFX08 gets special treatment...
BEQ GFX08GFX1EFix
CMP #$001E		;...so does GFX1E
BEQ GFX08GFX1EFix
SEP #$30
ReturnB04:
JML $84DB9C		;a RTS in bank 04

Layer3S2S:
LDA $1DE8
CMP #$0E		;only make FBlank-or-not decision once
BNE ContinueL3S2S
LDX $0DB3
LDA $1F11,x
ASL #3
CLC
ADC !LastSubmap
TAX
LDA UseFBlankSubmap2Submap,x
BEQ NoFBlank
JMP UpdateL3WithFBlank
NoFBlank:
LDX $0DB3
LDA $1F11,x
STA !LastSubmap
ContinueL3S2S:
LDA $1DE8
SEC
SBC #$0E
ASL #3
LDX $0DB3
CLC
ADC $1F11,x
ASL A
TAX
REP #$30
LDA OWExGFX,x		;get ExGFX file number
 JSL $8FF900		;decompress
SEP #$30
 JML $84DB9C		;a RTS in bank 04

Wait:
INC $1DE8		;next submap-change mode
JML $84DB9C		;a RTS in bank 04

GFX08GFX1EFix:		;this forces GFX file to use the second half of the palette
LDX #$0FE0
GFX08GFX1ELoop:
SEP #$20
LDA $7EEC00,x
ORA $7EEC01,x
ORA $7EEC10,x
ORA $7EEC11,x
STA $7EEC11,x
LDA $7EEC02,x
ORA $7EEC03,x
ORA $7EEC12,x
ORA $7EEC13,x
STA $7EEC13,x
LDA $7EEC04,x
ORA $7EEC05,x
ORA $7EEC14,x
ORA $7EEC15,x
STA $7EEC15,x
LDA $7EEC06,x
ORA $7EEC07,x
ORA $7EEC16,x
ORA $7EEC17,x
STA $7EEC17,x
LDA $7EEC08,x
ORA $7EEC09,x
ORA $7EEC18,x
ORA $7EEC19,x
STA $7EEC19,x
LDA $7EEC0A,x
ORA $7EEC0B,x
ORA $7EEC1A,x
ORA $7EEC1B,x
STA $7EEC1B,x
LDA $7EEC0C,x
ORA $7EEC0D,x
ORA $7EEC1C,x
ORA $7EEC1D,x
STA $7EEC1D,x
LDA $7EEC0E,x
ORA $7EEC0F,x
ORA $7EEC1E,x
ORA $7EEC1F,x
STA $7EEC1F,x
REP #$20
TXA
SEC
SBC #$0020
TAX
BMI GFX08GFX1EFinish
JMP GFX08GFX1ELoop
GFX08GFX1EFinish:
SEP #$30
JML $84DB9C

UpdateL3WithFBlank:
STZ $4200
STZ $420C
LDA #$80
STA $2100
JSL ReturnA
LDA #$81
STA $4200
LDA $1DE8
CLC
ADC #$03
STA $1DE8
JML $84DBD9

;-----------------------;
; Submap -> Submap NMI  ;
;-----------------------;

VRAMTable:		db $00,$04,$08,$0C,$10,$14,$18,$1C,$40,$44,$48,$4C
SrcTable:		db $EC,$F4,$EC,$F4,$EC,$F4,$EC,$F4,$EC,$EC,$EC,$EC

S2SNMI:
LDY $1DE8		;1st part of hijacked code
CPY #$06
BCC FinishS2S
CPY #$12
BCC S2SUpload

FinishS2S:
DEY			;2nd part of hijacked code
RTL

S2SUpload:
PHB
PHK
PLB
LDA #$80
STA $2115
STZ $2116
LDA VRAMTable-6,y
STA $2117
LDA #$01
STA $4320
LDA #$18
STA $4321
STZ $4322
LDA SrcTable-6,y
STA $4323
LDA #$7E
STA $4324
STZ $4325
LDA #$08
STA $4326
LDA #$04
STA $420B
PLB
LDY $1DE8		;1st part of hijacked code
BRA FinishS2S

;-------------------;
; Level -> Submap   ;
;-------------------;

Label1:
PHK
PER Return1-$01
PHB
LDA #$00
PHA
PLB
PEA $84CD
JML $808A79

Return1:
PHK
PER ReturnA-$01
PHB
LDA #$00
PHA
PLB
PEA $84CD
JML $8084C7

ReturnA:
LDY $0DB3
LDA $1F11,y
STA !LastSubmap
JSR LoadNormalSlots
PHB
PHK
PLB
LDA #$FC
STA $03
LDA #$F0
STA $04

LoopUntilDone2:
LDA $04
CLC
ADC #$10
STA $04
LDA $03
CLC
ADC #$04
STA $03
LDY $0DB3
LDA $1F11,y
ASL A
CLC
ADC $04
TAY
REP #$30
LDX #$0000
LDA.w OWExGFX,y
JSR TheGeneralRoutine
SEP #$30
CMP #$30
BCC LoopUntilDone2
PLB
RTL

DMATable2:
db $01,$18,$00,$B9,$7E,$00,$08

;----------------------;
; General GFX Decomprs ;
;----------------------;

TheGeneralRoutine:
PHP
REP #$20
PHA
STZ $00
LDA #$7EB9
STA $01
PLA
CMP #$007F
BEQ UploadReturn
JSL $8FF900
SEP #$30
LDA #$80
STA $2115
LDA #$40
CLC
ADC $03,x
STA $2117
STZ $2116
LDX #$06

OWLoop:
LDA.l DMATable2,x
STA $4310,x
DEX
BPL OWLoop
LDA #$02
STA $420B
LDA $04

UploadReturn:
PLP
RTS

;----------------------;
; OW L3 ExGFX, slot 1  ;
;----------------------;

OWExGFX:
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Overworld, Yoshi's Island, Vanilla Dome, Forest of Illusion, Valley of Bowser, Special World, Star World

;----------------------;
; OW L3 ExGFX, slot 2  ;
;----------------------;

dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Overworld, Yoshi's Island, Vanilla Dome, Forest of Illusion, Valley of Bowser, Special World, Star World

;----------------------;
; OW L3 ExGFX, slot 3  ;
;----------------------;

dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Overworld, Yoshi's Island, Vanilla Dome, Forest of Illusion, Valley of Bowser, Special World, Star World

;----------------------;
; OW L3 ExGFX, slot 4  ;
;----------------------;

dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Overworld, Yoshi's Island, Vanilla Dome, Forest of Illusion, Valley of Bowser, Special World, Star World

;-------------------------------;
; Flash Screen Between Submaps  ;
;-------------------------------;

UseFBlankSubmap2Submap:
db $00,$00,$00,$00,$00,$00,$00,$00	; Overworld	->	OW, YI, VD, FoI, VoB, Sp, Star
db $00,$00,$00,$00,$00,$00,$00,$00	; Yoshi's	->	OW, YI, VD, FoI, VoB, Sp, Star
db $00,$00,$00,$00,$00,$00,$00,$00	; Vanilla	->	OW, YI, VD, FoI, VoB, Sp, Star
db $00,$00,$00,$00,$00,$00,$00,$00	; Forest	->	OW, YI, VD, FoI, VoB, Sp, Star
db $00,$00,$00,$00,$00,$00,$00,$00	; Valley	->	OW, YI, VD, FoI, VoB, Sp, Star
db $00,$00,$00,$00,$00,$00,$00,$00	; Special	->	OW, YI, VD, FoI, VoB, Sp, Star
db $00,$00,$00,$00,$00,$00,$00,$00	; StarWorld	->	OW, YI, VD, FoI, VoB, Sp, Star

;------------;
; Credit Fix ;
;------------;

Label0:
JSR LoadNormalSlots
LDX $13C6
LDA #$18
RTL

;-----------------------;
; Level Layer 3 ExGFX   ;
;-----------------------;

Label2:
PHB                    ; Preserve DBR.
PHK                    ; \ PBR into DBR.
PLB                    ; /
LDA #$FC
STA $04
STZ $03

LoopUntilDone:
LDA $04
CLC
ADC #$04
STA $04
REP #$30
LDA $010B              ; \ Index level
ASL A
CLC
ADC $03
TAY
LDA.w Layer3ExGFX,y    ;  | Get GFX file from table.
LDX #$0001
JSR TheGeneralRoutine
SEP #$30
CMP #$0C
BCC LoopUntilDone
PLB
RTS

;--------------------------------------------------;
; ExGFX for Layer 3 slot 1			   ;
; Replace with the (Ex)GFX file you want to use.   ;
;--------------------------------------------------;

Layer3ExGFX: ; GFX28
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 0-F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$009F ; Levels 10-1F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 20-2F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 30-3F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 40-4F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 50-5F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 60-6F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 70-7F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 80-8F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 90-9F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels A0-AF
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels B0-BF
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels C0-CF
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels D0-DF
dw $0028,$0028,$0028,$0028,$0028,$00AA,$0028,$0028,$0028,$00AA,$0028,$0028,$0028,$0028,$00AA,$00AA ; Levels E0-EF
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels F0-FF
dw $0028,$0028,$0028,$00B5,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 100-10F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 110-11F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 120-12F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 130-13F
dw $0028,$0028,$0028,$0028,$0028,$00B5,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 140-14F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 150-15F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 160-16F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 170-17F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 180-18F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 190-19F
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 1A0-1AF
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 1B0-1BF
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 1C0-1CF
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 1D0-1DF
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 1E0-1EF
dw $0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028,$0028 ; Levels 1F0-1FF

;--------------------------------------------------;
; ExGFX for Layer 3 slot 2			   ;
; Replace with the (Ex)GFX file you want to use.   ;
;--------------------------------------------------;

Layer3ExGFX2: ; GFX29
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$05C3,$0029,$05C3,$0029,$0029,$05C3 ; Levels 0-F
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 10-1F
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 20-2F
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 30-3F
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 40-4F
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 50-5F
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$05C3,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 60-6F
dw $0029,$0029,$0029,$0029,$00A1,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$00A1,$0029,$0029,$0029 ; Levels 70-7F
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$00A1,$00A1,$00A1,$0029,$0029,$0029,$0029,$0029 ; Levels 80-8F
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 90-9F
dw $0029,$0029,$0029,$0029,$0029,$05C3,$05C3,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels A0-AF
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels B0-BF
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels C0-CF
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels D0-DF
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels E0-EF
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels F0-FF
dw $0029,$0029,$0029,$00B4,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 100-10F
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 110-11F
dw $0029,$0029,$0029,$00A1,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 120-12F
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 130-13F
dw $0029,$0029,$0029,$0029,$0029,$00B4,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 140-14F
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 150-15F
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 160-16F
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 170-17F
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 180-18F
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 190-19F
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 1A0-1AF
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 1B0-1BF
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 1C0-1CF
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 1D0-1DF
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 1E0-1EF
dw $0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029,$0029 ; Levels 1F0-1FF

;--------------------------------------------------;
; ExGFX for Layer 3 slot 3			   ;
; Replace with the (Ex)GFX file you want to use.   ;
;--------------------------------------------------;

Layer3ExGFX3: ; GFX2A
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 0-F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 10-1F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 20-2F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 30-3F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 40-4F
dw $002A,$002A,$002A,$002A,$00EA,$002A,$002A,$002A,$00EA,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 50-5F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 60-6F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$00EA,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 70-7F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 80-8F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 90-9F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels A0-AF
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels B0-BF
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels C0-CF
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels D0-DF
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels E0-EF
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels F0-FF
dw $002A,$002A,$002A,$00B4,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 100-10F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 110-11F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 120-12F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 130-13F
dw $002A,$002A,$002A,$002A,$00EA,$002A,$002A,$002A,$002A,$002A,$002A,$00EA,$002A,$002A,$002A,$002A ; Levels 140-14F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 150-15F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 160-16F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 170-17F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 180-18F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$00EA,$002A,$002A ; Levels 190-19F
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 1A0-1AF
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 1B0-1BF
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 1C0-1CF
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 1D0-1DF
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 1E0-1EF
dw $002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A,$002A ; Levels 1F0-1FF

;--------------------------------------------------;
; ExGFX for Layer 3 slot 4			   ;
; Replace with the (Ex)GFX file you want to use.   ;
;--------------------------------------------------;

Layer3ExGFX4: ; GFX2B
dw $002B,$002B,$0582,$0582,$002B,$002B,$0582,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 0-F
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 10-1F
dw $002B,$002B,$002B,$002B,$002B,$0582,$0582,$0582,$0582,$002B,$002B,$002B,$002B,$002B,$013B,$002B ; Levels 20-2F
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 30-3F
dw $002B,$002B,$002B,$002B,$002B,$002B,$05D4,$05D4,$05D4,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 40-4F
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 50-5F
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 60-6F
dw $05D7,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 70-7F
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 80-8F
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 90-9F
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels A0-AF
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels B0-BF
dw $002B,$002B,$002B,$002B,$002B,$0582,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels C0-CF
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels D0-DF
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels E0-EF
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels F0-FF
dw $002B,$002B,$002B,$00B5,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 100-10F
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$05D7,$002B,$002B,$002B ; Levels 110-11F
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$05D2,$002B,$002B,$01EC,$002B ; Levels 120-12F
dw $002B,$002B,$002B,$002B,$002B,$002B,$0582,$002B,$002B,$002B,$002B,$002B,$05D7,$05D7,$002B,$05D7 ; Levels 130-13F
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 140-14F
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 150-15F
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 160-16F
dw $002B,$01EC,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 170-17F
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 180-18F
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 190-19F
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$00E9,$00E9,$002B,$002B,$002B,$002B ; Levels 1A0-1AF
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 1B0-1BF
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 1C0-1CF
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$05D2,$002B,$05D2,$002B,$002B ; Levels 1D0-1DF
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 1E0-1EF
dw $002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B,$002B ; Levels 1F0-1FF

;-------------------;
; Load Normal Slots ;
;-------------------;

LoadNormalSlots:
LDA #$80
STA $2115
STZ $2116
LDA #$40
STA $2117
LDA #$03
STA $0F
LDA #$28
STA $0E

LoopGFX:
LDA $0E
TAX
LDA $00B992,x
STA $8A
LDA $00B9C4,x
STA $8B
LDA $00B9F6,x
STA $8C
STZ $00               ; \ Buffer = $7EB900
LDA #$B9	      ;  |
STA $01               ;  |
LDA #$7E              ;  |
STA $02               ; /
PHK
PER VRAMLoop-$01
PHB
PHY
JML $80BA47

VRAMLoop:
LDX #$06

DMALoopSoManieth:
LDA.l DMATable2,x
STA $4320,x
DEX
BPL DMALoopSoManieth
LDA #$04
STA $420B
INC $0E
DEC $0F
BPL LoopGFX
RTS

End:
print bytes