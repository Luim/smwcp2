;when touched, sets blue POW timer to 1/4 and plays the music, shake screen etc.
;by hach

db $42
JMP Main : JMP Return : JMP Return : JMP SpriteV : JMP SpriteH : JMP Main : JMP Return : JMP Return : JMP Return : JMP Return

Main:
	LDA #$08   ;shakes the ground
	STA $1887
	LDA #$0B   ;plays sound
	STA $1DF9
	LDA #$3F   ;sets timer
	STA $14AD
	LDA #$0E   ;plays POW music
	STA $1DFB
	RTL

SpriteV:
	LDA $AA,x
	BPL Return
SpriteH:
	LDA $14C8,x
	CMP #$09
	BEQ Main
	CMP #$0A
	BEQ Main
Return:
	RTL