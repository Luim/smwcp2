;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Arrow Launch, by cstutor89
;;
;; Description: Throws arrows either in a timed basis.
;;		Can throw many different sprites.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                !CUST_SPRITENUM = $9B

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
PRINT "INIT ",pc              
PRINT_TWO: "MAIN ",pc                                    
        	PHB                     
         	PHK                     
         	PLB                     
      		JSR GO_SHOOT  
          	PLB                     
  		RTL      

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main sprite code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GO_SHOOT:	LDA $17AB,x             ; \ RETURN if it's not time to generate
		BNE RETURN              ; /
		LDA #$20                ; \ set time till next generation = 20
                STA $17AB,x             ; /
		LDA $178B,x             ; \ don't generate if off screen vertically
                CMP $1C                 ;  |
                LDA $1793,x             ;  |
                SBC $1D                 ;  |
                BNE RETURN              ; /
                LDA $179B,x             ; \ don't generate if off screen horizontally
                CMP $1A                 ;  |
                LDA $17A3,x             ;  |
                SBC $1B                 ;  |
                BNE RETURN              ; / 
                LDA $179B,x             ; \ ?? something else related to x position of generator??
                SEC                     ;  | 
                SBC $1A                 ;  | 
                CLC                     ;  | 
                ADC #$10                ;  | 
                CMP #$20                ;  | 
                BCC RETURN              ; /
		JSL $02A9DE             ; \ get an index to an unused sprite slot, RETURN if all slots full
             	BMI RETURN              ; / after: Y has index of sprite being generated

SPRITE_GEN:	PHX                     ; \ before: X must have index of sprite being generated
                TYX                     ; / routine clears *all* old sprite values...
		LDA #$01                ; \ set sprite status for new sprite
              	STA $14C8,y             ; /
              	LDA #!CUST_SPRITENUM	; \ set sprite number for new sprite
             	STA $7FAB9E,x   	; /
		JSL $07F7D2		; reset sprite properties 
             	JSL $0187A7             ; get table values for custom sprite       
               	LDA #$88                ; \ mark as initialized
               	STA $7FAB10,x        	; /
              	PLX                     ; call init routine on sprite

            	LDA $179B,x             ; \ set x position for new sprite
                STA $00E4,y             ;  |
               	LDA $17A3,x             ;  |
               	STA $14E0,y             ; /
                LDA $178B,x             ; \ set y position for new sprite
         	STA $00D8,y             ;  |
            	LDA $1793,x             ;  |
           	STA $14D4,y             ; /

		LDA #$E0		;
		STA $00B6,y		; set the X speed
		LDA #$00		;
		STA $00AA,y		; set the Y speed

             	LDA $157C,x
            	STA $157C,y
RETURN:      	RTS			;  |
