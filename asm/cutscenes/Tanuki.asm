!tanukiType = !actorVar1,x
!tanukiXPos = !actorVar2,x
!tanukiYPos = !actorVar3,x
!tanukiDirection = !actorVar4,x

SetSpeakerToTanuki:
	LDX !tanukiIndex
	STX !currentSpeaker
	RTL

TanukiSetToBall:
	LDX !tanukiIndex
	LDA #$01
	STA !tanukiType
	RTL
	
TanukiSetToJump:
	LDX !tanukiIndex
	LDA #$00
	STA !tanukiType
	RTL
	
TanukiSetToZoomToRight:
	LDX !tanukiIndex
	LDA #$18
	STA !tanukiYPos
	LDA #$00
	STA !tanukiDirection
	RTL

TanukiSetToZoomToLeft:
	LDX !tanukiIndex
	LDA #$18
	STA !tanukiYPos
	LDA #$01
	STA !tanukiDirection
	RTL


	
TanukiCreate:
	REP #$20
	LDA.w #TanukiINIT
	STA $00
	LDX.b #TanukiINIT>>16
	STX $02
	
	LDA.w #TanukiMAIN
	STA $03
	LDX.b #TanukiMAIN>>16
	STX $05
	
	LDA.w #TanukiNMI
	STA $06
	LDX.b #TanukiNMI>>16
	STX $08
	SEP #$20
	JSL NewActor
	STX !tanukiIndex
	RTL

TanukiINIT:
	LDA #$10
	STA !tanukiYPos
	LDA #$E0
	STA !tanukiXPos
	RTL

TanukiTiles:
db $09, $0A	; Going right (normal)
db $29, $2A

db $0A, $09	; Going left (normal)
db $2A, $29

db $DB, $DC	; Going right (ball)
db $EB, $EC

db $DC, $DB	; Going left (ball)
db $EC, $EB

TanukiTilesXPositions:
db $00, $08	; Going right (normal)
db $00, $08

db $00, $08	; Going left (normal)
db $00, $08

db $00, $08	; Going right (ball)
db $00, $08

db $00, $08	; Going left (ball)
db $00, $08

TanukiTilesYPositions:
db $00, $00	; Going right (normal)
db $10, $10

db $00, $00	; Going left (normal)
db $10, $10

db $00, $00	; Going right (ball)
db $08, $08

db $00, $00	; Going left (ball)
db $08, $08

TanukiTilesProperties:
db $3E, $3E	; Going right (normal)
db $3E, $3E

db $7E, $7E	; Going left (normal)
db $7E, $7E

db $3E, $3E	; Going right (ball)
db $3E, $3E

db $7E, $7E	; Going left (ball)
db $7E, $7E
	
TanukiMAIN:
	
	PHK
	PLB

	LDA !tanukiDirection
	BEQ .movingRight
.movingLeft
	LDA !tanukiXPos
	CMP #$10
	BCC .doneMoving
	
	SEC
	SBC #$07
	STA !tanukiXPos
	BRA .doneMoving
	
.movingRight
	LDA !tanukiXPos
	CMP #$E0
	BCS .doneMoving
	CLC
	ADC #$07
	STA !tanukiXPos
	;BRA .doneMoving
	
.doneMoving

	LDA $13
	AND #$01
	BNE +
	LDA !tanukiYPos
	INC
	STA !tanukiYPos
+

	; "Stolen" from the Driad--makes Mario face this actor
	PHX
	
	LDA !tanukiXPos
	STA $00
	LDX !marioIndex
	LDA !marioXPos
	STA $01
	
	LDA $00
	CLC
	ADC #$10
	CMP $01
	BCC .faceLeft
	JSL MarioSetToFacingRight
	BRA +
.faceLeft
	JSL MarioSetToFacingLeft
+
	PLX
	

	STA $7FFFF2
	STZ $0C
	;LDA !tanukiType
	;STA $00
	;LDA !tanukiDirection
	;STA $01


	LDA #$03
	STA $06
.drawLoop
	;LDY #$04
	LDA !tanukiType
	CMP #$01
	BNE .notBall
	LDA $13
	AND #$04
	BEQ .noFlip
	LDA #$01
	STA $0C
.notBall
	LDA !tanukiDirection
	BEQ .noFlip
	LDA #$01
	STA $0C
.noFlip
	
	
	LDA !tanukiType
	ASL #3
	STA $03
	LDA $0C
	ASL #2
	CLC
	ADC $03
	CLC
	ADC $06
	TAY
	;LDA TanukiTiles,y
	
	LDA !tanukiXPos
	CLC
	ADC TanukiTilesXPositions,y
	STA $00
	
	LDA !tanukiYPos
	CLC
	ADC TanukiTilesYPositions,y
	STA $01
	
	LDA TanukiTiles,y
	STA $02
	
	LDA TanukiTilesProperties,y
	STA $03
	
	
	LDA #$02
	JSL DrawTile
	
	LDA $06
	DEC
	STA $06
	BPL .drawLoop
	
	
	RTL

TanukiNMI:
	RTL
	



	

