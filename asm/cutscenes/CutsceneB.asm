!CutsceneBDoorSpriteCHR = 	$0A24

CutsceneBPalette:
incbin CutsceneB.rawpal
	
CSINITB:
	STZ $2121
	%StandardDMA(#$2122, CutsceneBPalette, #512, #$00) 
	
	REP #$30	
	LDA #!DocCrocGeneralExGFX		; |
	LDX #$C000>>1				; | Upload Doc Croc (SP1)
	LDY #!CS1DocCrocExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	REP #$30
	LDA #!CutsceneBDoorSpriteCHR		; \
	LDX #$E000>>1				; | Upload SP2
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	SEP #$30
	
	LDA #$00
	STA !backgroundColor
	LDA #$00
	STA !backgroundColor+1
	LDA #$00
	STA !backgroundColor+2
	
	
	
	LDA #$00
	STA !currentFont
	
	JSL DocCrocCreate
	JSL CutsceneBDoorCreate
	LDA #$A2 : JSL DocCrocSetXPos
	LDA #$55 : JSL DocCrocSetYPos

	JSL SetSpeakerToDocCroc
	
	RTL

	

CSBT1:
db $FF
db $FF
%VWFASM(CutsceneBDoorOpen)
%VWFASM(DocCrocSetToStanding)
db $FF
db $FF

db "Bad news, boss. Mario's made it to the Pyro-Cryo research facility. I sent Frank to take care of him, but_", !CCZeroSpace
%VWFASM(DocCrocSetArmsBehindHead)
db "well, I wouldn't be surprised if Mario actually managed to beat him, to be completely frank. ", !CCButton, "Small F frank, I mean.", !CCButton, !CCClearText
%VWFASM(DocCrocSetArmsNormal)

%VWFASM(SetSpeakerToNull)
db !CCSetFont, $02
db "And well you shouldn't be, Doctor; I've just received word that the facility has gone off-line. Mario will be heading here next, I imagine. ", !CCButton, !CCClearText

%VWFASM(SetSpeakerToDocCroc)
db !CCSetFont, $00
db "From Glacignos? The only way to get in from there_", !CCZeroSpace
%VWFASM(DocCrocSetToStandingArmsOut)
db "well, he'd have to go straight through Sector 0A! "
%VWFASM(DocCrocSetToStanding)
db "He'd need to be a few sausages short of a six-", !CCZeroSpace, "pack to try to get in THAT way.", !CCButton, !CCClearText

%VWFASM(SetSpeakerToNull)
db !CCSetFont, $02
db "And from your own observations, would you say that sort of reckless rushing into action strikes you as out of character for our friend_?", !CCButton, !CCClearText

%VWFASM(SetSpeakerToDocCroc)
%VWFASM(DocCrocSetArmsBehindHead)
db !CCSetFont, $00
db "No, no, not a bit. "
%VWFASM(DocCrocSetArmsNormal)
db "You sort of have to admire the man's sheer chutzpah, really.", !CCButton, !CCClearText

%VWFASM(SetSpeakerToNull)
db !CCSetFont, $02
db "Make sure not to take your admiration too far, Doctor; this man has quite openly declared war on this entire company. I imagine you would wish to avoid fraternizing with the enemy, as it were, hmm_?", !CCButton, !CCClearText

%VWFASM(SetSpeakerToDocCroc)
%VWFASM(DocCrocSetArmsBehindHead)
db !CCSetFont, $00
db "Just trying to keep the company's good image, boss.", !CCButton, !CCClearText
%VWFASM(DocCrocSetArmsNormal)

%VWFASM(SetSpeakerToNull)
db !CCSetFont, $02
db "Your first priority should be to assure we still have a company left behind your well-", !CCZeroSpace, "maintained image, my dear Doctor. Declare a general lockdown, and double security in sector 1A.", !CCButton, !CCClearText

%VWFASM(SetSpeakerToDocCroc)
db !CCSetFont, $00
db "You got it, boss."
db $FF, $F2
%VWFASM(CutsceneBDoorClose)
db $FF, $FF, $F5

db !CCButton, !CCClearText

db $E0

