;only allow mario to pass if he's holding a particular sprite
;by smkdan

db $42
JMP Check : JMP Check : JMP Check : JMP Return : JMP Return : JMP Return : JMP Return
JMP Check : JMP Check : JMP Check

!SPRITENUM = $32

Check:
	LDX #$0B	;12 sprites to loop through
Loop:
	LDA $7FAB9E,x	;test sprite #
	CMP #!SPRITENUM
	BNE NextSprite	;must be equal to specified sprite

	LDA $1534,x
	CMP #$09
	BEQ GiveATen
	INC $1534,x
	RTL
GiveATen:
	STZ $1534,x
	INC $151C,x	;The ones address is at $1534,x, and the tens at $151C,x.
	RTL		;just return

NextSprite:
	DEX		;next sprite
	BPL Loop

Return:
	RTL