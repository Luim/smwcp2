;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Horizontal Thwomp, 
;; originally made by mikeyk ( left and right ) 
;;
;; First Extra Bit: YES
;;Clear	: Goes left
;;Set	: Goes right
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


incsrc subroutinedefs_xkas.asm


SPRITE_GRAVITY:		db $FC,$04
RETURN_SPEED:		db $10,$F0
DIRECTION_CHECK:		db $00,$01
POS_TABLE:		db $F6,$08
POS_TABLE_2:		db $FF,$00

!ANGRY_TILE		= $CA

X_OFFSET:		db $FC,$04,$FC,$04,$00
Y_OFFSET:		db $00,$00,$10,$10,$08
TILE_MAP:		db $8E,$8E,$AE,$AE,$C8
PROPERTIES:		db $03,$43,$03,$43,$03

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			PRINT "INIT ",pc
			LDA $E4,x
			CLC
			ADC #$08
			STA $E4,x
			STA $151C,x
			LDA $7FAB10,x
			AND #$04
			BEQ INIT_END
			INC $C2,x
INIT_END:		RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			PRINT "MAIN ",pc
			PHB
			PHK
			PLB
			JSR SPRITE_CODE_START
			PLB
			RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite main code 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

RETURN:			RTS  

SPRITE_CODE_START:	JSR SUB_GFX

			LDA $14C8,x
			CMP #$08
			BNE RETURN
			LDA $9D
			BNE RETURN

			STZ $AA,x

			JSL !SubOffScreen
			JSL $01A7DC

			LDA $1534,x
			BEQ WAITING
			DEC A
			BEQ ATTACKING
			JMP RETURNING

;-----------------------------------------------------------------------------------------
; state 0
;-----------------------------------------------------------------------------------------

WAITING:			LDA $186C,x
			ORA $15A0,x
			BNE RETURN0

			STZ $1528,x
			JSR SUB_HORZ_POS
			TYA
			LDY $C2,x
			CMP DIRECTION_CHECK,y
			BEQ RETURN0

			LDA $0E
			CPY #$00
			BNE RIGHT_THWOMP
			SEC
			SBC #$40
			PHA
			CMP #$48
			BCC THWOMP_4
			LDA #$01
			STA $1528,x
THWOMP_4:		PLA
			CMP #$60
			BCC RETURN0

NEXT_STATE:		LDA #$02
			STA $1528,x

			INC $1534,x

			STZ $B6,x

RETURN0:			RTS

RIGHT_THWOMP:		CLC
			ADC #$40
			PHA
			CMP #$B8
			BCS THWOMP_4_2
			LDA #$01
			STA $1528,x
THWOMP_4_2:		PLA
			CMP #$A0
			BCC NEXT_STATE
			RTS

;-----------------------------------------------------------------------------------------
; state 1
;-----------------------------------------------------------------------------------------

ATTACKING:		JSL $01802A

			LDY $C2,x
			LDA $B6,x
			CMP #$C0
			BMI DONT_INC_SPEED
			CLC
			ADC SPRITE_GRAVITY,y
			STA $B6,x

DONT_INC_SPEED:		LDA $E4,x
			PHA
			CLC
			ADC POS_TABLE,y
			STA $E4,x
			LDA $14E0,x
			PHA
			ADC POS_TABLE_2,y
			STA $14E0,x

			JSL $019138

			PLA
			STA $14E0,x
			PLA
			STA $E4,x

			LDA $1588,x
			AND #$03
			BEQ RETURN1
                    
			JSR SUB_9A04

			LDA #$40
			STA $1540,x

			LDA #$09
			STA $1DFC

			LDA #$18
			STA $1887

			INC $1534,x
RETURN1:			RTS                     

;-----------------------------------------------------------------------------------------
; state 2
;-----------------------------------------------------------------------------------------

RETURNING:		LDA $1540,x
			BNE RETURN2

			STZ $1528,x

			LDA $E4,x
			CMP $151C,x
			BNE RISE

			STZ $1534,x
			RTS

RISE:			LDY $C2,x
			LDA RETURN_SPEED,y
			STA $B6,x
			JSL $01802A
RETURN2:			RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUB_GFX:			JSL !GetDrawInfo

			LDA $1528,x
			STA $02
			PHX
			LDX #$03
			CMP #$00
			BEQ LOOP_START
			INX
LOOP_START:		LDA $00
			CLC
			ADC X_OFFSET,x
			STA $0300,y

			LDA $01
			CLC
			ADC Y_OFFSET,x
			STA $0301,y

			LDA PROPERTIES,x
			ORA $64
			STA $0303,y

			LDA TILE_MAP,x
			CPX #$04
			BNE NORMAL_TILE
			PHX
			LDX $02
			CPX #$02
			BNE NOT_ANGRY
			LDA #!ANGRY_TILE
NOT_ANGRY:		PLX
NORMAL_TILE:		STA $0302,y

			INY
			INY
			INY
			INY
			DEX
			BPL LOOP_START

			PLX

			LDY #$02
			LDA #$04
			JSL $01B7B3
			RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; speed related
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUB_9A04:		LDA $1588,x
			BMI THWOMP_1
			LDA #$00
			LDY $15B8,x
			BEQ THWOMP_2
THWOMP_1:		LDA #$18
THWOMP_2:		STA $B6,x
			RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_HORZ_POS
; This routine determines which side of the sprite Mario is on.  It sets the Y register
; to the direction such that the sprite would face Mario
; It is ripped from $03B817
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUB_HORZ_POS:		LDY #$00
			LDA $E4,x
			STA $00
			LDA $14E0,x
			STA $01
			REP #$20
			LDA $94
			SEC
			SBC $00
			STA $0E
			BPL SPR_L16
			INY
SPR_L16:			SEP #$20
			RTS