;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; H:None BG Scroll Reset Patch
; Coded by Edit1754/SMWEdit
;
; what this patch does:
; fixes the glitch in which the BG's X-position isn't reset when the
; player exits from a level with H:Variable into a level with H:None
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
header
lorom

org $0580CD
		autoclean JSL FixScroll

freecode

FixScroll:	INC $47			; \ hijacked
		INC $4B			; / code
		LDA.w $1413		; \ don't reset BG Horz Scroll if
		BNE Return		; / H scroll is not set to "none"
		STZ.b $1E		; \
		STZ.b $1F		;  | reset BG
		STZ.w $1466		;  | Horz Scroll
		STZ.w $1467		; /
Return:		RTL