#SPC
{
	#author "Moose"
	#comment "Desert ruins level 2"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
}

tuning[10]=0
tuning[15]=0

#0 t46 w255 $f4 $02
$ef $fc $18 $18
$f1 $06 $d0 $01
/
v184
(1)[y10 q7f @26c8 y9 @10 o4c12 y10 @26c24 y9 @10c12 y11 q78 c24 y10 q7f @26c12 y9 @10c24
y11 q78 @10c12 y9 q7f c24 y10 @26c12 y11 q78 @10c24 y9 q7f c12 y11 q78 c24 r12 c24]5
(2)[y10 q7f @26c8 y9 @10 o4c12 y10 @26c24 y9 @10c12 y11 q78 c24 y10 q7f @26c12 y9 @10c24
y11 q78 @10c12 y9 q7f c24 y10 @26c12 y11 q78 @10c24 y9 q7f c12 y11 q78 c24 y10 q7c @29c8]17
(1)7 (2)9

#1 v184
[y10 q7f @21c12 y11 q78 @22c24 q7f @22c12 q78 @22c24]152

#2 v184 @8 y10
(3)[$eb $0c $30 $fe q7f o2c8c12<b8a+8a8g+24g4r8]6
$eb $00 $00 $00
[q7f o2c=40 c24 q2f c8c8r8c8 q7f c12c=32c=40 c24 q2f c8c8r8c8 q7f c12<a+=32
o2c=40 c24 q2f c8c8r8c8 q7f c12c=32c=40 c24 q2f c8c8r8c8 q7f c12<b=32
a+=40 a+24 q2f a+8a+8r8a+8 q7f a+12g+=32a+=40 a+24 q2f a+8a+8r8a+8 q7f g+12a=32
a+=40 a+24 q2f a+8a+8r8a+8 q7f a+12a+=32a+=40 a+24 q2f a+8a+8r8g+8 q7f a+12b=32]2
(3)8 $eb $00 $00 $00
[a+=40 a+24 q2f a+8a+8r8a+8 q7f a+12a+=32g+=40 g+24 q2f g+8g+8r8g+8 q7f g+12a=32]4

#3 v184 y10 $df
(4)[@15 o2 q7f e8. q7c e8. q78 e8. q74 e=84]
(5)[r2]9 r4 q7f @10 o3c12c8c24
@1 q7c o4c4.f=120c4.<g=120>c4.f=120c4.<g=120
a+4.>d+=120<a+4.a+=120a+4.>d+=120c4.<a+=120
v208@0 $de $18 $10 $70 q7f r4o3g=40>c=80 $dd $00 $06 $a6 q2f c8 q7f <a+=40>c24 r8 <a4 $dd $24 $01 $a2 $dd $05 $01 $a1 g=72
r4o3g=40>c=56 $dd $00 $06 $a6 q2f d8e8 q7f f=40g24 r8 a4 $dd $24 $01 $ae $dd $05 $01 $ad g=72
r4o3f=40a+=56 $dd $00 $06 $a4 q2f >c8<a+8 q7f g+=40g24 r8 g+4 $dd $24 $01 $9f $dd $05 $01 $9e f=72
r4o3f=40a+=56 $dd $00 $06 $a4 q2f >c8d8 q7f d+=40f24 r8 g4 $dd $24 $01 $ac $dd $05 $01 $ab f=72 $df
v184
(4) (5)6 (4) (5)5 r4 q7f @10 o3c12c8c24
@6 $de $18 $10 $70 r8o3g+4 q2f a+8 q7f >c8 $dd $00 $06 $a6 q2f d+12 q7f f=56r8f12f+24g+8f+8g+12a+24r12g+=32f+8
f12f+24f=64d+24d12d+24d4. @0 q7a r8b8a+24b24a+24f+8f2 $dd $24 $30 $a7
@6 q7f r8o3f+12g+24a+8 q2f b8 q7f >c12 $dd $00 $06 $a6 d+24 r12 f=32f12f+24g+8f+8g+12a+24r12b8>c+8<b24a+12g+24f+12g+24
a+12b24a+=64f+24f12f+24f4.r8<a+12b24>d12d+24d12<b24a+12g+24a+4.

#4 v184 y10
@16 (6)[q7f r12o4f12g12g+12a+12> q5f c12 q7f <b12 $dd $00 $06 $b1 >c12<a+12g+12g=56
e12f24g12c+=32 $dd $1a $01 $a4 <a+12g+=56 $dd $18 $01 $9f $dd $17 $01 $9e r8
r12e12f12g12g+12 q5f a+12 q7f b12 $dd $00 $06 $a5 a+12g+12g12f+6]
(12)[r12>c12d12e12f12 q5f g12 q7f g12 $dd $00 $06 $ac g12f12] d+12d6
(7)[r12c24c+12e8g24c8r12c+24<a+12g+8a+24g8
a+8. $dd $00 $06 $a4 q7c a+8. $dd $00 $06 $a4 q78 a+8. $dd $00 $06 $a4 q76 a+8. $dd $00 $06 $a4] q74 a+8. $dd $00 $06 $a4 r16
[@1 y12 q7c o3a+4.>c=120<a+4.d=120a+4.>c=120<a+4.e=120
g+4.a+=120g+4.f=120g+4.a+=120g4.f=120]2 y10
@16 (6) (12) o4a+4
q7f r12o4f12g12g+12a+12> q5f c12 q7f <b12 $dd $00 $06 $b1 >c12<a+12g+12>c6
<g4e12c6 $dd $14 $01 $a3 <a+4>c4 $dd $00 $06 $a5 (7) q74 o3a+8. $dd $00 $06 $a4 r16
@11 q7c o4d+=120d+4.c+=120c+4.d+=120d+4.c+8 @0 q7a g+8f+24g+24f+24d+8d2 $dd $24 $30 $a4
@11 q7c o4d+=120d+4.c+=120c+4. @6 $ed $3a $6a f12f+24f=64d+24d12d+24d4. @11c+2^4 q7f @10 o3c12c8c24

#5 (5)6 y10 v92 ;or #4 vx/2
r12 $de $0c $10 $70 (10)[@16 o5c12d12e12f12 q5f g12 q7f g12 $dd $00 $06 $b8 g12f12] d+12d6
(11)[r12c24c+12e8g24c8r12c+24<a+12g+8a+24g8
a+8. $dd $00 $06 $b0 q7c a+8. $dd $00 $06 $b0 q78 a+8. $dd $00 $06 $b0 q76 a+8. $dd $00 $06 $b0] q74 a+8. $dd $00 $06 $b0 r16
v184 ;normal volume
@14 $ed $2e $26 $df (8)[q7c o4f12g8f24g8>c=120<f12g8f24g8c=120]2
(9)[q7c o4d+12f8d+24f8a+=120d+12f8d+24f8<a+=120]2 (8)2 (9)2
v92 ;volume/2
(5)6 r12 $de $0c $10 $70 (10) o5a+4
q7f r12o5f12g12g+12a+12> q5f c12 q7f <b12 $dd $00 $06 $bd >c12<a+12g+12>c6
<g4e12c6 $dd $14 $01 $af <a+4>c4 $dd $00 $06 $b1 (11) q74 a+8. $dd $00 $06 $b0 r16
y12 v184 $df ;and back
[@11 q7c o4c=120c4.<a+=120a+4.]4

#6 v138 y8
r16. @16 (6) v92 (12) o4d+12d6
(7) q74 a+=30 $dd $00 $06 $a4 v184 ;normal volume
@1 y8 (13)[q7c o3f4.a+=120f4.c=120]2
(14)[q7c o3d+4.g+=120d+4.d=120]2 (13)2 (14)2 v138 ; 75% volume
r16. @16 (6) v92 (12) o4a+4
q7f r12o4f12g12g+12a+12> q5f c12 q7f <b12 $dd $00 $06 $b1 >c12<a+12g+12>c6
<g4e12c6 $dd $14 $01 $a3 <a+4>c4 $dd $00 $06 $a5 (7) q74 o3a+=30 $dd $00 $06 $a4 y8 v184 ;normal volume
[@11 q7c o3g+=120g+4.f+=120f+4.]4

#7 v92 y12
r16.r16. $df (18)[@16 q7f r12o4f12g12g+12a+12> q5f c12 q7f <b12 $dd $00 $06 $b1 >c12<a+12g+12g=56
e12f24g12c+=32 $dd $1a $01 $a4 <a+12g+=56 $dd $18 $01 $9f $dd $17 $01 $9e r8
r12e12f12g12g+12 q5f a+12 q7f b12 $dd $00 $06 $a5 a+12g+12g12f+=14] v46 r12 (10) o5d+12d6
(11) q74 a+=30 $dd $00 $06 $b0 v184 ;normal volume
@14 $ed $2e $26 y10 (16)[q7f o3a+4.>f=120c4.<g=120]2
(17)[q7f o3g+4.>d+=120<a+4.f=120]2 (16)2 (17)2
v92 y12 r16.r16. (18) v46 r12 (10) o5a+4
q7f r12o5f12g12g+12a+12> q5f c12 q7f <b12 $dd $00 $06 $bd >c12<a+12g+12>c6
<g4e12c6 $dd $14 $01 $af <a+4>c4 $dd $00 $06 $b1 (11) q74 a+=30 $dd $00 $06 $b0 v184
y10 q7c @14 $ed $2e $26 [o3g+8 $dd $06 $01 $a2 >d+8f8a+8<a+12>d+24f12a+8f24d+12<a+24
o3f+8 $dd $06 $01 $a0 >c+8d+8g+8<g+12>c+24d+12g+8d+24<g+12a24]3
o3g+8 $dd $06 $01 $a2 >d+8f8a+8<a+12>d+24f12a+8f24d+12<a+24
o3f+8 $dd $06 $01 $a0 @0 $de $18 $10 $70 q7f a+12b24>d12d+24d12<b24a+12g+24a+4.

#amk=1