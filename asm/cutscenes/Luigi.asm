

!luigiXPos = !actorVar1,x
!luigiYPos = !actorVar2,x
!luigiState = !actorVar3,x
!luigiHolding = !actorVar4,x
!luigiProperties = !actorVar5,x

SetSpeakerToLuigi:
	LDX !luigiIndex
	STX !currentSpeaker
	RTL


LuigiCreate:
	REP #$20
	LDA.w #LuigiINIT
	STA $00
	LDX.b #LuigiINIT>>16
	STX $02
	
	LDA.w #LuigiMAIN
	STA $03
	LDX.b #LuigiMAIN>>16
	STX $05
	
	LDA.w #LuigiNMI
	STA $06
	LDX.b #LuigiNMI>>16
	STX $08
	SEP #$20
	JSL NewActor
	STX !luigiIndex
	RTL

LuigiDelete:
	LDX !luigiIndex
	JSL DeleteActor
	RTL

LuigiINIT:
	LDA #$40
	STA !luigiYPos
	RTL
	
LuigiSetToHolding:
	LDX !luigiIndex
	LDA #$01
	STA !luigiHolding
	RTL
	
LuigiSetToNotHolding:
	LDX !luigiIndex
	LDA #$00
	STA !luigiHolding
	RTL
	
LuigiSetX:
	LDX !luigiIndex
	STA !luigiXPos
	RTL
	
LuigiSetY:
	LDX !luigiIndex
	STA !luigiYPos
	RTL
	
;LuigiSetXTo90:
;	LDX !luigiIndex
;	LDA #$90
;	STA !luigiXPos
;	RTL
	
;LuigiSetXToF0:
;	LDX !luigiIndex
;	LDA #$F0
;	STA !luigiXPos
;	RTL
	
LuigiSetToStanding:
	LDX !luigiIndex
	LDA #$01
	STA !luigiState
	LDA #$90
	STA !luigiXPos
	RTL
	
LuigiSetToWalkingLeft:
	LDX !luigiIndex
	LDA #$00
	STA !luigiState
	LDA #$90
	STA !luigiXPos
	LDA #$F0
	STA !luigiXPos
	RTL
	
LuigiSetToWalkingRight:
	LDX !luigiIndex
	LDA #$02
	STA !luigiState
	LDA #$00
	STA !luigiXPos
	RTL
	
LuigiSetToFacingRight:
	LDX !luigiIndex
	LDA #$40
	STA !luigiProperties
	RTL
	
LuigiSetToFacingLeft:
	LDX !luigiIndex
	LDA #$00
	STA !luigiProperties
	RTL
	

LuigiStateTable:
dw LuigiRunInFromRight, LuigiStandStill, LuigiRunInFromLeft

LuigiMAIN:
	PHK
	PLB
	
	PHX				; Jump to the Doc's current routine.
	LDA !luigiState			;
	ASL
	TAX
	LDA LuigiStateTable,x	;
	STA $00				;
	LDA LuigiStateTable+1,x	;
	STA $01				;
	PLX				;
	PEA.w .ret1-1			;
	JMP [$0000]			;
.ret1	
	
	RTL
	
LuigiDrawTile:
	;PHA
	;LDA !cutsceneNumber
	;BEQ +
	;LDA $02
	;SEC
	;SBC #$80
	;STA $02
	;+
	;PLA
	JSL DrawTile
	RTS
	
LuigiRunInFromLeft:
	LDA !luigiXPos
	CMP #$63
	BCC .moveRight
	LDA #$01
	STA !luigiState
	BRA .draw
	
.moveRight
	LDA !luigiXPos
	INC
	INC
	STA !luigiXPos
	
.draw

	;LDA !luigiXPos
	LSR
	LSR
	AND #$03
	ASL
	PHA
	JMP LuigiStandardDraw
	
	

LuigiRunInFromRight:
	LDA !luigiXPos
	CMP #$90
	BCS .moveLeft
	LDA #$01
	STA !luigiState
	BRA .draw
	
.moveLeft
	LDA !luigiXPos
	DEC
	STA !luigiXPos
	
.draw

	;LDA !luigiXPos
	LSR
	LSR
	AND #$03
	ASL
	PHA
	
LuigiStandardDraw:		; Note: push the body tile onto the stack first
	
	LDA !luigiHolding
	BEQ .noDrawFish
	
	LDA !luigiXPos
	SEC
	SBC #$08
	STA $00
	
	LDA !luigiYPos
	CLC
	ADC #$08
	STA $01
	
	PLA
	CMP #$04
	BNE +
	INC $01
+
	PHA
	
	LDA #$6E
	STA $02
	
	LDA #$B3		; \
	EOR !luigiProperties	; |
	STA $03			; / Property
	
	;STX $04
	LDA #$02
	JSR LuigiDrawTile
	
	
.noDrawFish
	

	;STX $04
	;LDA !luigiHolding
	;BEQ +
	;INC $04
;+
	
	LDA !luigiXPos		; \
	STA $00			; / X pos
	LDA !luigiYPos		; \ Y pos
	STA $01			; /
	PLA
	CMP #$04
	BNE +
	INC $01
+
	PHA

	LDA !textIsAppearing	; If there is no text appearing,
	BNE .openMouth		;
	LDA #$20		; Then draw tile $40
	BRA +			;
.openMouth			;
	LDA $13			; \
	AND #$04		; |
	LSR			; | Otherwise, animate the mouth.
	CLC			; |
	ADC #$20		; /
	
+
	STA $02
	
	LDA #$32		; \
	LDY !cutsceneNumber
	BNE +
	INC
	+
	EOR !luigiProperties	; |
	STA $03			; / Property
	
	;INC $04
	
	LDA #$02
	JSR LuigiDrawTile
	

	
	LDA $01
	CLC
	ADC #$10
	STA $01
	
	PLA			; \
	STA $02			; /
	LDA !luigiHolding
	BEQ +
	LDA $02
	CLC
	ADC #$08
	STA $02
+
	
	;INC $04			; Index
	LDA #$02		; Size
	JSR LuigiDrawTile		;

	RTS
	
LuigiStandStill:
	LDA #$00
	PHA
	JMP LuigiStandardDraw

	
LuigiNMI:
	;LDA !hindenBirdNMIInit
	;BNE +
	;LDA.b #!layer1MapAddr+$800>>11<<2	; \
	;STA $2107				; /
	;LDA #$01
	;STA !hindenBirdNMIInit
	+
RTL
