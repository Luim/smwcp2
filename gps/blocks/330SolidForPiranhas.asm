;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;This block is solid only for Sprites $4F & $50,
;;Jumping Piranha Plant and Fire-Spitting variant.
;;
;;The tile should act like Tile 25.
;;
;;note: This block can be used for any sprites,
;;just change the sprite number(s) that are
;;compared against $9E,x. And if you need to
;;disallow carryable/kicked variants of any
;;sprite, just uncomment the code under MakeSolid.
;;                                       -Milk
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

db $42
JMP Return : JMP Return : JMP Return : JMP Sprites : JMP Sprites : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return

Sprites:
	LDA $9E,x		; Sprite Number
	CMP #$4F		;\ Branch if Jumping Piranha
	BEQ MakeSolid		;/
	CMP #$50		;\ Branch if Jumping Piranha, spit fire
	BEQ MakeSolid		;/
	RTL
MakeSolid:
	;LDA $14C8,x		; Sprite State
	;CMP #$09		;\ Return if Carryable
	;BEQ Return		;/
	;CMP #$0A		;\ Return if Kicked
	;BEQ Return		;/
	LDY #$30		;\
	STY $1693		; | make solid
	LDY #$01		;/
Return:
	RTL