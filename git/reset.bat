set /p reset="Are you sure you want to reset? This will remove all changes you have not commited. [y/N] "
if "%reset%" == "y" (
	git reset --hard
)
@pause