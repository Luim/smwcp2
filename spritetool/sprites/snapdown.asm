;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Snapdown, cstutor89
;; Displays Message 2, shakes the floor, and lowers the wall down on Mario
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PRINT "INIT ",pc	
		LDA #$02
		STA $1426
		RTL

PRINT "MAIN ",pc	
		PHB			; \
		PHK			;  | main sprite function, just calls local subroutine
		PLB			;  |
		JSR SPRITE_MAIN		;  |
		PLB			;  |
		RTL			; /

SPRITE_MAIN:	LDA $9D		;\ If sprites are locked...
		BNE RETURN	;/ Go to "RETURN".
		LDA $13		;\
		AND #$0F
		CMP #$0F
		BNE RETURN	;/ Only run code every 80 frames.	
		LDA #$02	;\ Make sure that new tiles are uploaded from the bottom of the screen
		STA $56		;/
		LDA $1469
		CMP #$5F
		BCS DESTROYSPRITE
		REP #$20	; 16-bit (accumulator).
		DEC $1468	; Make the screen scroll downwards automatically.
		SEP #$20	; 8-bit (accumulator).
		LDA #$0E	; Shake ground forever
		STA $1887
RETURN:		RTS

DESTROYSPRITE:	LDA $1468
		BNE RETURN
		LDA #$04
		STA $14C8,x
		RTS