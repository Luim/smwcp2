;; Sand Block. By ICB. Make act like Block 25
db $42
JMP Return : JMP MarioAbove : JMP Return : JMP Solid : JMP Return : JMP Return : JMP Solid : JMP Solid : JMP Return : JMP Return



MarioAbove:
	LDA $16		;If button
	AND #$40	;Y or X isn't pressed
	BEQ Solid	;return
	LDA #$E4	;Move Mario up
	STA $7D		;for a second
	STZ $7B		;Zero horz speed
	LDA #$0A	;Show ducking while picking up
	STA $1498	;item pose
	LDA #$06	;play sound
	STA $1DF9

	LDA #$02	;Generate
	STA $9C		;A
	JSL $00BEB0	;blank block
	JSR SUB_SMOKE	;make smoke (er...sand)

Solid:
	LDY #$01	;act like tile 130
	LDA #$30
	STA $1693
Return:
	RTL

SUB_SMOKE:
	PHP
	SEP #$20
	LDY #$03	; \ find a free slot to display effect
FINDFREE:
	LDA $17C0,y	;  |
        BEQ FOUNDONE	;  |
        DEY		;  |
        BPL FINDFREE	;  |
	PLP
        RTS		; / return if no slots open

FOUNDONE:
	LDA #$01	; \ set effect graphic to smoke graphic
        STA $17C0,y	; /
        LDA $98		; \ smoke y position = generator y position
	AND #$F0
	SEC 
	SBC #$10
        STA $17C4,y	; /

        LDA #$15	; \ set time to show smoke
        STA $17CC,y	; /
        LDA $9A		; \ load generator x position and store it for later
	AND #$F0
        STA $17C8,y	; /
        LDX $15E9
	PLP
        RTS

