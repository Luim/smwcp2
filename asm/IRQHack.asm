lorom
header

!freeram = $87		;Set freeram, shared with the wheel
!LavaYPos = $140B	; 

;org $80FFD8
;	db $05

org $8081AA
	autoclean JML IRQScanlineHack
	
org $8081AF
	IRQScanlineHackReturn:
	
org $808385
	autoclean JML IRQCode
	
org $0083B2
	IRQCodeReturn:

;Breaks the original bosses. We don't want the NMI to overwrite the stuff we set earlier
org $8082F7
	NOP #5

;Also breaks the original bosses, stops the lava from being done
org $809883
	RTS

org $808345
	LDY !freeram
	JMP +
	
org $808294
	+

org $808416
	autoclean JML L1ScrollHijack
	
org $80841B
	L1ScrollReturnNoWheel:

org $808438
	L1ScrollReturnWheel:
	
freecode

L1ScrollHijack:
	LDA $1908
	CMP #$04
	BNE .NotWheelBoss
		LDA $3A
		STA $210D
		LDA $3B
		STA $210D
		LDA $3C
		STA $210E
		LDA $3D
		STA $210E
		JML L1ScrollReturnWheel
	.NotWheelBoss:
		LDA #$59
		STA $2107
		JML L1ScrollReturnNoWheel


IRQScanlineHack:
	LDA #$80				;Force blank
	STA $2100
	LDA $1908
	CMP #$04
	BNE +
	LDA !freeram
	STA $4209
+
	JML IRQScanlineHackReturn

IRQCode:
	NOP #4				; cutscene hijack goes here
	LDA $1908
	CMP #$04
	BNE NotIRQ
	LDA !freeram
	BEQ NotIRQ
-
	BIT $4212
	BVS -
-
	BIT $4212
	BVC -				;Wait for hblank
	LDA #$01
	STA $2105
	LDA.b #($5400>>10<<2|1)
	STA $2107
	LDA.b #((0xA000>>13)|(0xA000>>9))
	STA $210B
;Layer 1 positioning
	REP #$20
	LDA $1C
	SEC
	SBC #$0180
	SEP #$20
	STA $210E
	XBA
	STA $210E

	LDA #$11
	STA $212C
	STZ $212D

	LDA $1A
	STA $210D
	LDA $1B
	STA $210D


NotIRQ:
	JML IRQCodeReturn
