header
lorom

cleartable
table chartable.txt

org $03BC0B
    dl messages

; This exists because LM will overwrite the dl at 03BC0B, thereby creating a memory leak
org $03BE70
    autoclean dl messages

freedata
    messages:
        message_000_1:
            db "Jukebox is located"
            db "inside   the  hub."
            db "Enter left side of"
            db "door.             "
            db "                  "
            db "                  "
            db "Version ", $67,$64,".", $66,$65,$FE
        message_000_2:
            db "sup", $FE
        message_001_1:
            db "     Jukebox      "
            db "Press up to enter "
            db "The Jukebox! Here,"
            db "you can listen to "
            db "the game's music! "
            db "Press  start  to  "
            db "Exit.", $FE
        message_001_2:
            db " Progress Tracker "
            db "This machine will "
            db "track your journey"
            db "through the world."
            db "Return here to see"
            db "  levels you have "
            db " beaten and coins "
            db "you have gathered."
            db $FE
        message_002_1:
            db "In   every  level,"
            db "there   are  three"
            db "SMWC        coins."
            db "Collect  them  all"
            db "to  unlock bonuses"
            db "and   other   cool"
            db "things!", $FE
        message_002_2:
            db "This  is  the goal"
            db "roulette!  How you"
            db "time   your   jump"
            db "will     determine"
            db "which   prize  you"
            db "get. Good luck!", $FE
        message_003_1:
            db "-Traveller's Tips-"
            db "                  "
            db "See  Lakitu flying"
            db "by?  With a shell,"
            db "you  can knock him"
            db "right  out  of the"
            db "sky!", $FE
        message_003_2:
            db "    -WARNING-     "
            db "                  "
            db "Lava  is  HOT!  Be"
            db "careful   not   to"
            db "touch    it,    or"
            db "you'll    lose   a"
            db "life!", $FE
        message_004_1:
            db "To     enter     a"
            db "minigame, press Up"
            db "in  front  of  the"
            db "sign.  Once you've"
            db "entered, you can't"
            db "go  backwards,  so"
            db "choose wisely.    "
            db "            -Mega-"
            db $FE
        message_004_2:
            db $FE
        message_005_1:
            db $FE
        message_005_2:
            db $FE
        message_006_1:
            db "     Amazing!     "
            db "What     excellent"
            db "Switch Scampering!"
            db $FE
        message_006_2:
            db "Hurry! Rush! Rush!"
            db "The time limits on"
            db "these switches are"
            db "short, so make the"
            db "most out of them!", $FE
        message_007_1:
            db " POINT OF ADVICE  "
            db "                  "
            db "Holding only the B"
            db "button  will yield"
            db "the       greatest"
            db "distance    across"
            db "the final leap.", $FE
        message_007_2:
            db $FE
        message_008_1:
            db "- SWITCH PALACE - "
            db "The  power  of the"
            db "switch  you   have"
            db "pushed  will  turn"
            db "                  "
            db "      into     .  "
            db "Your progress will"
            db "also   be   saved."
            db $FE
        message_008_2:
            db "The  coins  in the"
            db "next   room   will"
            db "increase       the"
            db "amount   of   time"
            db "remaining  on  the"
            db "clock. If you beat"
            db "the  clock,  a big"
            db "reward awaits!", $FE
        message_009_1:
            db "Office of TRUNTEC "
            db "                  "
            db "                  "
            db " The Boss is IN.", $FE
        message_009_2:
            db "RIGHT - Customer  "
            db "Service Department"
            db "                  "
            db "LEFT - Express    "
            db "ladder to Wood    "
            db "Nymph Woods       "
            db "                  "
            db "DOWN - A hot bath."
            db $FE
        message_00A_1:
            db $FE
        message_00A_2:
            db $FE
        message_00B_1:
            db "The  heat is on at"
            db "Norveg Industries!"
            db "Our  thermal  land"
            db "reclamation       "
            db "process    creates"
            db "rich  potash while"
            db "opening  new areas"
            db "for development.", $FE
        message_00B_2:
            db "Please  vacate the"
            db "area.  We  are not"
            db "liable   for   any"
            db "injuries sustained"
            db "by    unauthorized"
            db "persons     and-or"
            db "looky-loos  during"
            db "this procedure.", $FE
        message_00C_1:
            db "Aren't       these"
            db "flowers    lovely?"
            db "They're   safe  to"
            db "walk  on,  y'know!"
            db "The Boos here take"
            db "very  good care of"
            db "them,  so show the"
            db "proper respect.", $FE
        message_00C_2:
            db "  ---WARNING---   "
            db "A  Fishin' Boo has"
            db "been   spotted  in"
            db "this    area!   Be"
            db "careful!  Be  sure"
            db "to  duck  often to"
            db "avoid the flame it"
            db "carries!", $FE
        message_00D_1:
            db "Norveg  Mining Co."
            db "would    like   to"
            db "notify   you  that"
            db "the   dark   brown"
            db "rails are not safe"
            db "to  walk on by any"
            db "means.", $FE
        message_00D_2:
            db "Any       injuries"
            db "caused          by"
            db "attempting      to"
            db "prove   the   last"
            db "statment     false"
            db "will   not   be  a"
            db "justifiable excuse"
            db "in a court of law."
            db $FE
        message_00E_1:
            db "My,    you    look"
            db "chunky.   Take   a"
            db "running  leap  off"
            db "of  this cliff and"
            db "burn  off  some of"
            db "those nasty, nasty"
            db "carbs!", $FE
        message_00E_2:
            db $FE
        message_00F_1:
            db "WARPLANE  TRAINING"
            db "IN PROCESS.       "
            db "                  "
            db "Do not disrupt the"
            db "pilots.   Norveg's"
            db "soldiers   require"
            db "complete focus.", $FE
        message_00F_2:
            db "A  piece  of paper"
            db "falls   from   the"
            db "plane.            "
            db "                  "
            db "A", $68,"   E", $65,"   C", $66,"      "
            db "                  "
            db "...huh?", $FE
        message_010_1:
            db $FE
        message_010_2:
            db $FE
        message_011_1:
            db $FE
        message_011_2:
            db $FE
        message_012_1:
            db "Oh  dear! It's too"
            db "dark  in  here. Go"
            db "back   outside   -"
            db "perhaps   you  can"
            db "find  some sort of"
            db "lantern which will"
            db "brighten      this"
            db "room.", $FE
        message_012_2:
            db "  look UP in the  "
            db "sky               "
            db "                  "
            db "     and let its  "
            db "halcyon light     "
            db "                  "
            db "       cleanse the"
            db "dark away", $FE
        message_013_1:
            db "What    was   that"
            db "noise?  You didn't"
            db "press  that button"
            db "up there, did you?"
            db "AARRGH,     thanks"
            db "a    lot,   Mario!"
            db "Years   of    work"
            db "down the drain.", $FE
        message_013_2:
            db "Ignore  him,  he's"
            db "being         over"
            db "dramatic. That was"
            db "the  sound of that"
            db "room  resetting it"
            db "self. Not sure why"
            db "it    does    that"
            db "though...", $FE
        message_014_1:
            db "The  water here is"
            db "polluted   because"
            db "of        Norveg's"
            db "wretched machines."
            db "                  "
            db "Now you can't even"
            db "swim       without"
            db "getting killed!", $FE
        message_014_2:
            db "Whoa,    the   hot"
            db "springs      along"
            db "these    mountains"
            db "are  really  warm!"
            db "Don't  stay in too"
            db "long  or  you  may"
            db "burn, and we don't"
            db "want that right?", $FE
        message_015_1:
            db "     -NOTICE-     "
            db "Fishing  of  Porcu"
            db "puffers  is halted"
            db "temporarily,    as"
            db "they          have"
            db "developed       an"
            db "immunity to fire.", $FE
        message_015_2:
            db "    -NOTICE-      "
            db "The red species of"
            db "Porcupuffer     is"
            db "incredibly        "
            db "aggressive     and"
            db "will   jump   out."
            db "Travellers beware!"
            db $FE
        message_016_1:
            db "POINT OF ADVICE   "
            db "                  "
            db "If  you hold the Y"
            db "Button   while   a"
            db "blue  shell  is in"
            db "Yoshi's mouth, you"
            db "can fly faster.", $FE
        message_016_2:
            db "These grey bridges"
            db "are   no  ordinary"
            db "bridges.  Somehow,"
            db "hitting  a certain"
            db "switch  causes one"
            db "to be passable.", $FE
        message_017_1:
            db "Hold   it,  Kitty-"
            db "Cat!  Didn't  your"
            db "mother  ever  tell"
            db "you  not  to  leap"
            db "off         mighty"
            db "precipices?    No?"
            db "She encouraged it,"
            db "you say?", $FE
        message_017_2:
            db "...I'm    a   rock"
            db "formation. I never"
            db "had a mother.     "
            db "              .   "
            db "              .(", $FE
        message_018_1:
            db "Note  that  due to"
            db "mandatory   switch"
            db "cleaning       and"
            db "maintenance,  your"
            db "normal  P-switches"
            db "have been replaced"
            db "with much bouncier"
            db "counterparts.", $FE
        message_018_2:
            db $FE
        message_019_1:
            db $FE
        message_019_2:
            db $FE
        message_01A_1:
            db $FE
        message_01A_2:
            db "Those  things sure"
            db "are  bouncy! Could"
            db "they  be  used  to"
            db "your advantage...?"
            db $FE
        message_01B_1:
            db "Whoa! The pressure"
            db "down  here sure is"
            db "intense!  You  may"
            db "not   be  able  to"
            db "swim    very   far"
            db "without the aid of"
            db "something a little"
            db "more buoyant.", $FE
        message_01B_2:
            db "Holding 'UP' while"
            db "swimming without a"
            db "throw  block  will"
            db "allow  you to jump"
            db "a  bit higher when"
            db "under this extreme"
            db "pressure.", $FE
        message_01C_1:
            db $FE
        message_01C_2:
            db $FE
        message_01D_1:
            db "This  island's got"
            db "some really", $35," bite-"
            db "happy  alligators!"
            db "Better be careful,"
            db "especially  if you"
            db "see   a  (!)  sign"
            db "around.", $FE
        message_01D_2:
            db "The water ahead is"
            db "marshy         and"
            db "dangerous  to swim"
            db "in! You'll have to"
            db "find  another  way"
            db "to cross it!", $FE
        message_01E_1:
            db $FE
        message_01E_2:
            db $FE
        message_01F_1:
            db $FE
        message_01F_2:
            db $FE
        message_020_1:
            db $FE
        message_020_2:
            db $FE
        message_021_1:
            db $FE
        message_021_2:
            db $FE
        message_022_1:
            db "Trapped         in"
            db "the heat          "
            db "   Are servants of"
            db "      the pyramid."
            db "Those      grabbed"
            db "know defeat       "
            db "   Sweltering heat"
            db "   and death amid."
            db $FE
        message_022_2:
            db "When    the   moon"
            db "appears    and the"
            db "    sun leaves the"
            db "    day...        "
            db "Your         eerie"
            db "fears    will come"
            db "            out to"
            db "             play."
            db $FE
        message_023_1:
            db " -SWITCH  PALACE- "
            db " The power of the "
            db "switch you pressed"
            db "    has turned    "
            db "                  "
            db "      into     .  "
            db "Now get out of our"
            db "  temple NOWWWW!", $FE
        message_023_2:
            db " OoOoOoOoOoOoOoOo "
            db " Thank you Mario, "
            db " for pressing the "
            db "  ON-OFF switch.  "
            db " Now you'll never "
            db "escape this place."
            db "                  "
            db "LOWER CEILING NOW!"
            db "Oh,   hey   Mario."
            db "Us   archeologists"
            db "just   found  this"
            db "pyramid, and we're"
            db "real  busy digging"
            db "it  out. Feel free"
            db "to   go   in   and"
            db "explore, though.", $FE
        message_024_1:
            db $FE
        message_024_2:
            db $FE
        message_101_1:
            db $FE
        message_101_2:
            db $FE
        message_102_1:
            db "Oh,   hey   Mario."
            db "Us   archeologists"
            db "just   found  this"
            db "pyramid, and we're"
            db "real  busy digging"
            db "it  out. Feel free"
            db "to   go   in   and"
            db "explore, though.", $FE
        message_102_2:
            db "This place sure is"
            db "creepy... Everyone"
            db "here is too scared"
            db "to   go  past  the"
            db "first   room,  the"
            db "walls  says  there"
            db "are     mummies..."
            db "Evil cat mummies!", $FE
        message_103_1:
            db "He may have solved"
            db "that  one,  but he"
            db "won't  solve  this"
            db "one.   He'll  just"
            db "keep      circling"
            db "round   again  and"
            db "again.  Unless  he"
            db "goes this way ...", $FE
        message_103_2:
            db "So you figured the"
            db "way   out   Mario?"
            db "Whatever  you  do,"
            db "don't  touch  that"
            db "Rainbow  Block. It"
            db "will  only lead to"
            db "super fun times.  "
            db "           - TNK", $FE
        message_104_1:
            db $FE
        message_104_2:
            db $FE
        message_105_1:
            db "These   dark  blue"
            db "blocks  will  melt"
            db "when fire gets too"
            db "close   to   them!"
            db "Maybe  you  should"
            db "put  those L and R"
            db "buttons   to  some"
            db "use, at long last."
            db $FE
        message_105_2:
            db "Keep    the    top"
            db "fireball  with you"
            db "for a harder path,"
            db "but        special"
            db "surprise!    Also,"
            db "the  doors are all"
            db "reset doors if you"
            db "get stuck.", $FE
        message_106_1:
            db "The sand inside is"
            db "violent! We almost"
            db "lost  a  guy to it"
            db "yesterday,  he was"
            db "goofing  around on"
            db "those odd floating"
            db "blocks...  Why  do"
            db "they float?", $FE
        message_106_2:
            db "Hey,  no  powerups"
            db "except   mushrooms"
            db "here.   We   don't"
            db "wanna        break"
            db "anything! Also, it"
            db "seems   there  are"
            db "two       paths..."
            db "Hmmm...", $FE
        message_107_1:
            db "The sand inside is"
            db "violent! We almost"
            db "lost  a  guy to it"
            db "yesterday,  he was"
            db "goofing  around on"
            db "those odd floating"
            db "blocks...  Why  do"
            db "they float?", $FE
        message_107_2:
            db "Hey, we discovered"
            db "two  paths.  We're"
            db "still investgating"
            db "the upper path, so"
            db "mushrooms only. We"
            db "don't  wanna break"
            db "anything      that"
            db "could be valuable!"
            db $FE
        message_108_1:
            db $FE
        message_108_2:
            db $FE
        message_109_1:
            db $FE
        message_109_2:
            db $FE
        message_10A_1:
            db $FE
        message_10A_2:
            db $FE
        message_10B_1:
            db $FE
        message_10B_2:
            db $FE
        message_10C_1:
            db $FE
        message_10C_2:
            db $FE
        message_10D_1:
            db $FE
        message_10D_2:
            db $FE
        message_10E_1:
            db $FE
        message_10E_2:
            db $FE
        message_10F_1:
            db "Wait   a   second,"
            db "dude!  Before  you"
            db "go, take a look at"
            db "the  wall! I think"
            db "there   is  a  way"
            db "for  you to get up"
            db "there.", $FE
        message_10F_2:
            db "    -WARNING-     "
            db "                  "
            db "There  are quite a"
            db "few      dangerous"
            db "creatures   ahead."
            db "Stay away for your"
            db "sake!", $FE
        message_110_1:
            db "Step   right   up,"
            db "step", $35,"right", $36,$36,"up for"
            db "the", $35,"funnest   game"
            db "this  side  o' the"
            db "'Shroom   Kingdom!"
            db "Go   ahead,   kid."
            db "Give 'er a try and"
            db "see watcha win!", $FE
        message_110_2:
            db $FE
        message_111_1:
            db "-  SWITCH PALACE -"
            db "The  power  of the"
            db "switch  you   have"
            db "pushed  will  turn"
            db "                  "
            db "      into     .  "
            db "Aww...now we can't"
            db "have anymore fun!", $FE
        message_111_2:
            db "Aww,  it seems you"
            db "lost.  Don't worry"
            db "though,  you still"
            db "win  a trip to the"
            db "Muncher   pit,  on"
            db "the  house! Try to"
            db "play  better  next"
            db "time, though...", $FE
        message_112_1:
            db "Press  A  and Left"
            db "or  right  to move"
            db "to   another  cart"
            db "while  pressing  B"
            db "will make them all"
            db "jump based on what"
            db "cart        you're"
            db "currently inside.", $FE
        message_112_2:
            db "Light  up  all  of"
            db "the   switches  to"
            db "win  a  prize! Try"
            db "as   much  as  you"
            db "like  for  as long"
            db "as you want!", $FE
        message_113_1:
            db "Watch   out!   The"
            db "balloons  here can"
            db "be          pretty"
            db "unstable.  Try not"
            db "to  stand  on them"
            db "for       extended"
            db "periods of time.", $FE
        message_113_2:
            db $FE
        message_114_1:
            db $FE
        message_114_2:
            db $FE
        message_115_1:
            db "Welcome   to   the"
            db "carnival's    main"
            db "attraction,   also"
            db "known    as    the"
            db "greatest    bouncy"
            db "castle  ever. It's"
            db "sure  to  make you"
            db "JUMP for joy.", $FE
        message_115_2:
            db "(We    at   Norveg"
            db "Industries     are"
            db "sorry    for   the"
            db "quality   of   the"
            db "previous      pun."
            db "The   writer   has"
            db "suffered   a  most"
            db "horrible fate.)", $FE
        message_116_1:
            db $FE
        message_116_2:
            db $FE
        message_117_1:
            db $FE
        message_117_2:
            db $FE
        message_118_1:
            db "Jeptaks aim at you"
            db "with their arrows,"
            db "then  fly directly"
            db "at  you.  When you"
            db "hear           the"
            db "confirmation ding,"
            db "then they're gonna"
            db "fly! Avoid them!", $FE
        message_118_2:
            db $FE
        message_119_1:
            db $FE
        message_119_2:
            db $FE
        message_11A_1:
            db $FE
        message_11A_2:
            db $FE
        message_11B_1:
            db "The     creature's"
            db "wing    power   is"
            db "draining.   He  is"
            db "being  consumed by"
            db "the chapel itself."
            db "                  "
            db "Will  you  take it"
            db "on a final flight?"
            db $FE
        message_11B_2:
            db "PILOT  ACCESS CODE"
            db "ACCEPTED.   CHAPEL"
            db "TELEPORTATION WILL"
            db "COMMENCE  IN A FEW"
            db "MOMENTS.          "
            db "                  "
            db "BE     WARY     OF"
            db "SPIRITS.", $FE
        message_11C_1:
            db "    -NOTICE-      "
            db "                  "
            db " TRESPASSERS WILL "
            db " BE SHOT ON SIGHT "
            db "                  "
            db "  SURVIVORS WILL  "
            db "  BE PROSECUTED", $FE
        message_11C_2:
            db "Norveg  Industries"
            db "is   building  the"
            db "next    thing   in"
            db "relaxation! With a"
            db "breathtaking  view"
            db "and   ", $65,$67,"  swimming"
            db "pools,    Skypoint"
            db "Hotel opens soon!", $FE
        message_11D_1:
            db "Jump     into    a"
            db "Roviclaw  to start"
            db "it.  Jump again to"
            db "get  off early and"
            db "press  Up  or Down"
            db "on  the  D-Pad  to"
            db "move  the  claw to"
            db "avoid obstacles.", $FE
        message_11D_2:
            db "The   claw   stops"
            db "when   it  hits  a"
            db "wall. You can also"
            db "jump back into it,"
            db "and  if  you do so"
            db "after   it   stops"
            db "it'll  go back the"
            db "way it came!", $FE
        message_11E_1:
            db $FE
        message_11E_2:
            db $FE
        message_11F_1:
            db "Time to just relax"
            db "and  let  the wind"
            db "take  you where it"
            db "wants...", $FE
        message_11F_2:
            db "Hmmm...This   door"
            db "appears    to   be"
            db "locked. There must"
            db "be a key somewhere"
            db "around here...", $FE
        message_120_1:
            db $FE
        message_120_2:
            db $FE
        message_121_1:
            db "- SWITCH PALACE - "
            db "The  power  of the"
            db "switch  you   have"
            db "pushed  will  turn"
            db "                  "
            db "      into     .  "
            db "Your progress will"
            db "also   be   saved."
            db "                  "
            db $FE
        message_121_2:
            db "                  "
            db "Out    for    gas,"
            db "will be back soon."
            db "                  "
            db "                  "
            db "                  "
            db "     - Blimp Duck", $FE
        message_122_1:
            db "Watch  your  step!"
            db "The   glowing  red"
            db "lava on the ground"
            db "is as dangerous as"
            db "the ordinary lava,"
            db "so steer clear.", $FE
        message_122_2:
            db $FE
        message_123_1:
            db "This  door  cannot"
            db "be  opened  unless"
            db "you   activate  it"
            db "with   a   certain"
            db "switch.           "
            db "To  proceed,  just"
            db "climb onto the net"
            db "above!", $FE
        message_123_2:
            db "The    ground   is"
            db "shaking...        "
            db "                  "
            db "Maybe  you  should"
            db "hurry  out of here"
            db "before   it's  too"
            db "late.", $FE
        message_124_1:
            db $FE
        message_124_2:
            db "Here    lies   the"
            db "remants   of   the"
            db "author  before  he"
            db "moved   away  from"
            db "FPZero    to    be"
            db "neighbors     with"
            db "Pikerchu instead. "
            db "   - THE END -", $FE
        message_125_1:
            db $FE
        message_125_2:
            db $FE
        message_126_1:
            db "Ice   skating   is"
            db "fun!  Turning  can"
            db "be a bit difficult"
            db "and you can't turn"
            db "while  jumping  so"
            db "be        careful."
            db "Good", $35,"luck!        "
            db "...You'll need it."
            db $FE
        message_126_2:
            db $FE
        message_127_1:
            db $FE
        message_127_2:
            db $FE
        message_128_1:
            db $FE
        message_128_2:
            db $FE
        message_129_1:
            db "Throughout    this"
            db "part of the frozen"
            db "ruins,        some"
            db "of  the ice blocks"
            db "have  shown  to be"
            db "passable     after"
            db "performing       a"
            db "special jump.", $FE
        message_129_2:
            db $FE
        message_12A_1:
            db "Switching  heat on"
            db "and  off  can turn"
            db "lava  into  rocks,"
            db "ice   into  water,"
            db "and  can even make"
            db "chains  too hot to"
            db "climb as well.", $FE
        message_12A_2:
            db "-NOTE TO REMEMBER-"
            db "First,   it   goes"
            db "HIT    MISS   HIT."
            db "But,  then it goes"
            db "MISS   HIT   MISS."
            db "I've     left    a"
            db "subtle   hint   in"
            db "case I forget.", $FE
        message_12B_1:
            db "Welcome, mortal..."
            db "                  "
            db "Norveg   abandoned"
            db "us...  but you too"
            db "shall   join   our"
            db "ranks    of    the"
            db "forgotten!", $FE
        message_12B_2:
            db "   -SECTOR ", $6B,"A-    "
            db "                  "
            db "SHUT  DOWN  DUE TO"
            db "HAZARDOUS WORKING "
            db "   ENVIRONMENT.   "
            db "                  "
            db "DO NOT ENTER FOR  "
            db "   ANY REASON!", $FE
        message_12C_1:
            db " -INTRUDER ALERT- "
            db "                  "
            db "Factory   lockdown"
            db "initiated.        "
            db "                  "
            db "Combat       squad"
            db "Kappa-", $66,"   deployed"
            db "to main entrance.", $FE
        message_12C_2:
            db "- NOTE TO STAFF - "
            db "                  "
            db "Your  mother  does"
            db "NOT  work  here  -"
            db "throw your garbage"
            db "in the incinerator"
            db "yourself,     will"
            db "you?", $FE
        message_12D_1:
            db "The  oil  in  this"
            db "area  is extremely"
            db "thick, thus making"
            db "you  sink  slowly."
            db "It's  a  lot  like"
            db "quicksand,  you've"
            db "encountered  quick"
            db "sand before right?"
            db $FE
        message_12D_2:
            db $FE
        message_12E_1:
            db "It  seems  one  of"
            db "Norveg's   minions"
            db "left   a   jetpack"
            db "just  lying around"
            db "with barely enough"
            db "fuel to get out of"
            db "here. Press (B) in"
            db "midair to fly.", $FE
        message_12E_2:
            db "NOTICE-    Jumping"
            db "w", $A6,$A7,$A8," crate lifting"
            db "has  been shown to"
            db "cause  back  pain."
            db "We'd like to avoid"
            db "financial   damage"
            db "from  lawsuits, so"
            db "please avoid it.", $FE
        message_12F_1:
            db $FE
        message_12F_2:
            db $FE
        message_130_1:
            db $FE
        message_130_2:
            db $FE
        message_131_1:
            db "Four paths o' fun."
            db "Which will you    "
            db "          choose? "
            db "                  "
            db "                  "
            db "Conquer  each one,"
            db "If with me you'd  "
            db "         schmooze."
            db $FE
        message_131_2:
            db "Proceed       with"
            db "caution!   For  he"
            db "who   defieth  the"
            db "law   of   gravity"
            db "confuseth up  with"
            db "down   and   vice-"
            db "versa!  Get stuck,"
            db "bubala? Reset!", $FE
        message_132_1:
            db "Solve     my     ", $67
            db "perilous  paths if"
            db "you     wish    to"
            db "proceed.     Prove"
            db "yourself worthy of"
            db "battle!", $FE
        message_132_2:
            db "Proceed       with"
            db "caution!   For  he"
            db "who defies the law"
            db "of   gravity  will"
            db "confuse   up  with"
            db "down   and   vice-"
            db "versa!         Get"
            db "stumped? Reset!", $FE
        message_133_1:
            db "In   this   world,"
            db "everything      is"
            db "inverted.  If  you"
            db "want  to move, aim"
            db "for  the  opposite"
            db "direction.", $FE
        message_133_2:
            db $FE
        message_134_1:
            db $FE
        message_134_2:
            db $FE
        message_135_1:
            db $FE
        message_135_2:
            db $FE
        message_136_1:
            db "...You  should  be"
            db "sleeping   in  the"
            db "flowers.", $FE
        message_136_2:
            db "Softly, now.", $FE
        message_137_1:
            db "Beware  of the Red"
            db "Crystals,  as they"
            db "will        launch"
            db "fireballs       at"
            db "you. Touching them"
            db "is             not"
            db "recommended.", $FE
        message_137_2:
            db $FE
        message_138_1:
            db "There are power-up"
            db "bells  ahead which"
            db "act   as   follows"
            db "Yellow    ", $68,"  Coins"
            db "Red       Mushroom"
            db "Green       Flower"
            db "Blue       Feather"
            db "Multicolored  ", $64,"-UP"
            db $FE
        message_138_2:
            db $FE
        message_139_1:
            db $FE
        message_139_2:
            db $FE
        message_13A_1:
            db "In  the next pipe,"
            db "there's a bunch of"
            db "letters.   Somehow"
            db "Norveg was able to"
            db "move   the   green"
            db "ones    with   his"
            db "head? How exactly,"
            db "I don't know.", $FE
        message_13A_2:
            db "Somehow Norveg was"
            db "able  to  create a"
            db "rift  in  the time"
            db "continuum       by"
            db "pushing  things to"
            db "their      colored"
            db "boxes. Hmm ...", $FE
        message_13B_1:
            db $FE
        message_13B_2:
            db $FE

org $03BE80
    dw message_000_1-messages
    dw message_000_2-messages
    dw message_001_1-messages
    dw message_001_2-messages
    dw message_002_1-messages
    dw message_002_2-messages
    dw message_003_1-messages
    dw message_003_2-messages
    dw message_004_1-messages
    dw message_004_2-messages
    dw message_005_1-messages
    dw message_005_2-messages
    dw message_006_1-messages
    dw message_006_2-messages
    dw message_007_1-messages
    dw message_007_2-messages
    dw message_008_1-messages
    dw message_008_2-messages
    dw message_009_1-messages
    dw message_009_2-messages
    dw message_00A_1-messages
    dw message_00A_2-messages
    dw message_00B_1-messages
    dw message_00B_2-messages
    dw message_00C_1-messages
    dw message_00C_2-messages
    dw message_00D_1-messages
    dw message_00D_2-messages
    dw message_00E_1-messages
    dw message_00E_2-messages
    dw message_00F_1-messages
    dw message_00F_2-messages
    dw message_010_1-messages
    dw message_010_2-messages
    dw message_011_1-messages
    dw message_011_2-messages
    dw message_012_1-messages
    dw message_012_2-messages
    dw message_013_1-messages
    dw message_013_2-messages
    dw message_014_1-messages
    dw message_014_2-messages
    dw message_015_1-messages
    dw message_015_2-messages
    dw message_016_1-messages
    dw message_016_2-messages
    dw message_017_1-messages
    dw message_017_2-messages
    dw message_018_1-messages
    dw message_018_2-messages
    dw message_019_1-messages
    dw message_019_2-messages
    dw message_01A_1-messages
    dw message_01A_2-messages
    dw message_01B_1-messages
    dw message_01B_2-messages
    dw message_01C_1-messages
    dw message_01C_2-messages
    dw message_01D_1-messages
    dw message_01D_2-messages
    dw message_01E_1-messages
    dw message_01E_2-messages
    dw message_01F_1-messages
    dw message_01F_2-messages
    dw message_020_1-messages
    dw message_020_2-messages
    dw message_021_1-messages
    dw message_021_2-messages
    dw message_022_1-messages
    dw message_022_2-messages
    dw message_023_1-messages
    dw message_023_2-messages
    dw message_024_1-messages
    dw message_024_2-messages
    dw message_101_1-messages
    dw message_101_2-messages
    dw message_102_1-messages
    dw message_102_2-messages
    dw message_103_1-messages
    dw message_103_2-messages
    dw message_104_1-messages
    dw message_104_2-messages
    dw message_105_1-messages
    dw message_105_2-messages
    dw message_106_1-messages
    dw message_106_2-messages
    dw message_107_1-messages
    dw message_107_2-messages
    dw message_108_1-messages
    dw message_108_2-messages
    dw message_109_1-messages
    dw message_109_2-messages
    dw message_10A_1-messages
    dw message_10A_2-messages
    dw message_10B_1-messages
    dw message_10B_2-messages
    dw message_10C_1-messages
    dw message_10C_2-messages
    dw message_10D_1-messages
    dw message_10D_2-messages
    dw message_10E_1-messages
    dw message_10E_2-messages
    dw message_10F_1-messages
    dw message_10F_2-messages
    dw message_110_1-messages
    dw message_110_2-messages
    dw message_111_1-messages
    dw message_111_2-messages
    dw message_112_1-messages
    dw message_112_2-messages
    dw message_113_1-messages
    dw message_113_2-messages
    dw message_114_1-messages
    dw message_114_2-messages
    dw message_115_1-messages
    dw message_115_2-messages
    dw message_116_1-messages
    dw message_116_2-messages
    dw message_117_1-messages
    dw message_117_2-messages
    dw message_118_1-messages
    dw message_118_2-messages
    dw message_119_1-messages
    dw message_119_2-messages
    dw message_11A_1-messages
    dw message_11A_2-messages
    dw message_11B_1-messages
    dw message_11B_2-messages
    dw message_11C_1-messages
    dw message_11C_2-messages
    dw message_11D_1-messages
    dw message_11D_2-messages
    dw message_11E_1-messages
    dw message_11E_2-messages
    dw message_11F_1-messages
    dw message_11F_2-messages
    dw message_120_1-messages
    dw message_120_2-messages
    dw message_121_1-messages
    dw message_121_2-messages
    dw message_122_1-messages
    dw message_122_2-messages
    dw message_123_1-messages
    dw message_123_2-messages
    dw message_124_1-messages
    dw message_124_2-messages
    dw message_125_1-messages
    dw message_125_2-messages
    dw message_126_1-messages
    dw message_126_2-messages
    dw message_127_1-messages
    dw message_127_2-messages
    dw message_128_1-messages
    dw message_128_2-messages
    dw message_129_1-messages
    dw message_129_2-messages
    dw message_12A_1-messages
    dw message_12A_2-messages
    dw message_12B_1-messages
    dw message_12B_2-messages
    dw message_12C_1-messages
    dw message_12C_2-messages
    dw message_12D_1-messages
    dw message_12D_2-messages
    dw message_12E_1-messages
    dw message_12E_2-messages
    dw message_12F_1-messages
    dw message_12F_2-messages
    dw message_130_1-messages
    dw message_130_2-messages
    dw message_131_1-messages
    dw message_131_2-messages
    dw message_132_1-messages
    dw message_132_2-messages
    dw message_133_1-messages
    dw message_133_2-messages
    dw message_134_1-messages
    dw message_134_2-messages
    dw message_135_1-messages
    dw message_135_2-messages
    dw message_136_1-messages
    dw message_136_2-messages
    dw message_137_1-messages
    dw message_137_2-messages
    dw message_138_1-messages
    dw message_138_2-messages
    dw message_139_1-messages
    dw message_139_2-messages
    dw message_13A_1-messages
    dw message_13A_2-messages
    dw message_13B_1-messages
    dw message_13B_2-messages