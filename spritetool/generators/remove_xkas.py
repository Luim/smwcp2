import os
for i in [__ for __ in os.listdir(".") if __.endswith(".cfg")]:
    with open(i,"r") as infile:
        cfg_data = infile.read().split("\n")
    try:
        uses_xkas = int(cfg_data[-1].strip()) == 1
        asm_filename = cfg_data[-2].strip()
    except:
        uses_xkas = False
        asm_filename = cfg_data[-1].strip()

    if uses_xkas:
        print "removing: {}, {}".format(i, asm_filename)
        try:
            os.remove(i)
        except Exception as e:
            print e
        try:
            os.remove(asm_filename)
        except:
            pass