!norvegState = !actorVar1,x
!norvegXPos = !actorVar2,x
!norvegYPos = !actorVar3,x
!norvegTimer = !actorVar4,x

SetSpeakerToNorveg:
	LDX !norvegIndex
	STX !currentSpeaker
	RTL

NorvegSetToNormal:
	LDX !norvegIndex
	LDA #$00
	STA !norvegState
	RTL

NorvegSetToTalking:
	LDX !norvegIndex
	LDA #$01
	STA !norvegState
	RTL

NorvegSetToRobotReveal:
	LDX !norvegIndex
	LDA #$02
	STA !norvegState
	lda #$5B
	sta !norvegTimer
	JSL MarioSetToRobotReveal
	RTL

NorvegSetYPos:
	ldx !norvegIndex
	sta !norvegYPos
	rtl

NorvegSetXPos:
	ldx !norvegIndex
	sta !norvegXPos
	rtl

	
NorvegCreate:
	REP #$20
	LDA.w #NorvegINIT
	STA $00
	LDX.b #NorvegINIT>>16
	STX $02
	
	LDA.w #NorvegMAIN
	STA $03
	LDX.b #NorvegMAIN>>16
	STX $05
	
	LDA.w #NorvegNMI
	STA $06
	LDX.b #NorvegNMI>>16
	STX $08
	SEP #$20
	JSL NewActor
	STX !norvegIndex
	RTL

NorvegINIT:
	lda #$00
	sta !norvegState
	;LDA #$0F
	;STA !norvegYPos
	;LDA #$6F
	;STA !norvegXPos
	RTL

NorvegMAIN:
	
	PHK
	PLB

	LDA #$32 : STA $03
	
	LDA !norvegXPos : STA $00
	LDA !norvegYPos : STA $01
	LDA #$80 : STA $02
	LDA #$02 : JSL DrawTile
	
	LDA !norvegXPos : CLC : ADC #$08 : STA $00
	LDA !norvegYPos : STA $01
	LDA #$81 : STA $02
	LDA #$02 : JSL DrawTile
	
	LDA !norvegXPos : STA $00
	LDA !norvegYPos : CLC : ADC #$10 : STA $01
	LDA #$A0 : STA $02
	LDA #$02 : JSL DrawTile
	
	LDA !norvegXPos : CLC : ADC #$08 : STA $00
	LDA !norvegYPos : CLC : ADC #$10 : STA $01
	LDA #$A1 : STA $02
	LDA #$02 : JSL DrawTile
	
	LDA !norvegXPos : STA $00
	LDA !norvegYPos : CLC : ADC #$20 : STA $01
	LDA #$C0 : STA $02
	LDA #$02 : JSL DrawTile
	
	LDA !norvegXPos : CLC : ADC #$08 : STA $00
	LDA !norvegYPos : CLC : ADC #$20 : STA $01
	LDA #$C1 : STA $02
	LDA #$02 : JSL DrawTile

	PHX				; Jump to Norveg's current routine.
	phk
	pla
	sta $02
	LDA !norvegState		;
	ASL
	TAX
	LDA NorvegStateTable,x		;
	STA $00				;
	LDA NorvegStateTable+1,x	;
	STA $01				;
	PLX				;
	PEA.w .ret1-1			;
	JMP [$0000]			;
.ret1	

	
	RTL

NorvegNMI:
	RTL
	
NorvegStateTable:
	dw NorvegNormal, NorvegTalking, NorvegRobotReveal

NorvegNormal:
{
	rts
}
	
NorvegTalking:
{
	rts
}

NorvegRobotReveal:
{
	lda $13
	and #$02
	beq .dont_fade
	php
	lda !norvegTimer
	dec
	sta !norvegTimer
	lda !norvegYPos
	inc
	sta !norvegYPos
	lda !backgroundColor
	beq +
	dec !backgroundColor
	dec !backgroundColor+1
	dec !backgroundColor+2
	+
	dec $1C
	dec $20
	plp
	.dont_fade
	lda !norvegTimer
	bne +
	JSL NorvegSetToNormal
	JSL MarioSetWaitStateToNothing
	+
	rts
}
