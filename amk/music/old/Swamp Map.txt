;================================;
; SMWCP2: Swamp Map Theme        ;
; ~Red Chameleon                 ;
;   Song Duration: 0:31          ;
;   Insert Size: 353 (851) bytes ;
;   Number of Channels: 8        ;
;================================;

#SPC
{
	#author "Red Chameleon"
	#comment "Forest/Swamp map theme"
	#game "SMW Central Production 2"
}

#pad $0364

#samples
{
	#default
	#AMM
	#main_map
}

#instruments
{
	@5	$AF $00 $B8 $07 $00
}

;===============;
;Global Commands;
;===============;

#0 w230 t74 $F0


;==========;
;Channel #0;
;==========;

#0 @3 $ED $5E $C8

[r1]4

o3 l8 v248 y8

(1)[ccc4<a+4>d+d+4d+4c<a+4>c4]1
ccc<a+>c4fd+f4<a+4>c4<a+4>
(1)1c4^<a+>c4c+c<g+4a+4f2

o6 l16

(2)[v170 $E8 $C0 $40
y10g+y13fy10c+y7<a+>
y10g+y13fy10c+y7<a+>
y10g+y13fy10c+y7<a+>
y10g+y13fy10c+y7<a+>
r1

v170 $E8 $C0 $40
y10f+y13d+y10cy7<g+>
y10f+y13d+y10cy7<g+>
y10f+y13d+y10cy7<g+>
y10f+y13d+y10cy7<g+>
r1]1

(2)1

l8 y15

(3)[v210 $E8 $C0 $40
d+d+d+d+d+d+d+d+ r1]1

y5

(3)1

;==========;
;Channel #1;
;==========;

#1 @12 v211 y9

o3 l4

[b8b8bd8bddd8gdb8b8bd8bdgg8bg]6


;==========;
;Channel #2;
;==========;

#2 @8 v220 y12

o2

(4)[c4c8c4^8c4d+8]1c+4(5)[c8c2c4c8c4^8]1<a+8>(6)[d+4^8f4f8d+8c4]1
(4)1c+4(5)1f8(6)1
(4)1f4(5)1<a+8>d+4^8f4<a+8>c8c4
(7)[c4c8c4^8c4d+8d+4d+8c4d+4<a+4a+8a+4^8>c8c2c8c+4c4]1
c4c8c4^8c4d+8 d+4d+8c4d+4c+4c+8c+4^8<a+8a+2>c+8c+4c+4
(7)1


;==========;
;Channel #3;
;==========;

#3 @6 $ED $2C $EA v124 y12

[r1]4

o3

(8)[f1^1d+1^2]1g+4g4
(8)1c+4d+4
f1^1
d+1^1
f1^1
g+1^1
[d+1^1]2


;==========;
;Channel #7;
;==========;

#7 @30 v236 y14

o4

[r1]12

(9)[d+4^8c+8d+4^8g+4g+4c+8d+4]1c+4
d+4^8c+8d+8c+4<a+4a+4>c+8d+4<a+4>
(9)1a+4
g+8g8d+4d+4d+8c+8d+1

[r1]4

;==========;
;Channel #5;
;==========;

#5 v209 y8

o3

(10)[@21c8@21c8@21c4@21c4@21c4^8@21c4^8@21c8@21c8@21c4
@21c8@21c8@21c4@21c4@21c4^8@21c4^8@21c8@21c8@29c4]1

[@21c4@21c2@21c8@21c8@29c4@21c8@21c4@21c8@21c4
@21c4^8@21c8@21c4@21c8@21c8@29c8@21c8@21c2@21c4]4

(10)1


;==========;
;Channel #6;
;==========;

#6 v209 y8

o3

(11)[ @22c2 @22c4^8 @22c8 @22c2 @22c4 @22c4]2

[@22c4 @22c4 @22c8 @22c8 @22c4 @22c4 @22c16 @22c16 @22c8 @22c4 @22c4 @22c8
 @22c8 @22c4 @22c4 @22c4 @22c16 @22c16 @22c8 @22c4 @22c16 @22c16 @22c8 @22c4]4
(11)2


;==========;
;Channel #4;
;==========;

#4 @6 $ED $2C $EA v124 y10

[r1]4

o3

a+1^1
g+1^1
[a+1^1]2
>c1^1<
a+1^1
>c1^1
d+1^1<
[a+1^1]2


;===============;
;  END OF SONG  ;
;===============;                

#amk 1
