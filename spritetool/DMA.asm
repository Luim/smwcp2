
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

INCSRC h_DEFINE.asm

MACRO RATS_TAG(FREE, START, END)
ORG <FREE>
db "STAR"
dw <END>-<START>-$01
dw <END>-<START>-$01^$FFFF
ENDMACRO

MACRO VALUE(NUM)
!TEMP = <NUM>-$2000+8
dl !TEMP
ENDMACRO

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ORG $00A300	;HIJACK
PHB
JSL CODE_START
PLB
RTS

;;;;;;;;;;;;;;;;

%RATS_TAG(!FREESPACE_DMA, START_DMA, END_DMA)
START_DMA:

GFX_TABLE:
%VALUE(!ROM1)

CODE_START:

%DEBUG(49)

REP #$20
PHK
PLB

LDA !FLAG
BNE MAIN_CODE
JMP ORIGINAL_CODE_MODIFIED_SOURCE

MAIN_CODE:
LDA $0D85+0
CLC
ADC GFX_TABLE
STA !EXTRA_PTR+0
LDA $0D85+2
CLC
ADC GFX_TABLE
STA !EXTRA_PTR+2
LDA $0D85+$A
CLC
ADC GFX_TABLE
STA !EXTRA_PTR+4
LDA $0D85+$C
CLC
ADC GFX_TABLE
STA !EXTRA_PTR+6

MAIN_DMA:

WCODE_00A302:	LDX #$04                ; We're using DMA channel 2 
WCODE_00A304:	LDY $0D84               
WCODE_00A307:	BEQ WCODE_00A328           
WCODE_00A309:	LDY #$86                ; \ Set Address for CG-RAM Write to x86 
WCODE_00A30B:	STY $2121               ; / ; Address for CG-RAM Write
WCODE_00A30E:	LDA #$2200              
WCODE_00A311:	STA $4320               ; Parameters for DMA Transfer
WCODE_00A314:	LDA $0D82               ; \ Get location of palette from $0D82-$0D83 
WCODE_00A317:	STA $4322               ; / ; A Address (Low Byte)
WCODE_00A31A:	LDY #$00                ; \ Palette is stored in bank x00 
WCODE_00A31C:	STY $4324               ; / ; A Address Bank
WCODE_00A31F:	LDA #$0014              ; \ x14 bytes will be transferred 
WCODE_00A322:	STA $4325               ; / ; Number Bytes to Transfer (Low Byte) (DMA)
WCODE_00A325:	STX $420B               ; Transfer the colors ; Regular DMA Channel Enable
WCODE_00A328:	LDY #$80                ; \ Set VRAM Address Increment Value to x80 
WCODE_00A32A:	STY $2115               ; / ; VRAM Address Increment Value
WCODE_00A32D:	LDA #$1801              
WCODE_00A330:	STA $4320               ; Parameters for DMA Transfer
WCODE_00A333:	LDA #$67F0              
WCODE_00A336:	STA $2116               ; Address for VRAM Read/Write (Low Byte)
WCODE_00A339:	LDA #$8208			;SINGLE TILE?
WCODE_00A33C:	STA $4322               ; A Address (Low Byte)

	LDY GFX_TABLE+2

	LDA $0D84
	BNE NOT_ZERO
	SEP #$20
	RTL

NOT_ZERO:

WCODE_00A341:	STY $4324               ; / ; A Address Bank
WCODE_00A344:	LDA #$0020              ; \ x20 bytes will be transferred 
WCODE_00A347:	STA $4325               ; / ; Number Bytes to Transfer (Low Byte) (DMA)
WCODE_00A34A:	STX $420B               ; Transfer ; Regular DMA Channel Enable

WCODE_00A34D:	LDA #$6000              ; \ Set Address for VRAM Read/Write to x6000

WCODE_00A350:	STA $2116               ; / ; Address for VRAM Read/Write (Low Byte)
WCODE_00A353:	LDX #$00                

WCODE_00A355:	LDA !EXTRA_PTR,X             ; \ Get address of graphics to copy 

WCODE_00A358:	STA $4322               ; / ; A Address (Low Byte)
WCODE_00A35B:	LDA #$0040              ; \ x40 bytes will be transferred 
WCODE_00A35E:	STA $4325               ; / ; Number Bytes to Transfer (Low Byte) (DMA)
WCODE_00A361:	LDY #$04                ; \ Transfer 
WCODE_00A363:	STY $420B               ; / ; Regular DMA Channel Enable
WCODE_00A366:	INX                       ; \ Move to next address 
WCODE_00A367:	INX                       ; /  
WCODE_00A368:	CPX #$04               ; \ Repeat last segment while X<$0D84 (#$0A, #$06, OR #$00)
WCODE_00A36B:	BCC WCODE_00A355           ; /  
WCODE_00A36D:	LDA #$6100              ; \ Set Address for VRAM Read/Write to x6100 
WCODE_00A370:	STA $2116               ; / ; Address for VRAM Read/Write (Low Byte)
WCODE_00A373:	LDX #$00                

WCODE_00A375:	LDA !EXTRA_PTR+4,X             ; \ Get address of graphics to copy 

WCODE_00A378:	STA $4322               ; / ; A Address (Low Byte)
WCODE_00A37B:	LDA #$0040              ; \ x40 bytes will be transferred 
WCODE_00A37E:	STA $4325               ; / ; Number Bytes to Transfer (Low Byte) (DMA)
WCODE_00A381:	LDY #$04                ; \ Transfer 
WCODE_00A383:	STY $420B               ; / ; Regular DMA Channel Enable
WCODE_00A386:	INX                       ; \ Move to next address 
WCODE_00A387:	INX                       ; /  
WCODE_00A388:	CPX #$04               ; \ Repeat last segment while X<$0D84 
WCODE_00A38B:	BCC WCODE_00A375           ; /  

SHADOW_DMA:

QCODE_00A34D:	LDA #$6200              ; \ Set Address for VRAM Read/Write to x6000
 
QCODE_00A350:	STA $2116               ; / ; Address for VRAM Read/Write (Low Byte)
QCODE_00A353:	LDX #$00                

QCODE_00A355:	LDA !CUR_PTR,X             ; \ Get address of graphics to copy 
	CLC
	ADC GFX_TABLE

QCODE_00A358:	STA $4322               ; / ; A Address (Low Byte)
QCODE_00A35B:	LDA #$0040              ; \ x40 bytes will be transferred 
QCODE_00A35E:	STA $4325               ; / ; Number Bytes to Transfer (Low Byte) (DMA)
QCODE_00A361:	LDY #$04                ; \ Transfer 
QCODE_00A363:	STY $420B               ; / ; Regular DMA Channel Enable
QCODE_00A366:	INX                       ; \ Move to next address 
QCODE_00A367:	INX                       ; /  
QCODE_00A368:	CPX #$04               ; \ Repeat last segment while X<$0D84 (#$0A, #$06, OR #$00)
QCODE_00A36B:	BCC QCODE_00A355           ; /  

QCODE_00A36D:	LDA #$6300              ; \ Set Address for VRAM Read/Write to x6100 

QCODE_00A370:	STA $2116               ; / ; Address for VRAM Read/Write (Low Byte)
QCODE_00A373:	LDX #$00                

QCODE_00A375:	LDA !CUR_PTR+4,X             ; \ Get address of graphics to copy 
	CLC
	ADC GFX_TABLE

QCODE_00A378:	STA $4322               ; / ; A Address (Low Byte)
QCODE_00A37B:	LDA #$0040              ; \ x40 bytes will be transferred 
QCODE_00A37E:	STA $4325               ; / ; Number Bytes to Transfer (Low Byte) (DMA)
QCODE_00A381:	LDY #$04                ; \ Transfer 
QCODE_00A383:	STY $420B               ; / ; Regular DMA Channel Enable
QCODE_00A386:	INX                       ; \ Move to next address 
QCODE_00A387:	INX                       ; /  
QCODE_00A388:	CPX #$04               ; \ Repeat last segment while X<$0D84 
QCODE_00A38B:	BCC QCODE_00A375           ; /  
QCODE_00A38D:	SEP #$20                  ; 8 bit A ; Accum (8 bit) 
	RTL

ORIGINAL_CODE_MODIFIED_SOURCE:

;MarioGFXDMA:	REP #$20                  ; 16 bit A ; Accum (16 bit) 
CODE_00A302:	LDX #$04                ; We're using DMA channel 2 
CODE_00A304:	LDY $0D84               
CODE_00A307:	BEQ CODE_00A328           
CODE_00A309:	LDY #$86                ; \ Set Address for CG-RAM Write to x86 
CODE_00A30B:	STY $2121               ; / ; Address for CG-RAM Write
CODE_00A30E:	LDA #$2200              
CODE_00A311:	STA $4320               ; Parameters for DMA Transfer
CODE_00A314:	LDA $0D82               ; \ Get location of palette from $0D82-$0D83 
CODE_00A317:	STA $4322               ; / ; A Address (Low Byte)
CODE_00A31A:	LDY #$00                ; \ Palette is stored in bank x00 
CODE_00A31C:	STY $4324               ; / ; A Address Bank
CODE_00A31F:	LDA #$0014              ; \ x14 bytes will be transferred 
CODE_00A322:	STA $4325               ; / ; Number Bytes to Transfer (Low Byte) (DMA)
CODE_00A325:	STX $420B               ; Transfer the colors ; Regular DMA Channel Enable
CODE_00A328:	LDY #$80                ; \ Set VRAM Address Increment Value to x80 
CODE_00A32A:	STY $2115               ; / ; VRAM Address Increment Value
CODE_00A32D:	LDA #$1801              
CODE_00A330:	STA $4320               ; Parameters for DMA Transfer
CODE_00A333:	LDA #$67F0              
CODE_00A336:	STA $2116               ; Address for VRAM Read/Write (Low Byte)
CODE_00A339:	LDA #$8208			;SINGLE TILE?
CODE_00A33C:	STA $4322               ; A Address (Low Byte)

; GET BANK OF DATA FROM TABLE ENTRY
	LDY GFX_TABLE+2

CODE_00A341:	STY $4324               ; / ; A Address Bank
CODE_00A344:	LDA #$0020              ; \ x20 bytes will be transferred 
CODE_00A347:	STA $4325               ; / ; Number Bytes to Transfer (Low Byte) (DMA)
CODE_00A34A:	STX $420B               ; Transfer ; Regular DMA Channel Enable
CODE_00A34D:	LDA #$6000              ; \ Set Address for VRAM Read/Write to x6000 
CODE_00A350:	STA $2116               ; / ; Address for VRAM Read/Write (Low Byte)
CODE_00A353:	LDX #$00                

CODE_00A355:	LDA $0D85,X             ; \ Get address of graphics to copy 
	CLC
	ADC GFX_TABLE

CODE_00A358:	STA $4322               ; / ; A Address (Low Byte)
CODE_00A35B:	LDA #$0040              ; \ x40 bytes will be transferred 
CODE_00A35E:	STA $4325               ; / ; Number Bytes to Transfer (Low Byte) (DMA)
CODE_00A361:	LDY #$04                ; \ Transfer 
CODE_00A363:	STY $420B               ; / ; Regular DMA Channel Enable
CODE_00A366:	INX                       ; \ Move to next address 
CODE_00A367:	INX                       ; /  
CODE_00A368:	CPX $0D84               ; \ Repeat last segment while X<$0D84 (#$0A, #$06, OR #$00)
CODE_00A36B:	BCC CODE_00A355           ; /  
CODE_00A36D:	LDA #$6100              ; \ Set Address for VRAM Read/Write to x6100 
CODE_00A370:	STA $2116               ; / ; Address for VRAM Read/Write (Low Byte)
CODE_00A373:	LDX #$00                

CODE_00A375:	LDA $0D85+$A,X             ; \ Get address of graphics to copy 
	CLC
	ADC GFX_TABLE

CODE_00A378:	STA $4322               ; / ; A Address (Low Byte)
CODE_00A37B:	LDA #$0040              ; \ x40 bytes will be transferred 
CODE_00A37E:	STA $4325               ; / ; Number Bytes to Transfer (Low Byte) (DMA)
CODE_00A381:	LDY #$04                ; \ Transfer 
CODE_00A383:	STY $420B               ; / ; Regular DMA Channel Enable
CODE_00A386:	INX                       ; \ Move to next address 
CODE_00A387:	INX                       ; /  
CODE_00A388:	CPX $0D84               ; \ Repeat last segment while X<$0D84 
CODE_00A38B:	BCC CODE_00A375           ; /  
CODE_00A38D:	SEP #$20                  ; 8 bit A ; Accum (8 bit) 
	RTL

END_DMA:

%RATS_TAG(!ROM1, oneSeven_START, oneSeven_END)
oneSeven_START:
INCBIN STUFF\SHYGUY.bin
oneSeven_END:
