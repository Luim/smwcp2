!ram = $18A6
;!sprnumber = $B9 ;insert number
!hp = $05
!sfx = $28 ;when boss is hit/hurt
!bank = $1DFC

incsrc subroutinedefs_xkas.asm

print "INIT ",pc
Init:
	phb : phk : plb
	stz $1510,x	;eye should close flag
	stz $157C,x ;not direction
	stz $C2,x
	lda $1686
	ora #$09
	sta $1686 ;tweaker
	lda $166E,x
	ora #$30
	sta $166E,x
	lda $167A,x
	ora #$02
	sta $167A,x
	lda $7FAB10,x
	and #$04
	beq .notset
	;lda $1602,x
	;bne .monty_init
	;lda #$2B
	;sta $1662,x
	jsl $01ACF9
	lda $148D
	and #$03
	sta $1FD6,x
	lda !ram
	ora #$80
	sta !ram
	lda #$30
	sta $163E,x
	plb : rtl
.notset
	lda #$70
	sta $1504,x ;bobombs?
	lda #$70 ;78
	sta $E4,x
	stz $14E0,x
	lda #$20
	sta $D8,x
	lda #$01
	sta $14D4,x
	plb : rtl
.monty_init
	lda #$40
	sta $1540,x
	plb : rtl
	
print "MAIN ",pc
Main:
	phb : phk : plb
	lda $7FAB10,x
	and #$04
	bne .sub
	lda $1504,x
	beq +
	dec $1504,x
	+ jsr Code
	- plb : rtl
.sub
;	lda $1602,x
;	bne +
	jsr SubCode
	bra -
;	+ jsr MontyMain
;	bra -
Code:
	jsr SUB_OFF_SCREEN_X0
	jsr Graphics
	lda $9D
	bne +
	;lda $7FFFFF
	stz $B6,x
	stz $AA,x
	jsl $01802A
	lda $1528,x
	cmp #!hp
	bcs .kill
	
	lda $1510,x	;dont hurt tree if eye is closed
	bne +
	
	jsr HurtTree
	+
	
	

	jsr SpawnBomb
	lda $14
	bne +
	lda !ram ;d7 set = dont spawn vine
	bmi + ;bmi?
	jsr Spawn
	;jsl $01A7DC ;changed, tweaker disable interaction with mario?
	;bcc +
	;jsl $00F606
	.end
	+ rts
.kill 
	lda #$3F
	sta $1887 ;shake ground ?
	lda #$18 ;thunder sfx
	sta !bank
	;jsl $80FA80
	phx
	ldx #$0C ;what the shit man fuck so indie
	- stz $14C8,x
	dex
	bpl -
	plx
	inc $13C6
	lda #$FF
	sta $1493
	lda #$03
	sta $1DFB
	rts
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
Graphics:
	jsr GET_DRAW_INFO
	lda $14
	lsr #4
	and #$07
	phx
	tax
	lda .which,x
	plx
	sta $07
	lda $07 ;needed?
	beq +
	lda #$01 : sta $1510,x
	bra ++
	+ stz $1510,x
	++
.closeeye
	phx
	ldx #$03
.loop
	lda $00
	clc
	adc .xeyelid,x
	sta $0300,y
	lda $01
	clc
	adc .yeyelid,x
	sta $0301,y
	phx
	txa
	clc : adc $07 
	tax
	lda .eyelidtiles,x
	plx
	sta $0302,y
	lda #$23
	ora $64
	sta $0303,y
	iny
	iny
	iny
	iny
	dex
	bpl .loop
	plx
	
	
	iny #4
	jmp +++
.hurt
	lda $14
	lsr #2
	and #$07
	phx
	tax
	lda .movex,x
	clc
	adc $00
	sta $0300,y
	lda .movey,x
	plx
	clc
	adc $01
	adc #$10
	sta $0301,y
	jmp .branch	
+++
	
	lda $154C,x
	bne .hurt
	
	lda $D1
	and #$F0 ;useless? Yes.
	lsr #4
	phx
	tax
	lda .xdisp,x
	plx
	sta $03
	
	;lda $9D ;delete?
	;bne + ;
	phx
	lda $D3
	and #$F0
	lsr #4
	tax
	lda .ydisp1,x
	plx
	sta $04
	lda $D4
	bne +
	lda #$04
	sta $04
	+
	lda $00
	clc
	adc $03
	sta $0300,y
	lda $01
	clc
	adc $04
	sta $0301,y
.branch
	lda #$2A
	sta $0302,y
	lda #$03
	ora $64
	sta $0303,y
	ldy #$02
	lda #$04
	jsl $01B7B3
	
	rts
.which
db $04,$08,$08,$04
db $00,$00,$00,$00
	;phy
	;ldy #$00
	;lda #$00
	;jsl $01B7B3
	;rts
;eye closing stuff?
	;ply
	;iny #4
	;lda $0F33	;smw seconds timer
.xeyelid
db $00,$10,$00,$10
.yeyelid
db $00,$00,$10,$10
.eyelidtiles
db $28,$28,$28,$28
db $40,$42,$44,$46
db $60,$62,$64,$66
.xdisp
db $04,$04,$05,$06
db $08,$08,$09,$0B
db $0C,$0E,$0F,$10
db $12,$13,$14,$14
.ydisp1
db $08,$0A,$0C,$0E
db $10,$10,$10,$10
db $10,$10,$10,$10
db $10,$10,$10,$10

.movex
db $0C,$04,$04,$04
db $0C,$14,$14,$14
.movey
db $00,$00,$FC,$F8
db $F6,$F8,$FC,$00

SubCode: ;vines/wires
	jsr SUB_OFF_SCREEN_X0
	;lda $7F1234
	lda $163E,x
	bne Notyet
	jsr WireGraphics
	lda $9D
	bne +
	lda $C2,x
	cmp #$04
	beq .kill
	;lda $C2,x
	and #$03
	tay
	lda $1540,x
	beq .changestate
	lda .speed,y
	sta $AA,x
	jsl $01801A
	jsr CustomContact
	+ rts
.changestate
	lda .time,y
	sta $1540,x
	inc $C2,x
	rts
.kill
	stz $14C8,x
	lda !ram
	and #$7F
	sta !ram
	rts
.time
db $55,$30,$20,$30
.speed
db $00,$10,$00,$D0 ;changed $d0-->$c0
Notyet:
	jsr .notyetgraphics ;fuck get draw info
	lda $7F1234
	stz $AA,x
	jsl $01801A
	rts
.notyetgraphics
	JSL !GetDrawInfo
	lda $14
	lsr #2
	and #$03
	phx
	tax
	lda $00
	clc
	adc .xdisp,x
	sta $0300,y ;sta $0240,y
	lda $01
	clc
	adc .ydisp,x
	sec
	sbc #$10 ;changed, because the vine length
	sta $0301,y ;sta $0241,y
	lda .tiles,x
	plx
	sta $0302,y ;sta $0242,y
	lda #$29
	ora $64
	sta $0303,y ;sta $0243,y
	ldy #$00
	tya
	jsl $01B7B3
	rts
.xdisp
db $04,$FE,$06,$F9
.ydisp
db $70,$7D,$86,$71
.tiles
db $2E,$2F,$2E,$2F

WireGraphics:
	JSL !GetDrawInfo
	lda $1FD6,x
	and #$03
	phx
	tax
	lda .offset,x
	sta $04
	lda $14
	lsr #2
	and #$03
	sta $03
	ldx #$05
.loop
	lda $00
	sta $0300,y
	lda $01
	sec
	sbc #$04 ;clipping
	clc
	adc .ydisp,x
	sta $0301,y

	lda $04
	clc
	adc $03
	phx
	tax
	lda.w .tiles,x
	plx
	
	sta $0302,y
	lda #$0F
	ora $64
	sta $0303,y
	iny #4
	dex
	bpl .loop
	lda $03
	and #$01
	tax
	lda $00
	sta $0300,y
	lda $01
	clc
	adc #$5C
	sta $0301,y
	lda .tiles2,x
	plx
	sta $0302,y
	lda #$2F
	sta $0303,y
	ldy #$02
	lda #$06
	jsl $01B7B3
	rts
.ydisp
db $00,$10,$20,$30,$40,$50
.tiles
db $48,$68,$68,$48,$68,$48
db $68,$48,$68,$68,$48,$68
db $68,$48,$48,$68,$48,$48
db $48,$68,$48,$48,$68,$68
.tiles2
db $6A,$6C
.offset
db $00,$06,$0C,$12

Spawn:
	stz $C2,x
	phy
	jsl $01ACF9
	lda $148D
	and #$07 ;changed #$03
	asl #2
	sta $C2,x
	lda $1528,x
	and #$07
	asl A
	phx
	tax
	rep #$20
	lda.w .tablething,x
	sta $00
	sep #$20
	plx
	jmp ($0000)
	
	.four
	jsr .subroutine ;oh shit nigger what are you doin
	inc $C2,x
	.three
	jsr .subroutine
	inc $C2,x
	.two
	jsr .subroutine
	inc $C2,x
	.one
	jsr .subroutine
	ply
	rts
.tablething
dw Spawn_one,Spawn_two,Spawn_three,Spawn_four
dw Spawn_four,Spawn_four,Spawn_four,Spawn_four
.subroutine
	phx
	jsl $02A9DE ;spr=y, main=x
	bmi +
	lda #$01
	sta $14C8,y
	lda $7FAB9E,x
	phx
	tyx
	sta $7FAB9E,x
	plx
	;lda $7F1234
	lda $C2,x
	phx
	tax
	lda .xlo,x
	sta $00E4,y
	;lda .xhi
	lda #$00
	sta $14E0,y
	plx
	lda #$B0 ;$a0 changed ;possibly draw another tile on each vine and move back up?
	sta $00D8,y
	lda #$00
	sta $14D4,y
	sta $1602,y
	phx
	tyx
	jsl $07F7D2
	jsl $0187A7
	lda #$0C ;8|4
	sta $7FAB10,x
	plx
	+ plx : rts
.xlo ;10-e0
db $40,$60,$80,$A0
db $60,$C0,$D0,$E0
db $50,$70,$B0,$C0
db $30,$90,$B0,$D0

db $C0,$80,$50,$A0
db $90,$40,$B0,$E0
db $A0,$D0,$30,$40
db $D0,$B0,$50,$20

.xhi
db $00

CustomContact:
.clipping
	stz $0F
	lda #$00 ;xdisp
	bpl +
	dec $0F
	+ clc
	adc $E4,x
	sta $04
	lda $14E0,x
	adc $0F
	sta $0A
	lda #$10 ;width
	sta $06
	stz $0F
	lda #$00 ;ydisp
	bpl +
	dec $0F
	+ clc
	adc $D8,x
	sta $05
	lda $14D4,x
	adc $0F
	sta $0B
	lda #$70 ;length
	sta $07
	jsl $03B664 ;mario clipping
	jsl $03B72B ;check
	bcc .no_contact
	jsl $00F5B7
.no_contact
	rts
HurtTree:
	phx
	jsl $03B6E5
	phx
	ldx #$0C
.loop
	dex
	bmi .done
	lda $14C8,x
	beq .loop
	txa
	cmp $01,s
	beq .loop
	jsl $03B69F
	jsl $03B72B
	bcc .loop
	lda $14C8,x
	cmp #$09
	bcc .loop
	txy ;new spr
	plx ;tree
	lda #$00
	sta $14C8,y ;kill bobomb
	inc $1528,x
	lda #$3F
	sta $1887 ;shake ground ?
	sta $154C,x ;hurtframe counter
	lda #!sfx
	sta !bank
	phx
	;lda $7F1234 ;avdhjvahdahd kah dkha kdhda k aha haa
	.done
	plx
	
	
	
	
	
	plx
	rts
SpawnBomb:
	phx
	lda $1504,x
	bne .return
	lda $14
	and #$FF ;change? 3f
	ora $9D
	bne .return
	lda $7F1234
	lda #$70
	sta $1504,x
	jsl $02A9DE
	bmi .return
	tyx
	lda #$01
	sta $14C8,x
	lda #$0D ;bobomb
	sta $9E,x
	jsl $07F7D2
	phx
	jsl $01ACF9
	lda $148D
	and #$01
	tax
	lda .table,x
	plx
	sta $E4,x
	lda #$00
	sta $14E0,x
	sta $14D4,x
	lda #$E0 ;changed?
	sta $D8,x
	lda $7F1234
	;lda $D1 ;change ;delete ;$7f1234 etc etc
.return
	plx
	rts
.table
db $20,$E0

Full_Monty:
	pha ;
	lda $7F1234 ;delete
	lda !ram ;
	pla ;
SpawnMonty:
	lda $7F1234
	bit !ram
	bvs .return
	lda !ram
	ora #$40
	sta !ram
;spawn monty sprite

	rts
.return
	rts
MontyMain:
	jsr SUB_OFF_SCREEN_X0




SPR_T12:             db $40,$B0
SPR_T13:             db $01,$FF
SPR_T14:             db $30,$C0,$A0,$C0,$A0,$F0,$60,$90		;bank 1 sizes
		            db $30,$C0,$A0,$80,$A0,$40,$60,$B0		;bank 3 sizes
SPR_T15:             db $01,$FF,$01,$FF,$01,$FF,$01,$FF		;bank 1 sizes
					db $01,$FF,$01,$FF,$01,$00,$01,$FF		;bank 3 sizes
SUB_OFF_SCREEN_X0:   STZ $03					; /

START_SUB:           JSR SUB_IS_OFF_SCREEN   ; \ if sprite is not off screen, return
                    BEQ RETURN_35           ; /
                    LDA $5B                 ; \  goto VERTICAL_LEVEL if vertical level
                    AND #$01                ; |
                    BNE VERTICAL_LEVEL      ; /     
                    LDA $D8,x               ; \
                    CLC                     ; | 
                    ADC #$50                ; | if the sprite has gone off the bottom of the level...
                    LDA $14D4,x             ; | (if adding 0x50 to the sprite y position would make the high byte >= 2)
                    ADC #$00                ; | 
                    CMP #$02                ; | 
                    BPL ERASE_sprite        ; /    ...erase the sprite
                    LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0756 VC:176 00 FL:205
                    AND #$01                ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0780 VC:176 00 FL:205
                    ORA $03                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0796 VC:176 00 FL:205
                    STA $01                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0820 VC:176 00 FL:205
                    TAY                     ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0844 VC:176 00 FL:205
                    LDA $1A                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0858 VC:176 00 FL:205
                    CLC                     ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0882 VC:176 00 FL:205
                    ADC SPR_T14,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0896 VC:176 00 FL:205
                    ROL $00                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizcHC:0928 VC:176 00 FL:205
                    CMP $E4,x               ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:0966 VC:176 00 FL:205
                    PHP                     ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:0996 VC:176 00 FL:205
                    LDA $1B                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizCHC:1018 VC:176 00 FL:205
                    LSR $00                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdiZCHC:1042 VC:176 00 FL:205
                    ADC SPR_T15,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizcHC:1080 VC:176 00 FL:205
                    PLP                     ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F0 P:eNvMXdizcHC:1112 VC:176 00 FL:205
                    SBC $14E0,x             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1140 VC:176 00 FL:205
                    STA $00                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1172 VC:176 00 FL:205
                    LSR $01                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1196 VC:176 00 FL:205
                    BCC SPR_L31             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1234 VC:176 00 FL:205
                    EOR #$80                ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1250 VC:176 00 FL:205
                    STA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1266 VC:176 00 FL:205
SPR_L31:             LDA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1290 VC:176 00 FL:205
                    BPL RETURN_35           ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1314 VC:176 00 FL:205
ERASE_sprite:        LDA $14C8,x             ; \ if sprite status < 8, permanently erase sprite
                    CMP #$08                ; |
                    BCC KILL_sprite         ; /    
                    LDY $161A,x             ;A:FF08 X:0007 Y:0001 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1108 VC:059 00 FL:2878
                    CPY #$FF                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1140 VC:059 00 FL:2878
                    BEQ KILL_sprite         ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1156 VC:059 00 FL:2878
                    LDA #$00                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1172 VC:059 00 FL:2878
                    STA $1938,y             ;A:FF00 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZcHC:1188 VC:059 00 FL:2878
KILL_sprite:         STZ $14C8,x             ; erase sprite
RETURN_35:           RTS                     ; return

VERTICAL_LEVEL:      LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ; \
                    LSR A                   ; | 
                    BCS RETURN_35           ; /
                    LDA $E4,x               ; \ 
                    CMP #$00                ;  | if the sprite has gone off the side of the level...
                    LDA $14E0,x             ;  |
                    SBC #$00                ;  |
                    CMP #$02                ;  |
                    BCS ERASE_sprite        ; /  ...erase the sprite
                    LDA $13                 ;A:0000 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:1218 VC:250 00 FL:5379
                    LSR A                   ;A:0016 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1242 VC:250 00 FL:5379
                    AND #$01                ;A:000B X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1256 VC:250 00 FL:5379
                    STA $01                 ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1272 VC:250 00 FL:5379
                    TAY                     ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1296 VC:250 00 FL:5379
                    LDA $1C                 ;A:001A X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0052 VC:251 00 FL:5379
                    CLC                     ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0076 VC:251 00 FL:5379
                    ADC SPR_T12,y           ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0090 VC:251 00 FL:5379
                    ROL $00                 ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:enVMXdizCHC:0122 VC:251 00 FL:5379
                    CMP $D8,x               ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0160 VC:251 00 FL:5379
                    PHP                     ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0190 VC:251 00 FL:5379
                    LDA.w $001D             ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F2 P:eNVMXdizcHC:0212 VC:251 00 FL:5379
                    LSR $00                 ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdiZcHC:0244 VC:251 00 FL:5379
                    ADC SPR_T13,y           ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdizCHC:0282 VC:251 00 FL:5379
                    PLP                     ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:envMXdiZCHC:0314 VC:251 00 FL:5379
                    SBC $14D4,x             ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0342 VC:251 00 FL:5379
                    STA $00                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0374 VC:251 00 FL:5379
                    LDY $01                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0398 VC:251 00 FL:5379
                    BEQ SPR_L38             ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0422 VC:251 00 FL:5379
                    EOR #$80                ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0438 VC:251 00 FL:5379
                    STA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0454 VC:251 00 FL:5379
SPR_L38:             LDA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0478 VC:251 00 FL:5379
                    BPL RETURN_35           ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0502 VC:251 00 FL:5379
                    BMI ERASE_sprite        ;A:8AFF X:0002 Y:0000 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0704 VC:184 00 FL:5490

SUB_IS_OFF_SCREEN:   LDA $15A0,x             ; \ if sprite is on screen, accumulator = 0 
                    ORA $186C,x             ; |  
                    RTS                     ; / return
SPR_T1:              db $0C,$1C
SPR_T2:             db $01,$02

GET_DRAW_INFO:       STZ $186C,x             ; reset sprite offscreen flag, vertical
                    STZ $15A0,x             ; reset sprite offscreen flag, horizontal
                    LDA $E4,x               ; \
                    CMP $1A                 ;  | set horizontal offscreen if necessary
                    LDA $14E0,x             ;  |
                    SBC $1B                 ;  |
                    BEQ ON_SCREEN_X         ;  |
                    INC $15A0,x             ; /

ON_SCREEN_X:         LDA $14E0,x             ; \
                    XBA                     ;  |
                    LDA $E4,x               ;  |
                    REP #$20                ;  |
                    SEC                     ;  |
                    SBC $1A                 ;  | mark sprite invalid if far enough off screen
                    CLC                     ;  |
                    ADC #$0040            ;  |
                    CMP #$0180            ;  |
                    SEP #$20                ;  |
                    ROL A                   ;  |
                    AND #$01                ;  |
                    STA $15C4,x             ; / 
                    BNE INVALID             ; 
                    
                    LDY #$00                ; \ set up loop:
                    LDA $1662,x             ;  | 
                    AND #$20                ;  | if not smushed (1662 & 0x20), go through loop twice
                    BEQ ON_SCREEN_LOOP      ;  | else, go through loop once
                    INY                     ; / 
ON_SCREEN_LOOP:      LDA $D8,x               ; \ 
                    CLC                     ;  | set vertical offscreen if necessary
                    ADC SPR_T1,y            ;  |
                    PHP                     ;  |
                    CMP $1C                 ;  | (vert screen boundry)
                    ROL $00                 ;  |
                    PLP                     ;  |
                    LDA $14D4,x             ;  | 
                    ADC #$00                ;  |
                    LSR $00                 ;  |
                    SBC $1D                 ;  |
                    BEQ ON_SCREEN_Y         ;  |
                    LDA $186C,x             ;  | (vert offscreen)
                    ORA SPR_T2,y            ;  |
                    STA $186C,x             ;  |
ON_SCREEN_Y:         DEY                     ;  |
                    BPL ON_SCREEN_LOOP      ; /

                    LDY $15EA,x             ; get offset to sprite OAM
                    LDA $E4,x               ; \ 
                    SEC                     ;  | 
                    SBC $1A                 ;  | $00 = sprite x position relative to screen boarder
                    STA $00                 ; / 
                    LDA $D8,x               ; \ 
                    SEC                     ;  | 
                    SBC $1C                 ;  | $01 = sprite y position relative to screen boarder
                    STA $01                 ; / 
                    RTS                     ; return

INVALID:             PLA                     ; \ return from *main gfx routine* subroutine...
                    PLA                     ;  |    ...(not just this subroutine)
                    RTS                     ; /