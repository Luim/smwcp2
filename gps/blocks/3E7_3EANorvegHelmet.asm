db $42
JMP Return : JMP Mario : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Mario : JMP Mario : JMP Mario

!norveg_hurt_timer = $0DA1
!norveg_health = $1DEF
!get_random_number = $81ACF9
!left_hand_delay_timer = $13C8
!right_hand_delay_timer = $13F2
!robot_state = $1B7F


!base_block = #$03E7

!RAM_Map16TileUpload = $1B97

small_crack_blocks:
    dw $28E9,$28EA
small_crack_x_offsets:
    db $10,$20
small_crack_y_offsets:
    db $00,$00

medium_crack_blocks:
    dw $28C9,$28CA
    dw $28D9,$28DA
medium_crack_x_offsets:
    db $10,$20
    db $10,$20
medium_crack_y_offsets:
    db $00,$00
    db $10,$10



large_crack_blocks:
    dw $28CB,$28CC,$28CD,$28CE
    dw $28DB,$28DC,$28DD,$28DE
    dw $28EB,$28EC,$28ED,$28EE
large_crack_x_offsets:
    db $00,$10,$20,$30
    db $00,$10,$20,$30
    db $00,$10,$20,$30
large_crack_y_offsets:
    db $00,$00,$00,$00
    db $10,$10,$10,$10
    db $20,$20,$20,$20

open_helmet_blocks:
    dw $2852,$2852
    dw $289C,$289D,$289E,$289F
    dw $28AC,$28AD,$28AE,$28AF
    dw $28BC,$28BD,$28BE,$28BF
open_helmet_x_offsets:
    db $10,$20
    db $00,$10,$20,$30
    db $00,$10,$20,$30
    db $00,$10,$20,$30
open_helmet_y_offsets:
    db $F0,$F0
    db $00,$00,$00,$00
    db $10,$10,$10,$10
    db $20,$20,$20,$20

Mario:
    lda !robot_state
    cmp #$02
    bcs .return
    lda !norveg_hurt_timer
    bne Return
    lda #$28
    sta $1DFC
    lda #$80
    sta !norveg_hurt_timer
    lda !norveg_health
    beq +
        dec !norveg_health
    +
    lda !norveg_health
    cmp #$04
    bcs +
        jsl !get_random_number
        lda $148D
        sta !left_hand_delay_timer
        jsl !get_random_number
        lda $148D
        sta !right_hand_delay_timer
    +
    lda !norveg_health
    cmp #$06
    bne +
        lda #$00
        jmp do_blocks_upload
    +
    cmp #$03
    bne +
        lda #$01
        jmp do_blocks_upload
    +
    cmp #$01
    bne +
        lda #$02
        jmp do_blocks_upload
    +
    cmp #$00
    bne +
        lda #$03
        jmp do_blocks_upload
    +
    lda #$C0
    sta $7D
    .return
    rtl

Sprite:
Return:
    rtl

crack_blocks_pointers:
    dw small_crack_blocks
    dw medium_crack_blocks
    dw large_crack_blocks
    dw open_helmet_blocks

crack_x_offset_pointers:
    dw small_crack_x_offsets
    dw medium_crack_x_offsets
    dw large_crack_x_offsets
    dw open_helmet_x_offsets

crack_y_offset_pointers:
    dw small_crack_y_offsets
    dw medium_crack_y_offsets
    dw large_crack_y_offsets
    dw open_helmet_y_offsets

crack_block_counts:
    db $01,$03,$0B,$0D

block_offsets:
    dw $0000,$FFF0,$FFE0,$FFD0

do_blocks_upload:
    phx
    tax
    lda crack_block_counts,x
    sta $0A
    txa
    asl
    tax
    rep #$20
    lda crack_blocks_pointers,x
    sta $00
    lda crack_x_offset_pointers,x
    sta $05
    lda crack_y_offset_pointers,x
    sta $07
    lda $03
    sec
    sbc !base_block
    asl
    tax
    lda block_offsets,x
    sta $0C
    sep #$20
    ldx $0A
    upload_block_loop:
        lda #$00
        xba
        phy
        txy
        lda ($07),y
        sta $09
        lda ($05),y
        sta $02
        ply
        rep #$20
        clc
        adc $0C
        clc
        adc $9A
        and #$FFF0
        sta $0E
        lda $9A
        pha
        lda $98
        pha
        lda $0E
        sta $9A
        sep #$20
        phy
        txa
        asl
        tay
        rep #$20
        lda ($00),y
        ply
        ;sta !RAM_Map16TileUpload
        sta $0E
        lda $09
        and #$00FF
        cmp #$0080
        bcc +
            ora #$FF00
        +
        clc
        adc $98
        sta $98
        sep #$20
        lda #$FF
        sta $9C
        phx
        rep #$30
        lda $00
        pha
        lda $02
        pha
        lda $04
        pha
        lda $06
        pha
        lda $08
        pha
        lda $0A
        pha
        lda $0C
        pha
        lda $0E
        pha
        ldx $0E
        ;jsl $00BEB0 ;spawn block
        ; jsl !SubSetMap16
	%change_map16()
        pla
        sta $0E
        pla
        sta $0C
        pla
        sta $0A
        pla
        sta $08
        pla
        sta $06
        pla
        sta $04
        pla
        sta $02
        pla
        sta $00
        sep #$10
        plx
        pla
        sta $98
        pla
        sta $9A
        sep #$20
        dex
        bmi +
            jmp upload_block_loop
        +
    plx
    lda #$C0
    sta $7D
    rtl