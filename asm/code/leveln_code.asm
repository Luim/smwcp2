;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;True levelASM Code lies ahead. (This code executes after sprites drawn)
;If you are too lazy to search for a level
;Use CTRL+F. The format is as following:
;levelnx - levels 0-F
;levelnxx - levels 10-FF
;levelnxxx - levels 100-1FF
;Should be pretty obvious...
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

leveln0:

leveln1:
	RTS
leveln3:
leveln6:
leveln25:
leveln26:
leveln27:
leveln28:
levelnC5:
leveln2:
	!lv2_RAM_DataAlternator = $1415
	!lv2_RAM_HdmaDataStart = $7F9E00
	!lv2_Channels = 3
	!lv2_RAM_HdmaDataStart2 = (!lv2_RAM_HdmaDataStart+!lv2_HdmaDataSize)		; this just makes it right after.
	!lv2_RAM_HdmaRestorePointStart = !lv2_RAM_HdmaDataStart
	!lv2_RAM_HdmaTableStart = (!lv2_RAM_HdmaDataStart+(!lv2_Channels*2))
	!lv2_HdmaTableSize = (leveln2_HdmaTableEnd-leveln2_HdmaTableStart)
	!lv2_HdmaDataSize = ((!lv2_Channels*2)+leveln2_HdmaTableEnd-leveln2_HdmaTableStart)
	!lv2_RAM_HdmaTableStart2 = (!lv2_RAM_HdmaDataStart2+(!lv2_Channels*2))
	!lv2_HdmaDataDifference = (!lv2_RAM_HdmaDataStart2-!lv2_RAM_HdmaDataStart)

	LDA.b #!lv2_RAM_HdmaTableStart>>16
	STA $02
	LDA.b #(.HdmaTableStart)>>16
	STA $05
	REP #%00110000
	LDX.w !lv2_RAM_DataAlternator				; We keep two copies of each table. Get the offset depending on which one.
	LDY.w #.GTblB-.GTblA
	
	; Restore last scanline counts modified by the routine
	%hdma_vscroll_restore_last(!lv2_RAM_HdmaTableStart+(.GTblA-.HdmaTableStart), .GTblA, !lv2_RAM_HdmaRestorePointStart+(0*2), !lv2_RAM_DataAlternator, 1)
	%hdma_vscroll_restore_last(!lv2_RAM_HdmaTableStart+(.STblL2-.HdmaTableStart), .STblL2, !lv2_RAM_HdmaRestorePointStart+(1*2), !lv2_RAM_DataAlternator, 0)
	%hdma_vscroll_restore_last(!lv2_RAM_HdmaTableStart+(.STblL3-.HdmaTableStart), .STblL3, !lv2_RAM_HdmaRestorePointStart+(2*2), !lv2_RAM_DataAlternator, 0)

	; Set horizontal scroll values
	LDA $1E
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL3+21,x
	LSR
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL2+11,x
	LSR
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL2+6,x
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL3+16,x
	LSR
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL3+11,x
	LSR
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL3+6,x
	LSR
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL3+1,x
	
	; Set vertical scroll values, and also modify certain scanline count values between different V Speeds
	LDA $20
	CLC
	ADC #$0040
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL3+23,x
	LSR
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL2+13,x
	STA $00
	SEP #%00100000
	ADC #$26
	STA $03
	REP #%00100000
	LDA $00
	LSR
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL2+8,x
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL3+18,x
	STA $00
	SEP #%00100000
	PHP		; to backup carry flag
	ADC $03
	EOR #$FF
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL3+15,x
	LDA $00
	PLP
	ADC #$68
	EOR #$FF
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL2+5,x
	REP #%00100000
	LDA $00
	LSR
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL3+13,x
	STA $00
	SEP #%00100000
	ADC #$CC
	EOR #$FF
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL3+10,x
	REP #%00100000
	LDA $00
	LSR
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL3+8,x
	STA $00
	SEP #%00100000
	ADC #$D3
	EOR #$FF
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL3+5,x
	REP #%00100000
	LDA $00
	LSR
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL3+3,x
	SEP #%00100000
	ADC #$82
	EOR #$FF
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.STblL3+0,x
	SEC
	SBC #$70
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.GTblA+6,x
	STA !lv2_RAM_HdmaTableStart-.HdmaTableStart+.GTblB+6,x
	REP #%00100000
	
	LDA.w #!lv2_RAM_HdmaTableStart+(.STblL2-.HdmaTableStart)
	CLC : ADC.w !lv2_RAM_DataAlternator
	STA $00
	LDA $20
	LSR #2
	STA $06
	STA $03
	JSR HdmaVScrollMain_4					; Temporarily modify HDMA table w.r.t VScroll, and get effective start address
	LDA $00							; Get effective start address
	STA.l !lv2_RAM_HdmaRestorePointStart+(1*2),x
	STA $4352
	
	LDA.w #!lv2_RAM_HdmaTableStart+(.STblL3-.HdmaTableStart)
	CLC : ADC.w !lv2_RAM_DataAlternator
	STA $00
	LDA $06
	LSR #3
	STA $06
	STA $03
	JSR HdmaVScrollMain_4					; Temporarily modify HDMA table w.r.t VScroll, and get effective start address
	LDA $00							; Get effective start address
	STA.l !lv2_RAM_HdmaRestorePointStart+(2*2),x
	STA $4362

	LDA.w #!lv2_RAM_HdmaTableStart+(.GTblA-.HdmaTableStart)
	CLC : ADC.w !lv2_RAM_DataAlternator
	STA $00
	LDA $06
	STA $03
	JSR HdmaVScrollMain_2_Futatsu				; Temporarily modify both gradient HDMA tables w.r.t VScroll
	LDA $00							; Get effective start address of .GTblA
	STA.l !lv2_RAM_HdmaRestorePointStart+(0*2),x
	STA $4332
	CLC : ADC.w #.GTblB-.GTblA				; Offset to get the effective start address of the other
	STA $4342
	
	TXA
	EOR.w #!lv2_HdmaDataDifference				; \ We use this to alternate between two copies of the tables in memory, 
	STA.w !lv2_RAM_DataAlternator				; / so we can modify one while the screen's being drawn using the other
	
	; This draws our BG Sprites onscreen.
	LDY.w #$00FC
	LDX.w #$0000
-	LDA.w .SpTbl+0,x
	CMP.w #$7FFF
	BEQ +++
	SEC : SBC.b $1E
	AND.w #$01FF
	CMP.w #$0100
	BCC +
	CMP.w #$01F1		;TODO: Number different if 8x8
	BCC ++
+	STA.b $00
	LDA.w .SpTbl+2,x
	SEC : SBC.b $20
	AND.w #$01FF
	CMP.w #$00E0
	BCC .use
	CMP.w #$01F1		;TODO: Number different if 8x8
	BCC ++
.use	SEP #%00100000
	STA.w $0301,y
	LDA.b $00
	STA.w $0300,y
	LDA.w .SpTbl+4,x
	STA.w $0302,y
	LDA.w .SpTbl+5,x
	STA.w $0303,y
	PHY
	TYA
	LSR #2
	TAY
	LDA.w .SpTbl+6,x
	ORA.b $01
	STA.w $0460,y
	PLY
	REP #%00100000
	DEY #4
++	INX #7
	BRA -
+++
	
	SEP #%00110000
	LDA.b #%01111000		; Enable HDMA channels 4,5,6,7
	TSB $0D9F
	RTS
	
.HdmaTableStart

.GTblA		; Indirect Gradient channel 1: bytes 1 and 2
	db $0E : dw $0703+$1F0		; \  Indirect gradient table references palette in RAM so
	db $80 : dw $0703+$1F2		;  | that the colors of the gradient can be varied on a
	db $08 : dw $0703+$1F2		;  | per-level basis without too many headaches. Different
	db $60 : dw $0703+$1F4		; /  format, though, so they won't look right in the editor.
	db $00

.GTblB		; Indirect Gradient channel 2: byte 3
	db $0E : dw $0703+$1F6		; \
	db $80 : dw $0703+$1F7		;  | Same as above, but for the one-byte
	db $08 : dw $0703+$1F7		;  | channel instead of the 2 byte channel
	db $60 : dw $0703+$1F8		; /
	db $00

.STblL2		; Layer 2 H/V Scroll Table
	db $80 : dw $0000,$0000		; Blank transparency. We don't even bother scrolling this in code.
	db $5B : dw $0000,$0000		; Further structures on Layer 2
	db $80 : dw $0000,$0000		; Closer structures on Layer 2
	db $00
	
.STblL3		; Layer 3 H/V Scroll Table
	db $78 : dw $0000,$0000		; Clouds and sky and stuff
	db $20 : dw $0000,$0000		; Most distant structures
	db $10 : dw $0000,$0000		; Hill without structures attached
	db $18 : dw $0000,$0000		; Hill with bottom parts of structures
	db $80 : dw $0000,$0000		; Biggest Structures
	db $00
	
.HdmaTableEnd

.SpTbl	dw $0078
	dw $0157
	db $86
	db %00001111
	db %10
	
	dw $0078
	dw $0167
	db $8A
	db %00001111
	db %10
	
	dw $00F8
	dw $014F
	db $C0
	db %00001111
	db %10
	
	dw $00F8
	dw $0157
	db $8A
	db %00001111
	db %10
	
	dw $00F8
	dw $0167
	db $E0
	db %00001111
	db %10
	
	dw $0178
	dw $0157
	db $86
	db %00001111
	db %10
	
	dw $0178
	dw $0167
	db $8A
	db %00001111
	db %10
	
	dw $01F8
	dw $014F
	db $C0
	db %00001111
	db %10

	dw $01F8
	dw $0157
	db $8A
	db %00001111
	db %10
	
	dw $01F8
	dw $0167
	db $E0
	db %00001111
	db %10
	
	dw $7FFF
	
	
leveln4:
	
leveln5:


leveln7:
	
leveln8:
	
leveln9:
	
levelnA:
	
levelnB:
	
levelnC:
	
levelnD:
	
levelnE:
	
levelnF:
	
leveln10:
	
leveln11:
	
leveln12:
	
leveln13:
	
leveln14:
	RTS
leveln15:
	REP #%00100000
	LDA $1E
	STA $22
	LDA $20
	STA $24
	SEP #%00100000
leveln16:
	
leveln17:
	
leveln18:
	
leveln19:
	
leveln1A:
	
leveln1B:
	
leveln1C:
	
leveln1D:
	
leveln1E:
	
leveln1F:
	
leveln20:
	
leveln21:
	
leveln22:
	
leveln23:
	
leveln24:


leveln29:
	
leveln2A:
	
leveln2B:
	
leveln2C:
	
leveln2D:
	
leveln2E:
	
leveln2F:
	
leveln30:
	
leveln31:
	
leveln32:
	
leveln33:
	
leveln34:
	
leveln35:
	
leveln36:
	
leveln37:
	
leveln38:
	
leveln39:
	
leveln3A:
	
leveln3B:
	
leveln3C:
	
leveln3D:
	
leveln3E:
	
leveln3F:
	
leveln40:
	
leveln41:
	
leveln42:
	
leveln43:
	
leveln44:
	
leveln45:
	
leveln46:
	
leveln47:
	
leveln48:
	
leveln49:
	
leveln4A:
	
leveln4B:
	
leveln4C:
	
leveln4D:
	
leveln4E:
	
leveln4F:
	
leveln50:
	
leveln51:
	
leveln52:
	
leveln53:
	
leveln54:
	
leveln55:
	
leveln56:
	
leveln57:
	
leveln58:
	
leveln59:
	
leveln5A:
	
leveln5B:
	
leveln5C:
	
leveln5D:
	
leveln5E:
	
leveln5F:
	
leveln60:
	
leveln61:
	
leveln62:
	
leveln63:
	
leveln64:
	
leveln65:
	
leveln66:
	
leveln67:
	
leveln68:
	
leveln69:
	
leveln6A:
	
leveln6B:
	
leveln6C:
	
leveln6D:
	
leveln6E:
	
leveln6F:
	
leveln70:
	
leveln71:
	
leveln72:
	
leveln73:
	
leveln74:
	
leveln75:
	
leveln76:
	
leveln77:
	
leveln78:
	
leveln79:
	
leveln7A:
	
leveln7B:
	
leveln7C:
	
leveln7D:
	
leveln7E:
	
leveln7F:
	
leveln80:
	
leveln81:
	
leveln82:
	
leveln83:
	
leveln84:
	
leveln85:
	
leveln86:
	
leveln87:
	
leveln88:
	
leveln89:
	
leveln8A:
	
leveln8B:
	
leveln8C:
	
leveln8D:
	
leveln8E:
	
leveln8F:
	
leveln90:
	
leveln91:
	
leveln92:
	
leveln93:
	
leveln94:
	
leveln95:
	
leveln96:
	
leveln97:
	
leveln98:
	
leveln99:
	
leveln9A:
	
leveln9B:
	
leveln9C:
	
leveln9D:
	
leveln9E:
	
leveln9F:
	
levelnA0:
	
levelnA1:
	
levelnA2:
	
levelnA3:
	
levelnA4:
	
levelnA5:
	
levelnA6:
	
levelnA7:
	
levelnA8:
	
levelnA9:
	
levelnAA:
	
levelnAB:
	
levelnAC:
	
levelnAD:
	
levelnAE:
	
levelnAF:
	
levelnB0:
	
levelnB1:
	
levelnB2:
	
levelnB3:
	
levelnB4:
	
levelnB5:
	
levelnB6:
	
levelnB7:
	
levelnB8:
	
levelnB9:
	
levelnBA:
	
levelnBB:
	
levelnBC:
	
levelnBD:
	
levelnBE:
	
levelnBF:
	
levelnC0:
	
levelnC1:
	
levelnC2:
	
levelnC3:
	
levelnC4:
	
levelnC6:
	
levelnC7:
	
levelnC8:
	
levelnC9:
	
levelnCA:
	
levelnCB:
	
levelnCC:
	
levelnCD:
	
levelnCE:
	
levelnCF:
	
levelnD0:
	
levelnD1:
	
levelnD2:
	
levelnD3:
	
levelnD4:
	
levelnD5:
	
levelnD6:
	
levelnD7:
	
levelnD8:
	
levelnD9:
	
levelnDA:
	
levelnDB:
	
levelnDC:
	
levelnDD:
	
levelnDE:
	
levelnDF:
	
levelnE0:
	
levelnE1:
	
levelnE2:
	
levelnE3:
	
levelnE4:
	
levelnE5:
	
levelnE6:
	
levelnE7:
	
levelnE8:
	
levelnE9:
	
levelnEA:
	
levelnEB:
	
levelnEC:
	
levelnED:
	
levelnEE:
	
levelnEF:
	
levelnF0:
	
levelnF1:
	
levelnF2:
	
levelnF3:
	
levelnF4:
	
levelnF5:
	
levelnF6:
	
levelnF7:
	
levelnF8:
	
levelnF9:
	
levelnFA:
	
levelnFB:
	
levelnFC:
	
levelnFD:
	
levelnFE:
	
levelnFF:
	
leveln100:
	
leveln101:
	
leveln102:
	
leveln103:
	
leveln104:
	
leveln105:
	
leveln106:
	
leveln107:
	
leveln108:
	
leveln109:
	
leveln10A:
	
leveln10B:
	
leveln10C:
	
leveln10D:
	
leveln10E:
	
leveln10F:
	
leveln110:
	
leveln111:
	
leveln112:
	
leveln113:
	
leveln114:
	
leveln115:
	
leveln116:
	
leveln117:
	
leveln118:
	
leveln119:
	
leveln11A:
	
leveln11B:
	
leveln11C:
	
leveln11D:
	
leveln11E:
	
leveln11F:
	
leveln120:
	
leveln121:
	
leveln122:
	
leveln123:
	
leveln124:
	
leveln125:
	
leveln126:
	
leveln127:
	
leveln128:
	
leveln129:
	
leveln12A:
	
leveln12B:
	
leveln12C:
	
leveln12D:
	
leveln12E:
	
leveln12F:
	
leveln130:
	
leveln131:
	
leveln132:
	
leveln133:
	
leveln134:
	
leveln135:
	
leveln136:
	
leveln137:
	
leveln138:
	
leveln139:
	
leveln13A:
	
leveln13B:
	
leveln13C:
	
leveln13D:
	
leveln13E:
	
leveln13F:
	
leveln140:
	
leveln141:
	
leveln142:
	
leveln143:
	
leveln144:
	
leveln145:
	
leveln146:
	
leveln147:
	
leveln148:
	
leveln149:
	
leveln14A:
	
leveln14B:
	
leveln14C:
	
leveln14D:
	
leveln14E:
	
leveln14F:
	
leveln150:
	
leveln151:
	
leveln152:
	
leveln153:
	
leveln154:
	
leveln155:
	
leveln156:
	
leveln157:
	
leveln158:
	
leveln159:
	
leveln15A:
	
leveln15B:
	
leveln15C:
	
leveln15D:
	
leveln15E:
	
leveln15F:
	
leveln160:
	
leveln161:
	
leveln162:
	
leveln163:
	
leveln164:
	
leveln165:
	
leveln166:
	
leveln167:
	
leveln168:
	
leveln169:
	
leveln16A:
	
leveln16B:
	
leveln16C:
	
leveln16D:
	
leveln16E:
	
leveln16F:
	
leveln170:
	
leveln171:
	
leveln172:
	
leveln173:
	
leveln174:
	
leveln175:
	
leveln176:
	
leveln177:
	
leveln178:
	
leveln179:
	
leveln17A:
	
leveln17B:
	
leveln17C:
	
leveln17D:
	
leveln17E:
	
leveln17F:
	
leveln180:
	
leveln181:
	
leveln182:
	
leveln183:
	
leveln184:
	
leveln185:
	
leveln186:
	
leveln187:
	
leveln188:
	
leveln189:
	
leveln18A:
	
leveln18B:
	
leveln18C:
	
leveln18D:
	
leveln18E:
	
leveln18F:
	
leveln190:
	
leveln191:
	
leveln192:
	
leveln193:
	
leveln194:
	
leveln195:
	
leveln196:
	
leveln197:
	
leveln198:
	
leveln199:
	
leveln19A:
	
leveln19B:
	
leveln19C:
	
leveln19D:
	
leveln19E:
	
leveln19F:
	
leveln1A0:
	
leveln1A1:
	
leveln1A2:
	
leveln1A3:
	
leveln1A4:
	
leveln1A5:
	
leveln1A6:
	
leveln1A7:
	
leveln1A8:
	
leveln1A9:
	
leveln1AA:
	
leveln1AB:
	
leveln1AC:
	
leveln1AD:
	
leveln1AE:
	
leveln1AF:
	
leveln1B0:
	
leveln1B1:
	
leveln1B2:
	
leveln1B3:
	
leveln1B4:
	
leveln1B5:
	
leveln1B6:
	
leveln1B7:
	
leveln1B8:
	
leveln1B9:
	
leveln1BA:
	
leveln1BB:
	
leveln1BC:
	
leveln1BD:
	
leveln1BE:
	
leveln1BF:
	
leveln1C0:
	
leveln1C1:
	
leveln1C2:
	
leveln1C3:
	
leveln1C4:
	
leveln1C5:
	
leveln1C6:
	
leveln1C7:
	
leveln1C8:
	
leveln1C9:
	
leveln1CA:
	
leveln1CB:
	
leveln1CC:
	
leveln1CD:
	
leveln1CE:
	
leveln1CF:
	
leveln1D0:
	
leveln1D1:
	
leveln1D2:
	
leveln1D3:
	
leveln1D4:
	
leveln1D5:
	
leveln1D6:
	
leveln1D7:
	
leveln1D8:
	
leveln1D9:
	
leveln1DA:
	
leveln1DB:
	
leveln1DC:
	
leveln1DD:
	
leveln1DE:
	
leveln1DF:
	
leveln1E0:
	
leveln1E1:
	
leveln1E2:
	
leveln1E3:
	
leveln1E4:
	
leveln1E5:
	
leveln1E6:
	
leveln1E7:
	
leveln1E8:
	
leveln1E9:
	
leveln1EA:
	
leveln1EB:
	
leveln1EC:
	
leveln1ED:
	
leveln1EE:
	
leveln1EF:
	
leveln1F0:
	
leveln1F1:
	
leveln1F2:
	
leveln1F3:
	
leveln1F4:
	
leveln1F5:
	
leveln1F6:
	
leveln1F7:
	
leveln1F8:
	
leveln1F9:
	
leveln1FA:
	
leveln1FB:
	
leveln1FC:
	
leveln1FD:
	
leveln1FE:
	
leveln1FF:
	RTS

HdmaVScrollMain_2:
	%hdma_vscroll_main(2, 0)
	RTS
	
HdmaVScrollMain_2_Futatsu:
	%hdma_vscroll_main(2, 1)
	RTS
	
HdmaVScrollMain_4:
	%hdma_vscroll_main(4, 0)
	RTS