print "MAIN ",pc		
	PHB
	LDA #$03
	PHA
	PLB
	LDA $1528,x
	BEQ +
		JSR fireball
		PLB
		RTL
	+
	JSR statue
	PLB
	RTL

print "INIT ",pc
	LDA #$01
	STA $157C,X
	LDA $1528,x
	BNE +
		LDA #$A0
		STA $167A,X
		LDA #$0C
		STA $1662,X
	+
	STZ $C2,x
	RTL

statue:
	PHK
	PEA.w .main_return-1
	PEA $B75A
	JML $838A3C
	.main_return
	TXA	
	ASL				
	ASL				
	ADC $13				
	AND.b #$7F
	ORA $9D
	BNE .return	
		JSL $02A9E4	
	BMI .return		
		LDA.b #$17			
		STA.w $1DFC					
		LDA $E4,X			
		STA $00				
		LDA.w $14E0,X			
		STA $01				
		PHX				
		LDA.w $157C,X			
		TAX				
		LDA $00				
		CLC				
		ADC.w $8AC7,X	
		STA.w $E4,Y			
		LDA $01				
		ADC.w $8AC9,X	
		STA.w $14E0,Y			
		TYX			
		LDA #$01
		STA $14C8,x
		LDA #$0A
		STA $7FAB9E,x		
		JSL $07F7D2
		JSL $0187A7
		LDA #$08
		STA $7FAB10,x	
		INC $1528,x		
		PLX				
		LDA $D8,X			
		SEC				
		SBC.b #$02			
		STA.w $D8,Y			
		LDA.w $14D4,X			
		SBC.b #$00			
		STA.w $14D4,Y			
		LDA.w $157C,X			
		STA.w $157C,Y			
	.return
	RTS

fireball:
	PHK
	PEA.w .graphics-1
	PEA $B75A
	JML $838F1B
	.graphics
	LDA $9D
	BNE .return
		LDA #$F0
		STA $B6,x
		PHK
		PEA.w .off_screen-1
		PEA $B75A
		JML $83B85D
		.off_screen
		JSL $818022
		JSL $81803A
		JSR block_interact
	.return
		RTS
			
block_interact:
	REP #$30
	JSR get_pos
	LDA $07
	SEC
	SBC #$0010
	STA $07
	JSR evaporate

	JSR get_pos
	LDA $07
	SEC
	SBC #$0010
	STA $07
	LDA $05
	CLC
	ADC #$0010
	STA $05
	JSR evaporate

	JSR get_pos
	LDA $07
	SEC
	SBC #$0010
	STA $07
	LDA $05
	SEC
	SBC #$0010
	STA $05
	JSR evaporate

	SEP #$30
	LDX $15E9
	RTS
	
get_pos:
	SEP #$30
	LDX $15E9
	LDA $D8,x
	STA $05 
	LDA $14D4,x
	STA $06  
	LDA $E4,x
	STA $07  
	LDA $14E0,x
	STA $08  
	STA $09
	REP #$30
	RTS
	
evaporate:
	LDA $1925
	AND #$00FF
	ASL
	TAX
	LDA $00BDA8,x
	STA $0A
	LDA $00BE28,x
	STA $0D
	SEP #$20
	LDA #$00
	STA $0C
	STA $0F
	XBA
	LDA $08
	ASL
	CLC
	ADC $08
	REP #$20
	TAY
	LDA [$0A],y
	STA $6B
	LDA [$0D],y
	STA $6E
	SEP #$30
	LDA #$7E
	STA $6D
	INC
	STA $70
	REP #$30
	LDA $05
	AND #$01F0
	STA $00
	LDA $07
	AND #$00F0
	LSR #4
	CLC
	ADC $00
	TAY
	SEP #$20
	LDA [$6E],y
	XBA
	LDA [$6B],y
	REP #$20
	CMP #$2826
	BEQ nuke_block
	RTS
	
nuke_block:
	SEP #$30
	LDY #$03
	.loop
		LDA $17C0,y
		BEQ .continue
			DEY
		BPL .loop
	LDY #$07
	.loop2
		LDA $170B,y
		BEQ .continue2
			DEY
		BPL .loop2
		BRA .return
	.continue2
		LDA #$01
		STA $170B,y
		LDA #$0F
		STA $176F,y
		LDA $05
		AND #$F0
		STA $1715,y
		LDA $07
		AND #$F0
		STA $171F,y
		BRA .return
	.continue
		LDA #$01
		STA $17C0,y
		LDA #$1B
		STA $17CC,y
		LDA $05
		AND #$F0
		STA $17C4,y
		LDA $07
		AND #$F0
		STA $17C8,y
	.return
		LDA $07
		AND #$F0
		STA $9A
		LDA $09
		STA $9B
		LDA $05
		AND #$F0
		STA $98
		LDA $06
		STA $99
		LDA #$02
		STA $9C
		JSL $00BEB0
		REP #$30
	RTS