db $42

JMP MarioBelow : JMP MarioAbove : JMP MarioSide : JMP SpriteV : JMP SpriteH : JMP MarioCape : JMP MarioFireBall : JMP MarioCorner : JMP MarioHeadinside : JMP MarioBodyinside

!OnBlock1 = #$2A8E
!OffBlock1 = #$2A8F
!OnBlock2 = #$2A90
!OffBlock2 = #$2A91
!OnBlock3 = #$2A92
!OffBlock3 = #$2A93
!OnBlock4 = #$2A94
!OffBlock4 = #$2A95
!OnBlock5 = #$2A96
!OffBlock5 = #$2A97
!OnBlock6 = #$2A98
!OffBlock6 = #$2A99
!OnBlock7 = #$2A9A
!OffBlock7 = #$2A9B
!OnBlock8 = #$2A9C
!OffBlock8 = #$2A9D
!OnBlock9 = #$2A9E
!OffBlock9 = #$2A9F

!OnBlock = $60
!OffBlock = $62

!BlocksHit = $7FB100

MarioHeadinside:
LDA $149E
CMP #$01
BEQ +
JMP SpriteV
+
LDA #$0B
STA $1DF9
LDA #$FF
STA $9C
REP #$20
LDA !OnBlock9
STA !OnBlock
LDA !OffBlock9
STA !OffBlock
SEP #$20
LDA !BlocksHit+8
EOR #$01
STA !BlocksHit+8
JSR ChangeBlock
REP #$20
LDA $9A
PHA
SEC
SBC #$0030
STA $9A
LDA !OnBlock8
STA !OnBlock
LDA !OffBlock8
STA !OffBlock
SEP #$20
LDA !BlocksHit+7
EOR #$01
STA !BlocksHit+7
REP #$20
JSR ChangeBlock
PLA
STA $9A
LDA $98
PHA
SEC
SBC #$0030
STA $98
LDA !OnBlock6
STA !OnBlock
LDA !OffBlock6
STA !OffBlock
SEP #$20
LDA !BlocksHit+5
EOR #$01
STA !BlocksHit+5
REP #$20
JSR ChangeBlock
PLA
STA $98
SEP #$20
PHX
LDX #$08
.loop
LDA !BlocksHit,x
BEQ .NotWon
DEX
BPL .loop
STZ $14AF
BRA .return
.NotWon
LDA #$01
STA $14AF
.return
PLX
MarioBelow:
MarioSide:
MarioCorner:
MarioAbove:
SpriteV:
SpriteH:
MarioCape:
MarioFireBall:
MarioBodyinside:
RTL

ChangeBlock:
	PHP
	PHY
	PHX
	REP #$30
	LDA $1925
	AND #$00FF
	ASL
	TAX
	LDA $00BDA8,x
	STA $0A
	LDA $00BE28,x
	STA $0D
	SEP #$20
	LDA #$00
	STA $0C
	STA $0F
	XBA
	LDA $9B
	ASL
	CLC
	ADC $9B
	REP #$20
	TAY
	LDA [$0A],y
	STA $6B
	LDA [$0D],y
	STA $6E
	SEP #$30
	LDA #$7E
	STA $6D
	INC
	STA $70	
	REP #$30
	LDA $98
	AND #$01F0
	STA $00
	LDA $9A
	AND #$00F0
	LSR #4
	CLC
	ADC $00
    TAY
	SEP #$20
	LDA [$6E],y
	XBA
	LDA [$6B],y
	REP #$20
	CMP !OnBlock
	BEQ .OnBlock
	LDA !OnBlock
	BRA .Change
	.OnBlock
	LDA !OffBlock
	.Change
	STA $0660
	SEP #$30
	JSL $00BEB0
	PLX
	PLY
	PLP
	RTS