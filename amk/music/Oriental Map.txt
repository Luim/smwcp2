;An SMW Central Production - "Oriental Map" - SNN          ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#SPC
{
	#author "S.N.N."
	#comment "Oriental map theme"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#sky_and_dream
	#AMM
	"CCymbal.brr"
	"CStrings.brr"
	"CChoir.brr"
}

#0
w160 t57
$EF $FF $00 $00 $F1 $03 $46 $01
$F4 $02
@14 $ED $7F $F6 p20,50 y8
v85 <g32a16 $EF $FF $40 $40 ^32> v90 c8d8f8g4f8<g8
g32a16^32> v100 d8f8g16a8r16 v125 a8d8 v155 c8
<g32 v180 a16^32>c8 v210 d8f8g4 v230 d8<a1^8

v255
g32a16^32>c8d8f8g4f8<g8g32a16^32>d8f8g16a8r16a8d8c8
<g32a16^32>c8d8f8g4a4
d8d4d4d8d4
(1)[o3 v230 d16 v150 d16 v230 a8>d16 v150 d16 v230 a8>d16 v150 d16< v230 a8>d16 v150 d16 v230 f8]2
[o3c16 v150 c16 v230 g8>c16 v150 c16 v230 g8>c16 v150 c16< v230 g8>c16 v150 c16 v230 d8]2
(1)2
r8d8d8<a8>d8r8<a8c8
d1

#1

;percussion 1;

y11
r4 v180
[r1^2 

o4  v240 @15 ("CCymbal.brr", $04) $ED $7B $F3 c2
 
r4o6 @23 f+8r8f+8r1^4^8 ]2
[@23 f+8r4^8f+8r2^4^8
o4  v240 @15 ("CCymbal.brr", $04) $ED $7B $F3 c2

o6 @23 f+8r4^8f+8r1^4^8]2

#2

;percussion 2;
r8o3 v230
[@23f+8r8f+8f+8r8f+8r4]16

#3

;;;;;;;;;
;timpani;
;;;;;;;;;
o2 y12
[@10 v255 c8 v220 c16c16c8c8 v255 c8 v220 c8 v255 c8 v180 c8]16

#7

;strings 1;
@15 v140 p5,10
$ED $7B $E6
("CStrings.brr", $03)
o3a1
>c1
d1^1
a1
>c1
d1^1
<d1^1
c1^1
d4^8f4^8g2
f4^8d4^8
c1
d1

#5

;strings 2;
@15 v140 p5,10
$ED $7B $E6
("CStrings.brr", $03)
o3f1
g1
a1^1
>f1
g1
a1^1
[r1]10

#6

;strings 3;
@15 v140 p5,10
$ED $7B $E6
("CStrings.brr", $03)
o3d1
d1
f1^1
>d1
d1
f1^1
<a1^1
g1^1
a1^1
g1
a1

#4

;low flute;
y9
@0 $ED $7F $F6 v110 p5,90
o3
>d16<a16a16a16a16a16>d16<a16a16a16a16a16>c8<g8
>d16<a16a16a16a16a16>d16<a16a16a16a16a16>c8d8
d16<a16a16a16a16a16>d16<a16a16a16a16a16>c8<g8

;high flute;
v170
@0 $ED $7A $E5 p20,20
o5d16f16d16c16d8<a16>c16<a16g16a8f8g8

;8-bit;
@1 $ED $6F $00 v220
o6
<d32r32<a32r32a32r32a32r32a32r32a32r32>d32r32<a32r32a32r32a32r32a32r32a32r32>c16^32r32<g16^32r32
>d32r32<a32r32a32r32a32r32a32r32a32r32>d32r32<a32r32a32r32a32r32a32r32a32r32>c8d8
d32r32<a32r32a32r32a32r32a32r32a32r32>d32r32<a32r32a32r32a32r32a32r32a32r32>c8<g8

;high flute;
@0 v150 y11
("CPanFlute.brr", $04) $ED $7B $E6 p5,20
o5d16f16d16c16d8<a16>c16<a16g16a8f8g8

;;;;;;;
;choir;
;;;;;;;
v240 y11
("CChoir.brr", $06) $ED $78 $E6 p20,26
o3g+4^8>c+4^8g+4^8
g+4^8f+4e4
f+4e8c+4<b1^4^8>

;high flute;
@0 v150 y11
("CPanFlute.brr", $04) $ED $7B $E6 p5,20
o5d16f16d16c16d8<d16f16d16c16d8f8a8
f8d4c4<a8g8a8
>c1
d16d16d16d16c8c8<g8c8d4
;
                

#amk=1
