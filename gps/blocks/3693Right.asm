;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;This block will move an assigned Custom Sprite
;;to the left or right by 1 pixel every other frame
;;depending on the setting of Bit 3 of the Custom
;;Trigger found at $7FC0FC.
;;
;;The tile should act like Tile 130.
;;
;;Based on some code by Decimating DJ.
;;                                       -Milk
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

db $42

JMP Return : JMP Return : JMP Return : JMP Sprites : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return

Sprites:
	LDA $7FAB10,x		;\
	AND #$04		; | Custom Sprite check
	LDA $7FAB9E,x		; | if 1E
	CMP #$1E		; | then branch
	BNE Return		;/

	REP #$20		;\
	LDA $7FC0FC		; |
	AND #$0008		; | Custom Trigger Check
	BEQ Reverse		; |
	SEP #$20		;/

	LDA $14			;\ Every other frame
	AND #$01		;/
	BNE No_Move
	LDA $E4,x		;\
	SEC			; |
	SBC #$01		; | Move left 1 pixel
	STA $E4,x		; |
	LDA $14E0,x		; |
	SBC #$00		; |
	STA $14E0,x		;/
No_Move:
	RTL
Reverse:
	SEP #$20
	LDA $14			;\ Every other frame
	AND #$01		;/
	BNE No_Move
	LDA $E4,x		;\
	CLC			; |
	ADC #$01		; | Move right 1 pixel
	STA $E4,x		; |
	LDA $14E0,x		; |
	ADC #$00		; |
	STA $14E0,x		;/
	RTL
Return:
	LDA #$25		;\
	STA $1693		; | Act like tile 25
	LDY #$00		;/
	RTL