#amk 2

#SPC
{
	#author "Moose"
	#comment "Desert ruins map theme"
	#game "SMW Central Production 2"
}

#pad $0468

#samples
{
	#default
	#AMM
	#main_map
}

#0 t40 w160 $F0

#0 v255 q7f o3 l16

[@21c@22c@22c@22c@10g@22c@22c@21c@22c@22c@21c@22c@10g@22c@22c@22c]12
@29c8r2^4^8r2^4@10{ggg}gg

#1 v255 @1 l16 q7f

o4
r8efa-8a8b2r8b>cd8c8d8e8d8c8<b>c<baa-4r8ar{a-aa-}f8e2^4^8d8
r8efa-8a8b2r8b>cd8c8d8e8d8c8<b>c<baa-4r8ar{a-aa-}f8e1
r8ab->c+8d8e2r8efg8f8g8a8g8f8efedc+4r8dr{c+dc+}<b-8a1
[r1]2

#2 v255 @8 l16 q7f

o2
[e8^er8ere8^er8erf8^fr8frf8^fr8fre8^er8erd8^dr8dre8^er8ere8^er8er]2
a8^ar8ara8^ar8arb-8^b-r8b-rb-8^b-r8b-ra8^ar8arg8^gr8gra8^ar8ara8^ar8ar
e8r2^4^8r2^4d4

#3 v255 @1 l16 q7f

o3 [r1]4
r2b4>e4f2a8b8a8a8a-aa-fe4r8fr{efe}c8<b1
>r2e4a4b-2>d8e8d8d8c+dc+<ba4r8b-r{ab-a}f8e1 @3
r8efa-8a8b8>d8d8c8<baa-fe2^4 @1

#4 v230 @1 l16 q7f

o3 [r1]4
r2a-4b4>c2f8g8f8f8efec<b4r8>cr{<b>c<b}a8a-1
>r2c+4e4f2b-8>c8<b-8b-8ab-afe4r8fr{efe}d8c+1 @2 v150
<r8efa-8a8b8>d8d8c8<baa-fe2^4 v230 @1

#5 v220 @12 l16 q7f
o4
[e8^br8brr8^br8br]14