db $42
JMP MarioBelow : JMP MarioAbove : JMP MarioSide
JMP SpriteV : JMP SpriteH : JMP MarioCape : JMP MarioFireball
JMP TopCorner : JMP BodyInside : JMP HeadInside

print "This block moves the player left as much as layer 1 moved right."
print "For use in Lily Swamp Romp, Manic Mine, and Tourou Temple.  Thanks Lui37."

MarioAbove:
TopCorner:
	REP #$20
	LDA $1A		; make $00 how much layer 1 moved horizontally
	SEC
	SBC $1462
	STA $00
	LDA $94		; and subtract that from the player's X position
	SEC
	SBC $00
	STA $94
	SEP #$20
	
MarioBelow:
MarioSide:
BodyInside:
HeadInside:
SpriteV:
SpriteH:
MarioCape:
MarioFireball:
	RTL