;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Pix (Blue), by Yoshicookiezeus
; Based on mikeyk's Spiky Ball sprite
;
; Description: Bounces around diagonally. Can only be hurt by blue Koopa shells.
;
; Uses first extra bit: NO
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			PRINT "INIT ",pc
			RTL


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			PRINT "MAIN ",pc                                    
			PHB			; \
			PHK			;  | main sprite function, just calls local subroutine
			PLB			;  |
			JSR SPRITE_CODE_START	;  |
			PLB			;  |
			RTL			; /


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite main code 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

X_SPEED:			db $28,$D8
Y_SPEED:			db $28,$D8

RETURN:			RTS
SET_TIMER:		LDA #$A0
			STA $1540,x
			RTS
SPRITE_CODE_START:	JSR SUB_GFX		; graphics routine
			LDA $1540,x
			CMP #$20
			BCC NO_EARLYDET
			JSR NEAR_MARIO
NO_EARLYDET:		LDA $14C8,x
			CMP #$04
			BEQ SET_TIMER
			LDA $1540,x
			BEQ SET_TIMER
			CMP #$01
			BEQ KILL_SPRITE
			LDA $14C8,x		; if sprite is dead, RETURN
			CMP #$08
			BNE RETURN
			LDA $9D			;if sprites locked, RETURN
			BNE RETURN

STATEMACH:		LDA $1588,x		; \ if sprite is in contact with an object...
			AND #$03		;  |
			BEQ NO_CONTACT_X		;  |
			LDA $157C,x		;  | flip the direction status
			EOR #$01		;  |
			STA $157C,x		; /

NO_CONTACT_X:		LDA $1588,x		; \ if sprite is in contact with an object...
			AND #$0C		;  |
			BEQ NO_CONTACT_Y		;  |
			LDA $151C,x		;  | flip the direction status
			EOR #$01		;  |
			STA $151C,x		; /

NO_CONTACT_Y:		LDY $157C,x		; \ set x speed based on direction
			LDA X_SPEED,y		;  |
			STA $B6,x		; /
			LDY $151C,x		; \ set x speed based on direction
			LDA Y_SPEED,y		;  |
			STA $AA,x		; /                    
                                        
			JSL $01802A		; update position based on speed values
			RTS

KILL_SPRITE:		LDA #$0D	; Turn sprite..
			STA $9E,x	; Into Bob-omb.
			LDA #$08	; Status is 
			STA $14C8,x	; #$08..
			JSL $07F7D2	; Reset sprite tables.
			LDA #$01	; Set state of sprite.
			STA $1534,x	; Store it.
			LDA #$40	; Set time..
			STA $1540,x	; For explosion.
			LDA #$09	; Play sound..
			STA $1DFC	; Store it to $1DFC.
			LDA #$1B	; Make explosion..
			STA $167A,x	; Not kill other explosions.
RETURN_TWO:			RTS

NEAR_MARIO:		LDA $D1
			SEC
			SBC $E4,x
			CMP #$80
			BCC NO_FLIP1
			EOR #$FF
NO_FLIP1:		CMP #$18
			BCS RETURN
			LDA $D3
			SEC
			SBC $D8,x
			CMP #$80
			BCC NO_FLIP2
			EOR #$FF
NO_FLIP2:		CMP #$18
			BCS RETURN
			LDA #$20
			STA $1540,x
			RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

TILEMAP:			db $89,$BD
COLOR_SWAP:		db $D5,$D7,$D9,$DB
X_OFFSET:		db $00,$00
Y_OFFSET:		db $00,$00

SUB_GFX:			JSL $20CA4F	; sets y = OAM offset
			LDA $157C,x		; \ $02 = direction
			STA $02			; / 
			LDA $14			; \ 
			LSR A			;  |
			LSR A			;  |
			CLC			;  |
			ADC $15E9		;  |
			AND #$01		;  |
			STA $03			;  | $03 = index to frame start (0 or 1)
			PHX			; /
                    
			LDA $14C8,x
			CMP #$02
			BNE LOOP_START_2
			STZ $03

LOOP_START_2:		LDA $00			; \ tile x position = sprite x location ($00)
			STA $0300,y		; /

			LDA $01			; \ tile y position = sprite y location ($01)
			STA $0301,y		; /
		
			LDA $1540,x	
			CMP #$1A
			BCS DONTCHANGE
			LDA $14
			AND #$03
			PHX
			TAX
			LDA COLOR_SWAP,x
			PLX
			BRA STORETILES		
DONTCHANGE:		LDA $14AF
			BEQ HOTTILE
			LDA #$D9
			BRA STORETILES
HOTTILE:		LDA #$D7
STORETILES:		STA $0303,y		; store tile properties

			LDX $03			; \ store tile
			LDA TILEMAP,x		;  |
			STA $0302,y		; /

			INY			; \ increase index to sprite tile map ($300)...
			INY			;  |    ...we wrote 1 16x16 tile...
			INY 			;  |    ...sprite OAM is 8x8...
			INY			; /    ...so increment 4 times

			PLX			; pull, X = sprite index
			LDY #$00		; \ 460 = 2 (all 16x16 tiles)
			LDA #$00		;  | A = (number of tiles drawn - 1)
			JSL $01B7B3		; / don't draw if offscreen
			RTS			; RETURN
