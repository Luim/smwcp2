; Changes exit of the current screen to the jukebox

db $42

JMP MarioAbove : JMP MarioAbove : JMP MarioAbove : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return

!is_secondary_exit = $00
!exit_number = $100

MarioAbove:	
	ldx $D2
	lda $5B
	and #$01
	beq +
	ldx $D4
	+
	lda.b #!exit_number&$FF
	sta.w $19B8,x
	lda.b #(!exit_number&%0000000100000000>>8)|(!exit_number&%0001111000000000>>5)|(!is_secondary_exit<<1)|$04
	sta.w $19D8,x

Return:		RTL