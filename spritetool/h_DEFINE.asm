;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; DEFINES AND MACROS
; BY ALEXANDER
;
; INFO:
;   PUT IN SAME DIRECTORY AS SPRITETOOL.EXE AND DMA.ASM
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DEFINE ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

CONTROL_DATA = $7E2000		;PLAYER GFX RAM
CONTROL_D_END = $7EAD00
CONTROL_PER_FRAME = #$0B
CONTROL_D_LENGTH = 0+CONTROL_D_END-CONTROL_DATA-CONTROL_PER_FRAME

CONTROL_OVERFLOW = $7F8600
CONTROL_O_END = $7F9C7B
CONTROL_O_LENGTH = 0+CONTROL_O_END-CONTROL_OVERFLOW-CONTROL_PER_FRAME

FREE = $0F5E			;x14 BYTES FREE, x14 NEEDED
OVERFLOW = $7F8600

!FLAG = FREE
!REC = FREE+2
!RPL = FREE+4
!PHASE = FREE+6
!SP_INDEX = FREE+8
!OTHER = FREE+$A
!POINTER = FREE+$C
!SIZE = FREE+$E
!SCREEN_X = FREE+$10
!SCREEN_Y = FREE+$12

!CUR_PTR = OVERFLOW
!EXTRA_PTR = OVERFLOW+8
!OVER = OVERFLOW+$10

!INIT_XL = !OVER
!INIT_XH = !OVER+1
!INIT_YL = !OVER+2
!INIT_YH = !OVER+3

!FREESPACE_DMA = $0F8200
!ROM1 = $178200

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MACRO ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MACRO DEBUG(NUM)
PHA
PHP
SEP #$20
LDA #$<NUM>
STA $1337
LDA $1337
PLP
PLA
ENDMACRO

MACRO CHECK_UP(NOT)
;;; !OTHER ;;; 0 X X X HOLD DIR Y UP
LDA !OTHER
AND #$01
BEQ <NOT>
ENDMACRO

MACRO CHECK_HELD(NOT)
;;; !OTHER ;;; 0 X X X HOLD DIR Y UP
LDA !OTHER
AND #$08
BEQ <NOT>
ENDMACRO

MACRO CHECK_DIR(LEFT)
;;; !OTHER ;;; 0 X X X HOLD DIR Y UP
LDA !OTHER
AND #$04
BEQ <LEFT>
ENDMACRO

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MACRO PLAYER_INTERACT_SOLID()

_SOLID_INTERACTION:
_GOTO_GetDrawInfoBnk1:
LDA $14C8,X
PHA
PHK
PEA.w .RETURN-$01
PEA $8020
JML $01E305 ;$01A365
.RETURN
PLA
STA $14C8,X
STZ $00
STZ $01
STZ $02
STZ $03
_GOTO_TurnBlockBridge_INTERACTION:
PHK
PEA.w .RETURN-$01
PEA $8020
JML $01B852
.RETURN
RTS

ENDMACRO

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MACRO PLAYER_INTERACT_NONE()

_NO_INTERACTION:

ENDMACRO

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MACRO PLAYER_INTERACT_CARRY(RAM, FLAG)

_CARRY_INTERACTION:
LDA $<FLAG>,X
BEQ .NOT_HELD_BY_SHADOW
RTS

.NOT_HELD_BY_SHADOW
LDA $<RAM>,X
BEQ .NOT_HELD_BY_PLAYER
LDA $15
ORA $16
AND #$40
BEQ .DROP
JMP _INTERACT_PLAYER
.DROP
LDA #$00
STA $<RAM>,X
RTS

.NOT_HELD_BY_PLAYER
LDA $14D4,X
XBA
LDA $D8,X
REP #$20
SEC
SBC $96
SEC
SBC #$000E
CMP #$001C
SEP #$20
BCS .MISS
LDA $14E0,X
XBA
LDA $00E4,X
REP #$20
SEC
SBC $94
CLC
ADC #$000E
CMP #$001C
SEP #$20
BCC _INTERACT_PLAYER
.MISS
RTS

_INTERACT_PLAYER:
LDA $15
ORA $16
AND #$40
BEQ .END
LDA #$FF
STA $<RAM>,X
LDA $76
JMP _HOLD_OFFSET_GIVEN_DIR
.END
RTS

_HOLD_OFFSET_GIVEN_DIR:
BEQ _HOLD_LEFT
LDA $94
CLC
ADC #$08
STA $E4,X
LDA $95
ADC #$00
STA $14E0,X
BRA _HOLD_SET

_HOLD_LEFT:
LDA $94
SEC
SBC #$08
STA $E4,X
LDA $95
SBC #$00
STA $14E0,X

_HOLD_SET:
LDA $96
CLC
ADC #$10
STA $D8,X
LDA $97
ADC #$00
STA $14D4,X
RTS

ENDMACRO
