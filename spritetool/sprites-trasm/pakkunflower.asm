;pakkun flower (SML)
;by smkdan
;GFX from andyk250

;I lifted some of the code from the Rex since it's stomping behaviour can be applied here

;some definitions
!PROPRAM = $04
!TEMP = $05
!FRAMEINDEX = $09

!FIREFRAME = $02
!SQUISHFRAME = $04

;fire timers
!FIREINTERVAL = $80
!FIREDURATION = $20

;speed
XSPD:	db 08,$F8

;generation
!GENSPEEDX = $00
!GENSPEEDY = $A0
!GENPOSX = $0000
!GENPOSY = $FFF0

;some ram
;1504: seperate frame index
;1570:	state
;	00: living and idle
;	01: living and in air
;	02: squished
;	03: dieing
;1602:	general timer for above states

;sprite number that will be fired
!PROJECTILE = $64
;time to stay squished before falling off
!SQUISHTIME = $40

;states
!LIVEWALK = $00
!LIVEFIRE = $01
!SQUISH = $02
!DIEING = $03

;speed
XSPD_TWO:	db $08,$F8

PRINT "INIT ",pc
	JSR SUB_HORZ_POS	;face mario
	TYA
	STA $157C,x		;new direction

	LDA #!LIVEWALK		;set state to idling
	STA $1570,X
	LDA #!FIREINTERVAL	;jump interval into timer
	STA $1602,x
	RTL

PRINT_TWO: "MAIN ",pc
	PHB
	PHK
	PLB
	JSR Run
	PLB
	RTL

RETURN_I:
	RTS

Run:
	JSR SUB_OFF_SCREEN_X0
	JSL $20CA4F
	JSR GFX			;draw sprite

	LDA $14C8,x
	CMP #$08          	 
	BNE RETURN_I           
	LDA $9D			;locked sprites?
	BNE RETURN_I
	LDA $15D0,x		;yoshi eating?
	BNE RETURN_I

	JSL $01802A		;regular speed update
	JSL $018032             ; interact with sprites

;figure out what to do according to state
	LDA $1570,x		;load state
	BEQ DOLIVEWALK		;00
	DEC A
	BEQ DOLIVEFIRE		;01
	DEC A
	BEQ DOSQUISH		;02
	JMP DODIEING		;03

DOLIVEWALK:
	DEC $1602,x		;decrement timer
	BEQ ENTERLIVEFIRE

	LDA $14			;frame counter
	LSR A
	LSR A			;/4
	AND #$03		;4 frames
	STA $1504,x

	LDA $1588,x		;touching wall?
	AND #$03
	BEQ NOWALL

	LDA $157C,x		;direction
	EOR #$01		;flip
	STA $157C,x

NOWALL:
	LDY $157C,x		;load direction
	LDA XSPD,y
	STA $B6,x		;XSPD

	JMP INTERACTION

ENTERLIVEFIRE:
	JSL $02A9E4		;grab free sprite slot
	BMI NONEFREE		;return if none left before anything else...

	LDA #!LIVEFIRE		;set fire status
	STA $1570,x
	LDA #!FIREDURATION	;set firing time
	STA $1602,x

	JSR SETUPCUST		;generate sprite

	JMP INTERACTION
NONEFREE:
	LDA #!LIVEWALK		;keep walking if no sprites are free
	STA $1570,x
	LDA #!FIREINTERVAL	;interval timer
	STA $1602,x

	JMP INTERACTION

DOLIVEFIRE:
	DEC $1602,x		;decrement timer
	BEQ ENTERLIVEIDLE

	STZ $B6,x		;no XSPD

	LDA #!FIREFRAME		;set firing frame
	STA $1504,x
	JMP INTERACTION

ENTERLIVEIDLE:
	LDA #!LIVEWALK		;set idle status
	STA $1570,x
	LDA #!FIREINTERVAL	;set duration timer
	STA $1602,x

	JSR SUB_HORZ_POS	;face mario
	TYA
	STA $157C,x		;new direction

	JMP INTERACTION

;STANDARD
DOSQUISH:
	LDA $B6,x		;test XSPD, slow down if not stopped already
	BEQ STILL
	BMI INCX
	DEC $B6,x		;going right, --
	BRA STILL

INCX:
	INC $B6,x		;going left, --

STILL:
	LDA #!SQUISHFRAME	;set squished frame
	STA $1504,x

	LDA $1588,x		;don't touch counter if not on ground
	BIT #$04
	BNE ONGROUND

	RTS

ONGROUND:
	DEC $1602,x		;decrement timer
	BEQ ENTERDIEING

	RTS			;don't interact

;STANDARD
ENTERDIEING:
	LDA #$FF		;erase sprite permanetley
	STA $161A,x

	LDA #!DIEING		;enter dieing status
	STA $1570,x
	LDA $1686,x		;load 1686 tweaker byte
	ORA #$80		;don't interact wiht objects
	STA $1686,x

	LDA #$E0		;rise a bit with new yspeed
	STA $AA,x
	LDA #$10		;new xspeed
	STA $B6,x
		
	RTS			;don't interact

DODIEING:
	RTS			;do nothing in dieing state

INTERACTION:
	JSL $01A7DC		;mario interact
	BCC NO_CONTACT          ; (carry set = mario/rex contact)
	LDA $1490               ; \ if mario star timer > 0 ...
	BNE HAS_STAR            ; /    ... goto HAS_STAR
	LDA $154C,x             ; \ if rex invincibility timer > 0 ...
	BNE NO_CONTACT          ; /    ... goto NO_CONTACT
	LDA $7D                 ; \  if mario's y speed < 10 ...
	CMP #$10                ;  }   ... rex will hurt mario
	BMI REX_WINS            ; /    

MARIO_WINS:          
	JSR SUB_STOMP_PTS       ; give mario points
	JSL $01AA33             ; set mario speed
	JSL $01AB99             ; display contact graphic
	LDA $140D               ; \  if mario is spin jumping...
	ORA $187A               ;  }    ... or on yoshi ...
	BNE SPIN_KILL           ; /     ... goto SPIN_KILL

;set state to squished
	LDA #!SQUISH
	STA $1570,x		;new status
	LDA #!SQUISHTIME
	STA $1602,x		;squish time
	RTS                     ; return 

REX_WINS:            
	LDA $1497               ; \ if mario is invincible...
	ORA $187A               ;  }  ... or mario on yoshi...
	BNE NO_CONTACT          ; /   ... return
	JSR SUB_HORZ_POS         ; \  set new rex direction
	TYA                     ;  }  
	STA $157C,x             ; /
	JSL $00F5B7             ; hurt mario
	RTS

SPIN_KILL:   
	LDA #$04                ; \ rex status = 4 (being killed by spin jump)
	STA $14C8,x             ; /   
	LDA #$1F                ; \ set spin jump animation timer
	STA $1540,x             ; /
	JSL $07FC3B             ; show star animation
	LDA #$08                ; \ play sound effect
	STA $1DF9               ; /
	RTS                     ; return

NO_CONTACT:          
	RTS                     ; return

STAR_SOUNDS:         db $00,$13,$14,$15,$16,$17,$18,$19
KILLED_X_SPEED:      db $F0,$10

HAS_STAR:            
	LDA #$02                ; \ rex status = 2 (being killed by star)
	STA $14C8,x             ; /
	LDA #$D0                ; \ set y speed
	STA $AA,x               ; /
	JSR SUB_HORZ_POS         ; get new rex direction
	LDA KILLED_X_SPEED,y    ; \ set x speed based on rex direction
	STA $B6,x               ; /
	INC $18D2               ; increment number consecutive enemies killed
	LDA $18D2               ; \
	CMP #$08                ; | if consecutive enemies stomped >= 8, reset to 8
	BCC NO_RESET2           ; |
	LDA #$08                ; |
	STA $18D2  		; /   
NO_RESET2:           
	JSL $02ACE5             ; give mario points
	LDY $18D2               ; \ 
	CPY #$08                ; | if consecutive enemies stomped < 8 ...
	BCS NO_SOUND2           ; |
	LDA STAR_SOUNDS,y             ; |    ... play sound effect
	STA $1DF9               ; /

NO_SOUND2:           
	RTS                     ; final return      
			
;=====

SIZE:	db $02,$02,$00,$00,$00
	db $02,$02,$00,$00,$00
	db $02,$02,$00,$00,$00
	db $02,$02,$00,$00,$00
	db $02,$02,$00,$00,$00

PROP:	db $00,$40,$00,$00,$40
	db $00,$40,$00,$00,$40
	db $00,$40,$00,$00,$40
	db $00,$40,$00,$00,$40
	db $00,$40,$00,$00,$40

PALETTE:	db $FF,$FF,$0A,$0A,$0A	;FF: .cfg PALETTE, else custom
	db $FF,$FF,$0A,$0A,$0A
	db $FF,$FF,$0A,$0A,$0A
	db $FF,$FF,$0A,$0A,$0A
	db $FF,$FF,$0A,$0A,$0A
	
TILEMAP:	db $E9,$E9,$DC,$DD,$DC ;walk frame one
	db $E9,$E9,$C4,$C5,$C4 ;different feet
	db $A4,$A4,$DC,$DD,$DC	;walk frame two
	db $A4,$A4,$C4,$C5,$C4 ;different feet
	db $A0,$EE,$C4,$C5,$C4	;squished

XDISP:	db $FC,$04,$FC,$04,$0C	;4px to the left
	db $FC,$04,$FC,$04,$0C
	db $FC,$04,$FC,$04,$0C
	db $FC,$04,$FC,$04,$0C
	db $FC,$04,$FC,$04,$0C

YDISP:	db $F8,$F8,$08,$08,$08	;-8px
	db $F8,$F8,$08,$08,$08	;-0px
	db $F8,$F8,$08,$08,$08
	db $F8,$F8,$08,$08,$08
	db $FA,$FA,$08,$08,$08	;-0px
	
GFX:
	LDA $15F6,x	;properties...
	STA !PROPRAM

	LDA $1570,x	;test dieing state
	CMP #!DIEING
	BNE NOTDIEING	;don't yflip if not dieing

	LDA #$80
	TSB !PROPRAM	;but set the v bit

NOTDIEING:
	LDA $1504,x	;load frame index
	ASL A		;x2
	ASL A		;x4
	CLC
	ADC $1504,x	;x5
	STA !FRAMEINDEX	;and into frame index

	LDX #$00	;reset loop index
OAM_LOOP:
	PHY		;preserve OAM index
	TYA
	LSR A
	LSR A
	TAY
	LDA SIZE,x	;use loop index
	STA $0460,y
	PLY

	PHX		;preserve loop index
	LDX !FRAMEINDEX	;load frame index

	LDA $00
	CLC
	ADC XDISP,x	;xpos (no direction applied)
	STA $0300,y

	LDA !PROPRAM	;test y flip
	BIT #$80
	BNE FLIPY
	
	LDA $01
	CLC
	ADC YDISP,x	;ypos
	STA $0301,y
	BRA DOCHR

FLIPY:

	LDA $01
	SEC
	SBC YDISP,x
	STA $0301,y
	LDA SIZE,x	;add 8 on 8x8 tiles
	BNE DOCHR

	LDA $0301,y
	CLC
	ADC #$08	;add
	STA $0301,y


DOCHR:
	LDA TILEMAP,x	;chr
	STA $0302,y

	LDA #$00	;start with zero
	BIT PALETTE,x
	BMI NOCUSTOMPAL
	
	LDA PALETTE,x

NOCUSTOMPAL:
	ORA !PROPRAM	;properties
	EOR PROP,x	;apply flip
	ORA $64
	STA $0303,y

	INY		;next tile
	INY
	INY
	INY

	INC !FRAMEINDEX	;advance frame index
	PLX		;restore loop index

	INX		;advance loop
	CPX #$05	;5 tiles
	BNE OAM_LOOP

	LDX $15E9	;restore sprite index
	LDY #$FF	;$0460
        LDA #$04        ;5 tiles
        JSL $01B7B3	;reserve

	RTS

SETUPCUST:
	LDA #$01
	STA $14C8,y		;normal status, run init routine
	LDA #!PROJECTILE

	PHX
	TYX
	STA $7FAB9E,x		;into sprite type table for custom sprites
	PLX
	
;set positions accordingly		
;restore gen sprite index

	LDA $14E0,x		;create base 16bit pos
	XBA
	LDA $E4,x
	REP #$20		;16bit add
	CLC
	ADC.w #!GENPOSX		;add offset
	SEP #$20		;8bit stores
	STA $00E4,y		;store new xpos
	XBA
	STA $14E0,y
	
	LDA $14D4,x		;base ypos
	XBA
	LDA $D8,x
	REP #$20
	CLC
	ADC.w #!GENPOSY		;add
	SEP #$20
	STA $00D8,y		;store
	XBA
	STA $14D4,y

	TYX			;new sprite slot into X
	JSL $07F7D2		;create sprite
	JSL $0187A7
        LDA #$88		;set as custom sprite
        STA $7FAB10,X

;set projectile speed according to Mario
	LDX $15E9		;restore sprite index

	LDA #!GENSPEEDX		;load speed to launch
	STA $00B6,y		;set xspeed

	LDA #!GENSPEEDY
	STA $00AA,y
	
	RTS			;return

;=================
;BORROWED ROUTINES
;=================

;POINTS
;======

SUB_STOMP_PTS:       PHY                     ; 
                    LDA $1697               ; \
                    CLC                     ;  } 
                    ADC $1626,x             ; / some enemies give higher pts/1ups quicker??
                    INC $1697               ; increase consecutive enemies stomped
                    TAY                     ;
                    INY                     ;
                    CPY #$08                ; \ if consecutive enemies stomped >= 8 ...
                    BCS NO_SOUND            ; /    ... don't play sound 
                    LDA STAR_SOUNDS,y             ; \ play sound effect
                    STA $1DF9               ; /   
NO_SOUND:            TYA                     ; \
                    CMP #$08                ;  | if consecutive enemies stomped >= 8, reset to 8
                    BCC NO_RESET            ;  |
                    LDA #$08                ; /
NO_RESET:            JSL $02ACE5             ; give mario points
                    PLY                     ;
                    RTS                     ; return



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_OFF_SCREEN
; This subroutine deals with sprites that have moved off screen
; It is adapted from the subroutine at $01AC0D
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
SPR_T12:             db $40,$B0
SPR_T13:             db $01,$FF
SPR_T14:             db $30,$C0,$A0,$C0,$A0,$F0,$60,$90		;bank 1 sizes
		            db $30,$C0,$A0,$80,$A0,$40,$60,$B0		;bank 3 sizes
SPR_T15:             db $01,$FF,$01,$FF,$01,$FF,$01,$FF		;bank 1 sizes
					db $01,$FF,$01,$FF,$01,$00,$01,$FF		;bank 3 sizes

SUB_OFF_SCREEN_X1:   LDA #$02                ; \ entry point of routine determines value of $03
                    BRA STORE_03            ;  | (table entry to use on horizontal levels)
SUB_OFF_SCREEN_X2:   LDA #$04                ;  | 
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X3:   LDA #$06                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X4:   LDA #$08                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X5:   LDA #$0A                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X6:   LDA #$0C                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X7:   LDA #$0E                ;  |
STORE_03:			STA $03					;  |            
					BRA START_SUB			;  |
SUB_OFF_SCREEN_X0:   STZ $03					; /

START_SUB:           JSR SUB_IS_OFF_SCREEN   ; \ if sprite is not off screen, return
                    BEQ RETURN_35           ; /
                    LDA $5B                 ; \  goto VERTICAL_LEVEL if vertical level
                    AND #$01                ; |
                    BNE VERTICAL_LEVEL      ; /     
                    LDA $D8,x               ; \
                    CLC                     ; | 
                    ADC #$50                ; | if the sprite has gone off the bottom of the level...
                    LDA $14D4,x             ; | (if adding 0x50 to the sprite y position would make the high byte >= 2)
                    ADC #$00                ; | 
                    CMP #$02                ; | 
                    BPL ERASE_SPRITE        ; /    ...erase the sprite
                    LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0756 VC:176 00 FL:205
                    AND #$01                ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0780 VC:176 00 FL:205
                    ORA $03                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0796 VC:176 00 FL:205
                    STA $01                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0820 VC:176 00 FL:205
                    TAY                     ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0844 VC:176 00 FL:205
                    LDA $1A                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0858 VC:176 00 FL:205
                    CLC                     ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0882 VC:176 00 FL:205
                    ADC SPR_T14,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0896 VC:176 00 FL:205
                    ROL $00                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizcHC:0928 VC:176 00 FL:205
                    CMP $E4,x               ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:0966 VC:176 00 FL:205
                    PHP                     ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:0996 VC:176 00 FL:205
                    LDA $1B                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizCHC:1018 VC:176 00 FL:205
                    LSR $00                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdiZCHC:1042 VC:176 00 FL:205
                    ADC SPR_T15,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizcHC:1080 VC:176 00 FL:205
                    PLP                     ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F0 P:eNvMXdizcHC:1112 VC:176 00 FL:205
                    SBC $14E0,x             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1140 VC:176 00 FL:205
                    STA $00                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1172 VC:176 00 FL:205
                    LSR $01                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1196 VC:176 00 FL:205
                    BCC SPR_L31             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1234 VC:176 00 FL:205
                    EOR #$80                ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1250 VC:176 00 FL:205
                    STA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1266 VC:176 00 FL:205
SPR_L31:             LDA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1290 VC:176 00 FL:205
                    BPL RETURN_35           ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1314 VC:176 00 FL:205
ERASE_SPRITE:        LDA $14C8,x             ; \ if sprite status < 8, permanently erase sprite
                    CMP #$08                ; |
                    BCC KILL_SPRITE         ; /    
                    LDY $161A,x             ;A:FF08 X:0007 Y:0001 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1108 VC:059 00 FL:2878
                    CPY #$FF                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1140 VC:059 00 FL:2878
                    BEQ KILL_SPRITE         ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1156 VC:059 00 FL:2878
                    LDA #$00                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1172 VC:059 00 FL:2878
                    STA $1938,y             ;A:FF00 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZcHC:1188 VC:059 00 FL:2878
KILL_SPRITE:         STZ $14C8,x             ; erase sprite
RETURN_35:           RTS                     ; return

VERTICAL_LEVEL:      LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ; \
                    LSR A                   ; | 
                    BCS RETURN_35           ; /
                    LDA $E4,x               ; \ 
                    CMP #$00                ;  | if the sprite has gone off the side of the level...
                    LDA $14E0,x             ;  |
                    SBC #$00                ;  |
                    CMP #$02                ;  |
                    BCS ERASE_SPRITE        ; /  ...erase the sprite
                    LDA $13                 ;A:0000 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:1218 VC:250 00 FL:5379
                    LSR A                   ;A:0016 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1242 VC:250 00 FL:5379
                    AND #$01                ;A:000B X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1256 VC:250 00 FL:5379
                    STA $01                 ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1272 VC:250 00 FL:5379
                    TAY                     ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1296 VC:250 00 FL:5379
                    LDA $1C                 ;A:001A X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0052 VC:251 00 FL:5379
                    CLC                     ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0076 VC:251 00 FL:5379
                    ADC SPR_T12,y           ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0090 VC:251 00 FL:5379
                    ROL $00                 ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:enVMXdizCHC:0122 VC:251 00 FL:5379
                    CMP $D8,x               ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0160 VC:251 00 FL:5379
                    PHP                     ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0190 VC:251 00 FL:5379
                    LDA.w $001D             ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F2 P:eNVMXdizcHC:0212 VC:251 00 FL:5379
                    LSR $00                 ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdiZcHC:0244 VC:251 00 FL:5379
                    ADC SPR_T13,y           ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdizCHC:0282 VC:251 00 FL:5379
                    PLP                     ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:envMXdiZCHC:0314 VC:251 00 FL:5379
                    SBC $14D4,x             ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0342 VC:251 00 FL:5379
                    STA $00                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0374 VC:251 00 FL:5379
                    LDY $01                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0398 VC:251 00 FL:5379
                    BEQ SPR_L38             ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0422 VC:251 00 FL:5379
                    EOR #$80                ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0438 VC:251 00 FL:5379
                    STA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0454 VC:251 00 FL:5379
SPR_L38:             LDA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0478 VC:251 00 FL:5379
                    BPL RETURN_35           ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0502 VC:251 00 FL:5379
                    BMI ERASE_SPRITE        ;A:8AFF X:0002 Y:0000 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0704 VC:184 00 FL:5490

SUB_IS_OFF_SCREEN:   LDA $15A0,x             ; \ if sprite is on screen, accumulator = 0 
                    ORA $186C,x             ; |  
                    RTS                     ; / return

;SUBHORZPOS
;==========

SUB_HORZ_POS:        LDY #$00                ;A:25D0 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1020 VC:097 00 FL:31642
                    SEP #$20
                    LDA $94                 ;A:25D0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZCHC:1036 VC:097 00 FL:31642
                    SEC                     ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1060 VC:097 00 FL:31642
                    SBC $E4,x               ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1074 VC:097 00 FL:31642
                    STA $0E                 ;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1104 VC:097 00 FL:31642
                    LDA $95                 ;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1128 VC:097 00 FL:31642
                    SBC $14E0,x             ;A:2500 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZcHC:1152 VC:097 00 FL:31642
                    BPL LABEL16             ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1184 VC:097 00 FL:31642
                    INY                     ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1200 VC:097 00 FL:31642
LABEL16:             RTS                     ;A:25FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:1214 VC:097 00 FL:31642

;SUBVERTPOS
;==========

SUB_VERT_POS:        LDY #$00               ;A:25A1 X:0007 Y:0001 D:0000 DB:03 S:01EA P:envMXdizCHC:0130 VC:085 00 FL:924
                    LDA $96                ;A:25A1 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdiZCHC:0146 VC:085 00 FL:924
                    SEC                    ;A:2546 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdizCHC:0170 VC:085 00 FL:924
                    SBC $D8,x              ;A:2546 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdizCHC:0184 VC:085 00 FL:924
                    STA $0F                ;A:25D6 X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0214 VC:085 00 FL:924
                    LDA $97                ;A:25D6 X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0238 VC:085 00 FL:924
                    SBC $14D4,x            ;A:2501 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdizcHC:0262 VC:085 00 FL:924
                    BPL LABEL11            ;A:25FF X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0294 VC:085 00 FL:924
                    INY                    ;A:25FF X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0310 VC:085 00 FL:924
LABEL11:             RTS                    ;A:25FF X:0007 Y:0001 D:0000 DB:03 S:01EA P:envMXdizcHC:0324 VC:085 00 FL:924 