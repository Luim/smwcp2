;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Score, by cstutor89
;; Displays the score (points you have obtained from minigames)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

TILEMAP: db $76,$77,$46,$47,$C2,$C3,$D2,$D3,$D4,$D5,$4D,$67

print "INIT ",pc
Init:	RTL      

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
Main:	PHB
	PHK
	PLB		
	JSR Run
	PLB
	RTL					;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Run:	LDA $7FAB10,x
 	AND #$04
	BNE SKIP
 	JSR GFX
	RTS
SKIP:	JSR GFXTAR
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; draw the timer
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GFX:

	; we *should* be able to get away with hardcoding this
	LDY #$80

	LDA #$58
	STA $0200,y	;xpos 'P'
	STA $0228,y		;xpos 'P'
	CLC
	ADC #$08
	STA $0204,y	;xpos 'E'
	STA $022C,y		;xpos 'L'
	CLC
	ADC #$08
	STA $0208,y	;xpos 'N'
	STA $0230,y		;xpos 'A'
	CLC
	ADC #$08
	STA $020C,y	;xpos 'A'
	STA $0234,y		;xpos 'Y'
	CLC
	ADC #$08
	STA $0210,y	;xpos 'L'
	STA $0238,y		;xpos 'E'
	CLC
	ADC #$08
	STA $0214,y	;xpos 'T'
	STA $023C,y		;xpos 'D'
	CLC
	ADC #$08
	STA $0218,y	;xpos 'Y'
	STA $0240,y		;xpos ':'
	CLC
	ADC #$08
	STA $021C,y	;xpos ':'
	CLC
	ADC #$10
	STA $0224,y	;xpos NUM1
	CLC
	ADC #$08
	STA $0220,y	;xpos NUM2
	STA $0244,y		;xpos NUM2

	LDA #$04
	CLC
	ADC #$00
	STA $0201,y	;ypos 'P'
	STA $0205,y	;ypos 'E'
	STA $0209,y	;ypos 'N'
	STA $020D,y	;ypos 'A'
	STA $0211,y	;ypos 'L'
	STA $0215,y	;ypos 'T'
	STA $0219,y	;ypos 'Y'
	STA $021D,y	;ypos ':'
	STA $0221,y	;ypos NUM1
	STA $0225,y	;ypos NUM2
	CLC : ADC #$08
	STA $0229,y	;ypos 'P'
	STA $022D,y	;ypos 'L'
	STA $0231,y	;ypos 'A'
	STA $0235,y	;ypos 'Y'
	STA $0239,y	;ypos 'E'
	STA $023D,y	;ypos 'D'
	STA $0241,y	;ypos ':'
	STA $0245,y	;ypos NUM2

	
	LDA #$CF	; P
	STA $0202,y
	LDA #$C4	; E
	STA $0206,y
	LDA #$CD	; N
	STA $020A,y
	LDA #$C0	; A
	STA $020E,y
	LDA #$CB	; L
	STA $0212,y
	LDA #$D3	; T
	STA $0216,y
	LDA #$D8	; Y
	STA $021A,y
	LDA #$FF	; :
	STA $021E,y
	PHX
	LDA $7F9A81
LOOP:	TAX
	CMP #$0A
	BCC E_LOOP
	SEC
	SBC #$0A
	BRA LOOP
E_LOOP: LDA TILEMAP,x
	STA $0222,y
	LDX #$00
	LDA $7F9A81
LOOP2:	CMP #$0A
	BCC E_LP2
	SEC
	SBC #$0A
	INX
	BRA LOOP2;
E_LP2:	LDA TILEMAP,x
	STA $0226,y
	PLX



	LDA #$CF	; P
	STA $022A,y
	LDA #$CB	; L
	STA $022E,y
	LDA #$C0	; A
	STA $0232,y
	LDA #$D8	; Y
	STA $0236,y
	LDA #$C4	; E
	STA $023A,y
	LDA #$C3	; D
	STA $023E,y
	LDA #$FF	; :
	STA $0242,y

	PHX
	LDX #$04
	PHY
	LDY #$00
-	LDA $7F9A83
	AND DoorsWhich,x
	BEQ +
	INY
+	DEX
	BPL -
	TYX
	LDA TILEMAP,x
	PLY
	STA $0246,y
	PLX

	LDA #$39	;formerly 39
	STA $0203,y	; P
	STA $0207,y	; E
	STA $020B,y	; N
	STA $020F,y	; A
	STA $0213,y	; L
	STA $0217,y	; T
	STA $021B,y	; Y
	STA $021F,y	; :
	STA $022B,y	; P
	STA $022F,y	; L
	STA $0233,y	; A
	STA $0237,y	; Y
	STA $023B,y	; E
	STA $023F,y	; D
	STA $0243,y	; :
	LDA #$38
	STA $0223,y	;prop
	STA $0227,y	;prop
	STA $0247,y

	LDX $15E9	;restore sprite index
	LDY #$00	;8X16
	LDA #$00	;2 tile
	JSL $81B7B3	;reserve

	; better set the tile sizes manually too
	PHX : LDX #17
-	STZ $0440,x
	DEX : BPL -
	PLX



	RTS

DoorsWhich:	db $01,$02,$04,$08,$10

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; draw the timer
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GFXTAR:	LDA #$C0
	STA $0200,y	;xpos TARGET
	CLC
	ADC #$0E
	STA $0204,y	;xpos NUM1 TARGET
	CLC
	ADC #$08
	STA $0208,y	;xpos NUM2 TARGET
	CLC
	ADC #$08
	STA $020C,y	;xpos '/'
	CLC
	ADC #$08
	STA $0210,y	;xpos '2'
	CLC
	ADC #$08
	STA $0214,y	;xpos '0'
	LDA #$C0
	STA $0218,y	;xpos BULLET
	CLC
	ADC #$0E
	STA $021C,y	;xpos NUM1 BULLET
	CLC
	ADC #$08
	STA $0220,y	;xpos NUM2 BULLET
	CLC
	ADC #$08
	STA $0224,y	;xpos '/'
	CLC
	ADC #$08
	STA $0228,y	;xpos '2'
	CLC
	ADC #$08
	STA $022C,y	;xpos '5'

	LDA #$28
	STA $0201,y	;xpos TARGET
	STA $0205,y	;xpos NUM1 TARGET
	STA $0209,y	;xpos NUM2 TARGET
	STA $020D,y	;xpos '/'
	STA $0211,y	;xpos '2'
	STA $0215,y	;xpos '0'
	LDA #$30
	STA $0219,y	;xpos BULLET
	STA $021D,y	;xpos NUM1 BULLET
	STA $0221,y	;xpos NUM2 BULLET
	STA $0225,y	;xpos '/'
	STA $0229,y	;xpos '2'
	STA $022D,y	;xpos '5'

	LDA #$CA
	STA $0202,y
	LDA #$CB
	STA $020E,y
	LDA #$46
	STA $0212,y
	LDA #$76
	STA $0216,y
	LDA #$DA
	STA $021A,y
	LDA #$CB
	STA $0226,y
	LDA #$46
	STA $022A,y
	LDA #$C3
	STA $022E,y

	PHX
	LDA $7F9A90
LOOP3:	TAX
	CMP #$0A
	BCC E_LOOP3
	SEC
	SBC #$0A
	BRA LOOP3
E_LOOP3: LDA TILEMAP,x
	STA $020A,y
	LDX #$00
	LDA $7F9A90
LOOP4:	CMP #$0A
	BCC E_LP4
	SEC
	SBC #$0A
	INX
	BRA LOOP4
E_LP4:	LDA TILEMAP,x
	STA $0206,y
	PLX
	PHX
	LDA $7F9A91
LOOP5:	TAX
	CMP #$0A
	BCC E_LOOP5
	SEC
	SBC #$0A
	BRA LOOP5
E_LOOP5: LDA TILEMAP,x
	STA $0222,y
	LDX #$00
	LDA $7F9A91
LOOP6:	CMP #$0A
	BCC E_LP6
	SEC
	SBC #$0A
	INX
	BRA LOOP6
E_LP6:	LDA TILEMAP,x
	STA $021E,y
	PLX

	LDA #$38
	STA $0203,y	;prop
	STA $0207,y	;prop
	STA $020B,y	;prop
	STA $020F,y	;prop
	STA $0213,y	;prop
	STA $0217,y	;prop
	LDA #$35
	STA $021B,y	;prop
	LDA #$38
	STA $021F,y	;prop
	STA $0223,y	;prop
	STA $0227,y	;prop
	STA $022B,y	;prop
	STA $022F,y	;prop

	LDX $15E9	;restore sprite index
	LDY #$00	;8X16
	LDA #$00	;2 tile
	JSL $81B7B3	;reserve
	RTS