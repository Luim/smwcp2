lorom

!goal_flag = $1B97

org $00E9A1
	autoclean JML screen_border_check

freecode

screen_border_check:
	REP #$20
	LDA !goal_flag
	BEQ .no_goal
	LDA $7E
	CMP #$00EA
	SEP #$20
	BCC .not_offscreen
	LDA #$FA		; don't fall.
	STA $7D
	STZ $72
	LDA #$04
	TSB $77
	; STA $13EF
	
	
.not_offscreen
	JML $80E9A0
	
.no_goal
	SEP #$20
	LDA $7E
	CMP.b #$F0
	JML $80E9A5
