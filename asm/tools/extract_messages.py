import snesproc
import sys
import re

def chunked(inseq,chunk):
    return (inseq[i:i+chunk] for i in range(0, len(inseq), chunk))

def read_message(data, address):
	byte = snesproc.read_bytes(data, address, 1)
	message = [byte]
	while byte != 0xFE:
		address += 1
		byte = snesproc.read_bytes(data, address, 1)
		message.append(byte)
	special_chars = {
		0x1A: "!",
		0x1B: ".",
		0x1C: "-",
		0x1D: ",",
		0x1E: "?",
		0x1F: " ",
		0x5A: "#",
		0x5B: "(",
		0x5C: ")",
		0x5D: "'"
	}
	def convert(char):
		if char in special_chars:
			return special_chars[char]
		elif 0 <= char <= 0x19:
			return chr(char+65)
		elif 0x40 <= char <= 0x59:
			return chr(char-0x40+97)
		else:
			return char

	return map(convert, message)

def create_message(message):
	out_message = []
	max_line_length = 18
	is_character = re.compile(r"[a-zA-Z\!\.\-\,\? \#\(\)']")

	for line in chunked(message, max_line_length):
		outline = "db "
		in_string = False
		for char in line:
			if str(char) == char and is_character.match(char):
				if in_string:
					outline += char
				elif outline == "db ":
					outline += "\"{}".format(char)
					in_string = True
				else:
					outline += ",\"{}".format(char)
					in_string = True
			else:
				if in_string:
					outline += "\", ${:02X}".format(char)
					in_string = False
				elif outline == "db ":
					outline += "${:02X}".format(char)
				else:
					outline += ",${:02X}".format(char)
		if in_string:
			outline += "\""
		out_message.append(outline)
	return out_message



def main(argv=None):
	if argv is None:
		argv = sys.argv[1:]

	rom_name = argv[0]
	if len(argv) > 1:
		out_file_name = argv[1]
	else:
		out_file_name = "messages.txt"

	with open(rom_name, "rb") as infile:
		rom_data = infile.read()

	messages_address = snesproc.LoRomtoPC(
						snesproc.read_bytes(rom_data, snesproc.LoRomtoPC(0x03BC0B, header=True), 3),
						header=True)
	message_offsets_address = snesproc.LoRomtoPC(0x03BE80, header=True)

	messages = []

	with open(out_file_name, "w") as outfile:
		for i in xrange (0, (0x25*2+0x3B*2)*2, 2):
			level = i/2/2
			if level > 0x24:
				level += 0x100 - 0x24
			message_num = i/2%2+1

			message_offset = snesproc.read_bytes(rom_data, message_offsets_address + i, 2)
			message_address = messages_address + message_offset
			message = create_message(read_message(rom_data, message_address))
			messages.append(message)

			outfile.write("message_{:03X}_{}:\n".format(level, message_num))
			for line in message:
				outfile.write("    {}\n".format(line))


	return 0

if __name__ == "__main__":
	exit(main())