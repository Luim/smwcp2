#SPC
{
	#author "Moose"
	#comment "Boss battle theme"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#AMM
;	#sky_and_dream
	"CCymbal.brr"
}

"Hihat=@4 (\"CCymbal.brr\", $04) $ed $7f $f6"
"Bend=$dd $00 $0c"
#0
t75
w160
@17
v230
$ef $97 $0d $0d
$f1 $07 $13 $01
$de $30 $10 $90
[q7f
o3 c4 d+16 r16 c16 r16 c8 d+16 r16 c16 r16 c16 r16 d+8 c16 r16 c16 r16 d+16 r16 c8 c16 r16 f4 c4 d+16 r16 c16 r16 c8 d+16 r16 c16 r16 c16 r16 d+8 c16 r16 d+16 r16 f2 ^8 ]2
q7f
o4 c4 d+16 r16 c16 r16 c8 d+16 r16 c16 r16 c16 r16 d+8 c16 r16 c16 r16 d+16 r16 c8 c16 r16 f4 c4 d+16 r16 c16 r16 c8 d+16 r16 c16 r16 c16 r16 d+8 c16 r16 d+16 r16 f2 ^8 c4 d+16 r16 c16 r16 c8 d+16 r16 c16 r16 c16 r16 d+8 c16 r16 c16 r16 d+16 r16 c8 c16 r16 f4 c+2 c+8 d+4 c2 ^2 ^8 
(2)[r2 ^2 ]4
@4 $ed $2a $69
v190
(3)[o4 f4 c16 r16 c16 r16 f16 r16 g+4 g2 d+4 ^8 d+4 d4 d16 r16 d4 ^8 d8 d8 c+8 ^16 c+8 ^16 c+8 c4 c4 ]
o4 f4 c16 r16 c16 r16 f16 r16 g+4 d+4 ^8 c16 r16 c16 r16 d+16 r16 g4 r8 
(2)2
(3)
@17
v230
[o3 c4 d+16 r16 c16 r16 c8 d+16 r16 c16 r16 c16 r16 d+8 c16 r16 c16 r16 d+16 r16 c8 c16 r16 f4 c4 d+16 r16 c16 r16 c8 d+16 r16 c16 r16 c16 r16 d+8 c16 r16 d+16 r16 f2 ^8 ]2
o4 c4 d+16 r16 c16 r16 c8 d+16 r16 c16 r16 c16 r16 d+8 c16 r16 c16 r16 d+16 r16 c8 c16 r16 f4 c4 d+16 r16 c16 r16 c8 d+16 r16 c16 r16 c16 r16 d+8 c16 r16 d+16 r16 f2 ^8 c4 d+16 r16 c16 r16 c8 d+16 r16 c16 r16 c16 r16 d+8 c16 r16 c16 r16 f4 ^8 d+4 c+2 c+8 d+4 c2 ^4 ^8 c4 c+2 c+8 d+8 r8 f2 ^4 ^8 f4 @4 $ed $2a $69
v190
(2)4
(7)[o4 g4 d16 r16 d16 r16 g16 r16 a+4 a2 f4 ^8 f4 e4 e16 r16 e4 ^8 e8 e8 d+8 ^16 d+8 ^16 d+8 d4 d4 ]
o4 g4 d16 r16 d16 r16 g16 r16 a+4 f4 ^8 d16 r16 d16 r16 f16 r16 a4 r8 
(2)2
(7)
o4 f4 ^8 f4 ^8 f4 g8 ^16 f8 ^16 d+8 f+8 r8 a8 r8 @17
v230
[< d4 f16 r16 d16 r16 d8 f16 r16 d16 r16 d16 r16 f8 d16 r16 d16 r16 f16 r16 d8 d16 r16 g4 d4 f16 r16 d16 r16 d8 f16 r16 d16 r16 d16 r16 f8 d16 r16 f16 r16 g2 ^8 ]2


#1
@4 $ed $2a $69
$de $30 $10 $90
v190

(2)15
q7f
r8 o4 c8 e8 g8 > c8 < c8 e8 g8
g+4 r4 f8 ^16 g8 ^16 g+8 a+8 ^16 g+8 ^16 g8 d+4 r4 d8 d+8 f8 b4 Bend $b0 ^8 f16 r16 g16 r16 g+8 ^16 g8 ^16 f8 e4 g4 
(11)[g4 Bend $ac f16 r16 f16 r16 > c16 r16 f4 d+2 < a+4 ^8 a+4 > c4 < f16 r16 f4 ^8 f8 g8 g8 Bend $ac ^16 g8 ^16 f8 e4 g4 ]
o4 g+4 f16 r16 f16 r16 g+16 r16 > c4 < f+4 Bend $ab ^8 d+16 r16 d+16 r16 g16 r16 > c4 r8 < d8 d+16 r16 f16 r16 b4 Bend $b0 ^8 f16 r16 g16 r16 g8 Bend $ac ^16 g8 ^16 f8 e4 g4 
(11)
o4 f2 ^2 ^2 ^2 
(2)6
o4 f+4 Bend $ab a+16 r16 g16 r16 g8 a+16 r16 g16 r16 g16 r16 a+8 g16 r16 g16 r16 a+16 r16 g8 g16 r16 > c8.^16 $dd $00 $0c $ac < f+4 Bend $ab a+16 r16 g16 r16 g8 a+16 r16 g16 r16 g16 r16 a+8 g16 r16 a+16 r16 a+2 Bend $b0 ^8 g4 a+16 r16 g16 r16 g8 a+16 r16 g16 r16 g16 r16 a+8 g16 r16 g16 r16 > c4 ^8 < a+4 g+2 g+8 a+4 g2 ^4 ^8 r4 r2 ^2 r8 c8 f8 g+8 > c8 < c8 f8 g+8 g4 r8 d16 r16 g8 ^16 a8 ^16 a+8 > c8 ^16 < a+8 ^16 a8 f4 r4 e8 f8 g8 b4 Bend $b0 ^8 g16 r16 a16 r16 a+8 ^16 a8 ^16 g8 f+4 a4 
(13)[a4 Bend $ae g16 r16 g16 r16 > d16 r16 g4 f2 c4 ^8 c4 d4 < g16 r16 g4 ^8 g8 a8 a8 Bend $ae ^16 a8 ^16 g8 f+4 a4 ]
o4 a+4 g16 r16 g16 r16 a+16 r16 > d4 < g+4 Bend $ad ^8 f16 r16 f16 r16 a16 r16 > d4 r8 < e8 f16 r16 g16 r16 > c4 Bend $b2 ^8 < g16 r16 a16 r16 a8 Bend $ae ^16 a8 ^16 g8 f+4 a4 
(13)
o5 d4 ^8 < a4 ^8 > d4 d+8 ^16 d8 ^16 c8 d8 r8 f+8 r8
$df
g2 ^2 ^2 ^2 ^2^2 ^2 ^2 
(2)4


#2
@14 $ed $7f $ef
v255
(2)7
q7f
r2 ^4 o2 f4 $dd $0c $24 $85
(16)[< f8 f8 f8 f8 f8 f8 f8 f8 ]6
o2 c+8 c+8 c+8 c+8 c+8 c+8 c+8 c8 c2 ^4 c4 
[< f8 f8 f8 f8 f8 f8 f8 f8 d+8 d+8 d+8 d+8 d+8 d+8 d+8 d+8 d8 d8 d8 d8 d8 d8 d8 d8 c+8 c+8 c+8 d+8 d+8 d+8 c8 c8 ]4
(16)14
o2 c+8 c+8 c+8 c+8 c+8 c+8 c+8 c8 c8 c8 c8 c8 c8 c8 c8 c8 c+8 c+8 c+8 c+8 c+8 d+8 d+8 f8 f8 f8 f8 f8 f8 f8 f8 f8 
[< g8 g8 g8 g8 g8 g8 g8 g8 f8 f8 f8 f8 f8 f8 f8 f8 e8 e8 e8 e8 e8 e8 e8 e8 d+8 d+8 d+8 f8 f8 f8 d8 d8 ]4
o1 f8 f8 f8 f8 f8 f8 f8 f8 d+8 d+8 d+8 d+8 d8 d8 d8 d8 
[g8 g8 g8 g8 g8 g8 g8 g8 ]4
(2)4


#3
v225
Hihat
(22)[q0b o5 g8 g8 q7c $ed $7e $f7 $f4 $03 g+8 q0b Hihat $f4 $03 g8 ]30
q7f
r2 ^2 
(22)64
(2)2
(22)32
(2)2
(22)8
(2)4


#4
v230
@17
(2)16
$de $30 $10 $90
[o3 f4 f16 r16 f16 r16 f4 f16 r16 f16 r16 d+4 d+16 r16 d+16 r16 d+4 d+16 r16 d+16 r16 d4 d16 r16 d16 r16 d4 d16 r16 d16 r16 c+4 c+16 r16 d+4 d+16 r16 c4 ]4
@4 $ed $2a $69
v190
q7f
o4 c2 ^2 ^2 ^2 
(2)16
@17
v230
[o3 g4 g16 r16 g16 r16 g4 g16 r16 g16 r16 f4 f16 r16 f16 r16 f4 f16 r16 f16 r16 e4 e16 r16 e16 r16 e4 e16 r16 e16 r16 d+4 d+16 r16 f4 f16 r16 d4 ]4
o3 f4 f16 r16 f16 r16 f4 f16 r16 f16 r16 d+4 d+16 r16 d+16 r16 d4 d16 r16 d16 r16
@4 $ed $2a $69
v190 $df
o4 a+2 ^2 ^2 ^2 ^2 ^2 ^2 ^2 
(2)4


#5
v255
$f4 $03
(2)4
(33)[$f4 $03 @21c8 r8 $f4 $03 @29c8 r8 ]7
(34)[@29c16 @29c16 @29c16 @29c16 @29c16 @29c16 @29c16 @29c16 ]
(33)15
(34)
(33)31
(34)
(33)35
(34)
(33)35
(34)
(33)7
(34)
(2)4
                

#amk=1
