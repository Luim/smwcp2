incsrc subroutinedefs_xkas.asm

print "INIT ",pc
	JSR SubHorzPos
	TYA
	STA $157C,x
	RTL

print "MAIN ",pc
	PHB
	PHK
	PLB
	JSR Code
	PLB
	RTL

Speed:
	db $E0,$20

More:
	INC $1534,x
End:
	BRL Return
Code:
	JSR Draw
	LDA $9D
	BNE End
	LDA $14C8,x
	CMP #$08
	BNE End
	JSR SubOffScreenX0
	JSL $01801A
	JSL $018022
	JSL $019138
	JSL $01A7DC
	BCC End
	LDA $140D
	ORA $187A
	BNE Spin
Hurt:
	JSL $00F5B7
Return:
	RTS
Spin:
	LDA $0E
	CMP #$E6
	BPL Hurt
	JSL $01AB99
	JSL $01AA33
	LDA #$02
	STA $1DF9
	RTS

Anime:
	db $AA,$AC
Flip:
	db $49,$09

Draw:
	JSL !GetDrawInfo
	LDA $157C,x
	STA $02
	LDA $00
	STA $0300,y
	LDA $01
	STA $0301,y
	LDA $14
	LSR A
	LSR A
	LSR A
	AND #$01
	PHX
	TAX
	LDA Anime,x
	STA $0302,y
	LDX $02
	LDA Flip,x
	PLX
	ORA $64
	STA $0303,y
	LDY #$02
	LDA #$00
	JSL $01B7B3
	RTS
	

SpriteTable1:
	db $0C,$1C
SpriteTable2:
	db $01,$02


SpriteTable12:
	db $40,$B0
SpriteTable13:
	db $01,$FF
SpriteTable14:
	db $30,$C0,$A0,$C0,$A0,$F0,$60,$90
	db $30,$C0,$A0,$80,$A0,$40,$60,$B0
SpriteTable15:
	db $01,$FF,$01,$FF,$01,$FF,$01,$FF
	db $01,$FF,$01,$FF,$01,$00,$01,$FF

SubOffScreenX1:
	LDA #$02
	BRA Store03
SubOffScreenX2:
	LDA #$04
	BRA Store03
SubOffScreenX3:
	LDA #$06
	BRA Store03
SubOffScreenX4:
	LDA #$08
	BRA Store03
SubOffScreenX5:
	LDA #$0A
	BRA Store03
SubOffScreenX6:
	LDA #$0C
	BRA Store03
SubOffScreenX7:
	LDA #$0E
Store03:
	STA $03
	BRA StartSub
SubOffScreenX0:
	STZ $03
StartSub:
	JSR SubIsOffScreen
	BEQ Return35
	LDA $5B
	AND #$01
	BNE VerticalLevel
	LDA $D8,x
	CLC
	ADC #$50
	LDA $14D4,x
	ADC #$00
	CMP #$02
	BPL EraseSprite
	LDA $167A,x
	AND #$04
	BNE Return35
	LDA $13
	AND #$01
	ORA $03
	STA $01
	TAY
	LDA $1A
	CLC
	ADC SpriteTable14,y
	ROL $00
	CMP $E4,x
	PHP
	LDA $1B
	LSR $00
	ADC SpriteTable15,y
	PLP
	SBC $14E0,x
	STA $00
	LSR $01
	BCC SpriteLabel31
	EOR #$80
	STA $00
SpriteLabel31:
	LDA $00
	BPL Return35
EraseSprite:
	LDA $14C8,x
	CMP #$08
	BCC KillSprite
	LDY $161A,x
	CPY #$FF
	BEQ KillSprite
	LDA #$00
	STA $1938,y
KillSprite:
	STZ $14C8,x
Return35:
	RTS
VerticalLevel:
	LDA $167A,x
	AND #$04
	BNE Return35
	LDA $13
	LSR A
	BCS Return35
	LDA $E4,x
	CMP #$00
	LDA $14E0,x
	SBC #$00
	CMP #$02
	BCS EraseSprite
	LDA $13
	LSR A
	AND #$01
	STA $01
	TAY
	LDA $1C
	CLC
	ADC SpriteTable12,y
	ROL $00
	CMP $D8,x
	PHP
	LDA.w $001D
	LSR $00
	ADC SpriteTable13,y
	PLP
	SBC $14D4,x
	STA $00
	LDY $01
	BEQ SpriteLabel38
	EOR #$80
	STA $00
SpriteLabel38:
	LDA $00
	BPL Return35
	BMI EraseSprite
SubIsOffScreen:
	LDA $15A0,x
	ORA $186C,x
	RTS

SubHorzPos:
	LDY #$00
	LDA $94
	SEC
	SBC $E4,x
	STA $0F
	LDA $95
	SBC $14E0,x
	BPL Label16
	INY
Label16:
	RTS