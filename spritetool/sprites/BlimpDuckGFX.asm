
DrawSparkles:
	LDA !attacked+7
	BEQ +
	LDA $14
	AND #$01
	BNE +
	LDA $14E0,x
	BNE +
	PHY
	LDA $00
	PHA
	;LDA $01
	;PHA
	
	JSL $01ACF9
	LDA $148D
	AND #$0F
	CLC
	ADC $00
	STA $02
	
	LDA $148E
	AND #$07
	CLC
	ADC $01
	CLC
	ADC $1C
	STA $00

	JSL $8285BA
	;PLA
	;STA $01
	PLA
	STA $00
	
	PLY
+
RTS


IdleGFX:
{
	PHX
	LDX #$00
	LDA $14
	CLC
	ADC #$0F
	AND #$3F
	CMP #$1F
	BCS +
	LDX #$02
	+
	STX $0F
	PLX
	
	
	LDA !timeToSpawn
	CMP #$04
	BCC +
	CMP #$09
	BCS +
	LDA $14
	AND #$01
	ASL #5
	CLC
	ADC #$8E
	STA $0E
	JMP DrawPressingButton
+

	LDA $14
	AND #$04
	LSR
	STA $0E
	
	LDA !timeToSpawn
	CMP #$0F
	BCS .drawNone
	CMP #$0D
	BCS .drawFirst
	CMP #$02
	BCS .drawSecond
	CMP #$00
	BCS .drawFirst
	BRA .drawNone
.drawFirst
	LDA #$CE
	BRA +
.drawSecond
	LDA #$EE
+
	STA $0302,y
	
	LDA  $00 : 		    STA $0300,y
	LDA  $01 : CLC : ADC #$10 : STA $0301,y
	LDA #$3D : ORA !facing    : STA $0303,y
	INY #4
.drawNone

	LDA  $00 : 		    STA $0300,y
	LDA  $01 : 		    STA $0301,y
	LDA #$00 : CLC : ADC $0F  : STA $0302,y
	LDA #$3D : ORA !facing    : STA $0303,y
	INY #4

	LDA  $00 : 		    STA $0300,y
	LDA  $01 : CLC : ADC #$10 : STA $0301,y
	LDA #$20 : CLC : ADC $0F  : STA $0302,y
	LDA #$3D : ORA !facing :    STA $0303,y
	INY #4

	LDA  $00 : 		    STA $0300,y
	LDA  $01 : CLC : ADC #$18 : STA $0301,y
	LDA #$30 : CLC : ADC $0F  : STA $0302,y
	LDA #$3D : ORA !facing :    STA $0303,y
	INY #4
	
	LDA  $00 :		    STA $0300,y
	LDA  $01 : CLC : ADC #$18 : STA $0301,y
	LDA #$EA : CLC : ADC $0E  : STA $0302,y
	LDA #$3D : ORA !facing :    STA $0303,y
	INY #4
	TYA
	LSR
	LSR
	DEC
	
	LDY #$02
	JSL $01B7B3

	RTS


}

HurtGFX:
{
	LDA !facing
	STA $0F
.goingDown
	PHY
	PHX
	LDA !facing
	LSR #6
	TAX

	LDA  $00 : CLC : ADC FOLWX,x :	STA $0300,y
	LDA  $01 : CLC : ADC #$10    :	STA $0301,y
	LDA #$09 :			STA $0302,y
	LDA #$3D : ORA $0F	     :	STA $0303,y
	INY #4

	LDA  $00 : CLC : ADC FORWX,x :	STA $0300,y
	LDA  $01 : CLC : ADC #$10    :	STA $0301,y
	LDA #$08 :			STA $0302,y
	LDA #$3D : ORA $0F	     :	STA $0303,y
	INY #4
	
	LDA  $00 : 		    STA $0300,y
	LDA  $01 : 		    STA $0301,y
	LDA #$0C : 		    STA $0302,y
	LDA #$3D : ORA $0F	 :  STA $0303,y
	INY #4

	LDA  $00 : 		    STA $0300,y
	LDA  $01 : CLC : ADC #$10 : STA $0301,y
	LDA #$2C : 		    STA $0302,y
	LDA #$3D : ORA $0F	 :    STA $0303,y
	INY #4

	LDA $0E
	TAX

	LDA  $00 : 		    STA $0300,y
	LDA  $01 : CLC : ADC #$18 : STA $0301,y
	LDA #$3C : 		    STA $0302,y
	LDA #$3D : ORA $0F	 :    STA $0303,y

	PLX
	LDA #$04
	LDY #$02
	JSL $01B7B3		;Draw all tiles

	PLY
	TYA
	LSR
	LSR			
	TAY				
	LDA $0460,y		;Adjust some of them to be 8x8 instead of 16x16.
	AND #$01
	STA $0460,y
	INY	
	LDA $0460,y
	AND #$01
	STA $0460,y
	

	RTS

}

FORWX: db $F8, $10
FOLWX: db $10, $F8
FOBT:  db $34, $3A

FOMT:  db $26, $28
FOBT2: db $46, $48

FlyOffGFX:
{
	LDA $14
	LSR
	LSR
	AND #$01
	STA $0E

	LDA !facing
	STA $0F
	LDA $AA,x
	BPL .goingDown
	CMP #$F8
	BCS .goingDown
	JMP .goingUp
.goingDown
	PHY
	PHX
	LDA !facing
	LSR #6
	TAX

	LDA  $00 : CLC : ADC FOLWX,x :	STA $0300,y
	LDA  $01 : CLC : ADC #$10    :	STA $0301,y
	LDA #$09 :			STA $0302,y
	LDA #$3D : ORA $0F	     :	STA $0303,y
	INY #4

	LDA  $00 : CLC : ADC FORWX,x :	STA $0300,y
	LDA  $01 : CLC : ADC #$10    :	STA $0301,y
	LDA #$08 :			STA $0302,y
	LDA #$3D : ORA $0F	     :	STA $0303,y
	INY #4
	
	LDA  $00 : 		    STA $0300,y
	LDA  $01 : 		    STA $0301,y
	LDA #$04 : 		    STA $0302,y
	LDA #$3D : ORA $0F	 :  STA $0303,y
	INY #4

	LDA  $00 : 		    STA $0300,y
	LDA  $01 : CLC : ADC #$10 : STA $0301,y
	LDA #$24 : 		    STA $0302,y
	LDA #$3D : ORA $0F	 :    STA $0303,y
	INY #4

	LDA $0E
	TAX

	LDA  $00 : 		    STA $0300,y
	LDA  $01 : CLC : ADC #$18 : STA $0301,y
	LDA FOBT,x : 		    STA $0302,y
	LDA #$3D : ORA $0F	 :    STA $0303,y

	PLX
	LDA #$04
	LDY #$02
	JSL $01B7B3		;Draw all tiles

	PLY
	TYA
	LSR
	LSR			
	TAY				
	LDA $0460,y		;Adjust some of them to be 8x8 instead of 16x16.
	AND #$01
	STA $0460,y
	INY	
	LDA $0460,y
	AND #$01
	STA $0460,y
	

	RTS

.goingUp
	LDA $00 		  : STA $0300,y
	LDA $01 		  : STA $0301,y
	LDA #$06 		  : STA $0302,y
	LDA #$3D : ORA !facing    : STA $0303,y
	INY #4
	
	LDA $00		  		     : STA $0300,y
	LDA $01             : CLC : ADC #$08 : STA $0301,y
	LDA $14  : AND #$02 : CLC : ADC #$16 : STA $0302,y
	LDA #$3D : ORA !facing               : STA $0303,y
	INY #4

	LDA $00		  		     : STA $0300,y
	LDA $01             : CLC : ADC #$10 : STA $0301,y
	LDA $14  : AND #$02 : CLC : ADC #$26 : STA $0302,y
	LDA #$3D : ORA !facing               : STA $0303,y
	INY #4
	
	LDA $00		  		     : STA $0300,y
	LDA $01             : CLC : ADC #$20 : STA $0301,y
	LDA $14  : AND #$02 : CLC : ADC #$46 : STA $0302,y
	LDA #$3D : ORA !facing               : STA $0303,y
	INY #4


	LDA #$03
	LDY #$02
	JSL $01B7B3		;Draw all tiles

	RTS
}
	

BulletBillPrepGFX:
{
	JMP GFXFinish
}

BulletBillAttackGFX:
{
		PHX
	LDX #$00	;
	LDA $14		;
	CLC		;
	ADC #$0F	;
	AND #$3F	;
	CMP #$1F	; $0F = frame adder
	BCS +		;
	LDX #$02	;
	+		;
	STX $0F		;
	
	LDX #$AE	;
	LDA $14		;
	AND #$1F	;
	CMP #$04	; $0E = Current button tile
	BCS +		;
	LDX #$8E	;
	+		;
	STX $0E		;
	

	PLX
	
DrawPressingButton:

	PHY
	
	LDY #$10	;
	LDA !facing	; $0D = arm/button x offset.
	BEQ +		;
	LDY #$F0	;
+			;
	STY $000D		;
	
	PLY
						; Used during the idle summon.	
	LDA  $00 : 		    STA $0300,y
	LDA  $01 : 		    STA $0301,y
	LDA #$80 :		    STA $0302,y
	LDA #$3D : ORA !facing    : STA $0303,y
	INY #4

	LDA  $00 : 		    STA $0300,y
	LDA  $01 : CLC : ADC #$08 : STA $0301,y
	LDA #$90 :		    STA $0302,y
	LDA #$3D : ORA !facing :    STA $0303,y
	INY #4

	LDA  $00 : 		    STA $0300,y
	LDA  $01 : CLC : ADC #$18 : STA $0301,y
	LDA #$30 : CLC : ADC $0F  : STA $0302,y
	LDA #$3D : ORA !facing :    STA $0303,y
	INY #4

	LDA  $00 : CLC : ADC $0D  : STA $0300,y
	LDA  $01 : CLC : ADC #$08 : STA $0301,y
	LDA #$82 :		    STA $0302,y
	LDA #$3D : ORA !facing :    STA $0303,y
	INY #4
	
	LDA  $00 : CLC : ADC $0D  : STA $0300,y
	LDA  $01 : SEC : SBC #$08 : STA $0301,y
	LDA $0E  : 		    STA $0302,y
	LDA #$3D : ORA !facing :    STA $0303,y

	LDA #$04
	LDY #$02
	JSL $01B7B3

	RTS


}

DashRightPrepGFX:
{
	LDA #$00 : 		    STA $0300,y
	LDA  $01 : 		    STA $0301,y
	LDA #$E2 : 		    STA $0302,y
	LDA #$3D :		    STA $0303,y

	LDA #$00
	LDY #$02
	JSL $01B7B3
	JMP GFXFinish
}

DashLeftPrepGFX:
{
	LDA #$F0 : 		    STA $0300,y
	LDA  $01 : 		    STA $0301,y
	LDA #$E2 : 		    STA $0302,y
	LDA #$7D :		    STA $0303,y

	LDA #$00
	LDY #$02
	JSL $01B7B3
	JMP GFXFinish
}

;DrawLeftArrow:
;{
;	SEP #$20
;	LDA #$00 : 		    STA $0300,y
;	LDA  $01 : 		    STA $0301,y
;	LDA #$E2 : 		    STA $0302,y
;	LDA #$3D :		    STA $0303,y
;
;	LDA #$00
;	LDY #$02
;	JSL $01B7B3
;	JMP GFXFinish
;}

;DrawRightArrow:
;{
;	SEP #$20
;	LDA #$F0 : 		    STA $0300,y
;	LDA  $01 : 		    STA $0301,y
;	LDA #$E2 : 		    STA $0302,y
;	LDA #$7D :		    STA $0303,y
;
;	LDA #$00
;	LDY #$02
;	JSL $01B7B3
;	JMP GFXFinish
;}

DashRightGFX:
{
	LDA !attackTimes
	REP #$20
	BNE +
	LDA #$0070
	BRA ++
	+
	LDA #$0080
	++
	CMP !stateTime
	SEP #$20
	BCS DashRightPrepGFX
	
	JSR DrawSparkles
;	REP #$20
;	LDA !xpos
;	BMI +
;	CMP #$0100
;	BCS DrawRightArrow
;	BRA .isOnScreen
;+
;	CMP #$FFF0
;	BRA DrawLeftArrow
;	
;.isOnScreen
	SEP #$20
	



	LDA $14
	AND #$02
	BEQ +
	LDA #$05
	+
	STA $0F

	LDA !attacked+7
	BNE .drawHelmet
	
	STZ $0E
	LDA $14
	AND #$02
	BEQ +
	LDA #$20
	+
	STA $0E
	
	LDA $00  : SEC : SBC #$10 : STA $0300,y
	LDA $01                   : STA $0301,y
	LDA #$C6 : CLC : ADC $0E  : STA $0302,y
	LDA #$3D                  : STA $0303,y
	INY #4
	
	LDA $00                   : STA $0300,y
	LDA $01                   : STA $0301,y
	LDA #$C8 : CLC : ADC $0E  : STA $0302,y
	LDA #$3D                  : STA $0303,y
	INY #4
	
	BRA +
	
.drawHelmet
	LDA $00  : SEC : SBC #$10 : STA $0300,y
	LDA $01                   : STA $0301,y
	LDA #$84 : CLC : ADC $0F  : STA $0302,y
	LDA #$3D                  : STA $0303,y
	INY #4
	
	LDA $00                   : STA $0300,y
	LDA $01                   : STA $0301,y
	LDA #$86 : CLC : ADC $0F  : STA $0302,y
	LDA #$3D                  : STA $0303,y
	INY #4
+
	
	LDA $00  : SEC : SBC #$18 : STA $0300,y
	LDA $01  : CLC : ADC #$10 : STA $0301,y
	LDA #$A3 : CLC : ADC $0F  : STA $0302,y
	LDA #$3D                  : STA $0303,y
	INY #4
	
	LDA $00  : SEC : SBC #$10 : STA $0300,y
	LDA $01  : CLC : ADC #$10 : STA $0301,y
	LDA #$A4 : CLC : ADC $0F  : STA $0302,y
	LDA #$3D                  : STA $0303,y
	INY #4
	
	LDA $00                   : STA $0300,y
	LDA $01  : CLC : ADC #$10 : STA $0301,y
	LDA #$A6 : CLC : ADC $0F  : STA $0302,y
	LDA #$3D                  : STA $0303,y
	INY #4
	
	LDA $14
	AND #$02
	CLC
	ADC #$C0
	STA $0F
	
	LDA $00  : CLC : ADC #$0C : STA $0300,y
	LDA $01                   : STA $0301,y
	LDA $0F                   : STA $0302,y
	LDA #$3D                  : STA $0303,y
	INY #4
	
	LDA $00  : CLC : ADC #$0C : STA $0300,y
	LDA $01  : CLC : ADC #$10 : STA $0301,y
	LDA $0F                   : STA $0302,y
	LDA #$BD                  : STA $0303,y

	
	LDA #$06
	LDY #$02
	JSL $01B7B3
	JMP GFXFinish
}

DashLeftGFX:
{	
	LDA !attackTimes
	REP #$20
	BNE +
	LDA #$0070
	BRA ++
	+
	LDA #$0080
	++
	CMP !stateTime
	SEP #$20
	BCC +
	JMP DashLeftPrepGFX
	+

	JSR DrawSparkles
	
	LDA $14
	AND #$02
	BEQ +
	LDA #$05
	+
	STA $0F
	
	LDA !attacked+7
	BNE .drawHelmet
	
	STZ $0E
	LDA $14
	AND #$02
	BEQ +
	LDA #$20
	+
	STA $0E
	
	LDA $00  : CLC : ADC #$10 : STA $0300,y
	LDA $01                   : STA $0301,y
	LDA #$C6 : CLC : ADC $0E  : STA $0302,y
	LDA #$7D                  : STA $0303,y
	INY #4
	
	LDA $00                   : STA $0300,y
	LDA $01                   : STA $0301,y
	LDA #$C8 : CLC : ADC $0E  : STA $0302,y
	LDA #$7D                  : STA $0303,y
	INY #4
	
	BRA +
	
.drawHelmet

	LDA $00  : CLC : ADC #$10 : STA $0300,y
	LDA $01                   : STA $0301,y
	LDA #$84 : CLC : ADC $0F  : STA $0302,y
	LDA #$7D                  : STA $0303,y
	INY #4
	
	LDA $00                   : STA $0300,y
	LDA $01                   : STA $0301,y
	LDA #$86                  : STA $0302,y
	LDA #$7D                  : STA $0303,y
	INY #4
	
+	
	
	LDA $00  : CLC : ADC #$18 : STA $0300,y
	LDA $01  : CLC : ADC #$10 : STA $0301,y
	LDA #$A3 : CLC : ADC $0F  : STA $0302,y
	LDA #$7D                  : STA $0303,y
	INY #4
	
	LDA $00  : CLC : ADC #$10 : STA $0300,y
	LDA $01  : CLC : ADC #$10 : STA $0301,y
	LDA #$A4 : CLC : ADC $0F  : STA $0302,y
	LDA #$7D                  : STA $0303,y
	INY #4
	
	LDA $00                   : STA $0300,y
	LDA $01  : CLC : ADC #$10 : STA $0301,y
	LDA #$A6 : CLC : ADC $0F  : STA $0302,y
	LDA #$7D                  : STA $0303,y
	INY #4
	
	LDA $14
	AND #$02
	CLC
	ADC #$C0
	STA $0F
	
	LDA $00  : SEC : SBC #$0C : STA $0300,y
	LDA $01                   : STA $0301,y
	LDA $0F                   : STA $0302,y
	LDA #$7D                  : STA $0303,y
	INY #4
	
	LDA $00  : SEC : SBC #$0C : STA $0300,y
	LDA $01  : CLC : ADC #$10 : STA $0301,y
	LDA $0F                   : STA $0302,y
	LDA #$FD                  : STA $0303,y

	
	LDA #$06
	LDY #$02
	JSL $01B7B3
	JMP GFXFinish
}

SummonGFX:
{
	JMP GFXFinish
}

DrawSmashArrow:
{
	LDA $00  : STA $0300,y
	LDA $01  : STA $0301,y
	LDA #$E0 : STA $0302,y
	LDA #$3D : STA $0303,y
	
	LDA #$00
	LDY #$02
	JSL $01B7B3
	JMP GFXFinish
	
}

SmashDownDrawNothing:
SEP #$20
JMP GFXFinish

SmashDownGFX:
{
	REP #$20
	LDA !stateTime
	CMP #$0010
	BCC SmashDownDrawNothing
	CMP #$0070
	SEP #$20
	BCC DrawSmashArrow
	
	PHY	; Save y; we need it later to make some of the tiles 8x8.
	
	LDA $00                              : STA $0300,y
	LDA $01             : CLC : ADC #$1B : STA $0301,y
	LDA $14  : AND #$02 : CLC : ADC #$60 : STA $0302,y
	LDA #$3D :                           : STA $0303,y
	INY #4
	
	LDA $00                                       : STA $0300,y
	LDA $01  : SEC : SBC #$08                     : STA $0301,y
	LDA $14  : AND #$02 : ASL #4 : CLC : ADC #$0E : STA $0302,y
	LDA #$3D : ORA !facing                        : STA $0303,y
	INY #4
	
	LDA $00                   : STA $0300,y
	LDA $01  : CLC : ADC #$08 : STA $0301,y
	LDA #$4E : STA $0302,y    : STA $0302,y
	LDA #$3D : ORA !facing    : STA $0303,y
	INY #4
	
	LDA $00                   : STA $0300,y
	LDA $01  : CLC : ADC #$10 : STA $0301,y
	LDA #$5E : STA $0302,y    : STA $0302,y
	LDA #$3D : ORA !facing    : STA $0303,y
	INY #4
	
	PHX
	
	LDA !facing
	BEQ +
	LDA #$01
	+
	STA $0F
	TAX
	
	LDA $00  : CLC : ADC FORWX,x : STA $0300,y
	LDA $01  : CLC : ADC #$10    : STA $0301,y
	LDA $14  : AND #$02 : CLC : ADC $0F : CLC : ADC #$6A : STA $0302,y
	LDA #$3D : ORA !facing       : STA $0303,y
	INY #4
	
	LDA $00  : CLC : ADC FOLWX,x : STA $0300,y
	LDA $01  : CLC : ADC #$10    : STA $0301,y
	LDA $14  : AND #$02 : CLC : ADC $0F : CLC : ADC #$6B : STA $0302,y
	LDA #$3D : ORA !facing       : STA $0303,y
	INY #4
	
	LDA $00  : CLC : ADC FORWX,x : STA $0300,y
	LDA $01  : CLC : ADC #$10    : STA $0301,y
	LDA #$2A                     : STA $0302,y
	LDA #$3D : ORA !facing       : STA $0303,y
	
	PLX
	
	INY #4
	LDA #$06
	JSR DrawPlayerOffscreenArrow
	
	LDY #$02
	JSL $01B7B3
	
	PLY
	TYA
	LSR
	LSR
	CLC
	ADC #$04
	TAY				
	LDA $0460,y		;Adjust some of them to be 8x8 instead of 16x16.
	AND #$01
	STA $0460,y
	INY	
	LDA $0460,y
	AND #$01
	STA $0460,y
	INY	
	LDA $0460,y
	AND #$01
	STA $0460,y
	
	JMP GFXFinish
	
}

SmashReturnGFX:
{	

	LDA $00 		  	     : STA $0300,y
	LDA $01 		  	     : STA $0301,y
	LDA #$06 		  	     : STA $0302,y
	LDA #$3D : ORA !facing    	     : STA $0303,y
	INY #4
	
	LDA $00		  		     : STA $0300,y
	LDA $01             : CLC : ADC #$08 : STA $0301,y
	LDA $14  : AND #$02 : CLC : ADC #$16 : STA $0302,y
	LDA #$3D : ORA !facing    	     : STA $0303,y
	INY #4

	LDA $00		  		     : STA $0300,y
	LDA $01             : CLC : ADC #$10 : STA $0301,y
	LDA $14  : AND #$02 : CLC : ADC #$26 : STA $0302,y
	LDA #$3D : ORA !facing    	     : STA $0303,y
	INY #4
	
	LDA $00		  		     : STA $0300,y
	LDA $01             : CLC : ADC #$20 : STA $0301,y
	LDA $14  : AND #$02 : CLC : ADC #$46 : STA $0302,y
	LDA #$3D : ORA !facing    	     : STA $0303,y
	
	INY #4
	LDA #$03
	JSR DrawPlayerOffscreenArrow


	LDY #$02
	JSL $01B7B3		;Draw all tiles
	JMP GFXFinish
}

SmashLookGFX:
{	
	PHX
	LDX #$00
	LDA $14
	CLC
	ADC #$0F
	AND #$3F
	CMP #$1F
	BCS +
	LDX #$02
	+
	STX $0F
	PLX
	
	LDA $14
	AND #$04
	LSR
	STA $0E
	
	LDA !attacked+$D
	BEQ .lookDown
	LDA #$68
	BRA +
.lookDown
	
	LDA $14
	AND #$20
	LSR #4
	BNE .noDrawTop
	PHA
	LDA $00	 		: STA $0300,y
	LDA $01 : SEC : SBC #$08 : STA $0301,y
	LDA #$54 		: STA $0302,y
	LDA #$3D : ORA !facing  : STA $0303,y
	INY : INY : INY : INY
	PLA
.noDrawTop
	CLC
	ADC #$64
+
	STA $05
	
	LDA $00	 		: STA $0300,y
	LDA $01  		: STA $0301,y
	LDA $05 		: STA $0302,y
	LDA #$3D : ORA !facing  : STA $0303,y
	INY : INY : INY : INY
	
	LDA  $00 : 		    STA $0300,y
	LDA  $01 : CLC : ADC #$10 : STA $0301,y
	LDA #$20 : CLC : ADC $0F  : STA $0302,y
	LDA #$3D : ORA !facing :    STA $0303,y
	INY #4

	LDA  $00 : 		    STA $0300,y
	LDA  $01 : CLC : ADC #$18 : STA $0301,y
	LDA #$30 : CLC : ADC $0F  : STA $0302,y
	LDA #$3D : ORA !facing :    STA $0303,y
	
	INY #4
	
	
	
	LDA  $00 :		    STA $0300,y
	LDA  $01 : CLC : ADC #$18 : STA $0301,y
	LDA #$EA : CLC : ADC $0E  : STA $0302,y
	LDA #$3D : ORA !facing :    STA $0303,y
	INY #4
	TYA
	LSR
	LSR
	DEC

	JSR DrawPlayerOffscreenArrow

	LDY #$02
	JSL $01B7B3

	JMP GFXFinish
}

DrawPlayerOffscreenArrow:
{
	PHA
	REP #$20
	LDA $80
	CLC
	ADC #$0020
	SEP #$20
	BPL .doNotDraw
	LDA $7E  : STA $0300,y
	LDA #$02 : STA $0301,y
	LDA #$E0 : STA $0302,y
	
	LDA $0F6B
	CMP #$30
	BCS .drawDownArrow
	LDA #$3D
	BRA +
.drawDownArrow
	LDA #$BD
+
	STA $0303,y

	PLA
	INC
	RTS
	
	.doNotDraw
	PLA
	RTS
}

DefeatGFX:
{
REP #$20		;
LDA !stateTime		;
CMP #$00AA		;
SEP #$20		;
BCS +			;
JMP SmashLookGFX	;
+			;
REP #$20
CMP #$0100
SEP #$20
BCS +
JMP HurtGFX
+
	LDA !facing		; The following is basically a copy of the hurt GFX, but flipped upside-down.
	STA $0F
.goingDown
	PHY
	PHX
	LDA !facing
	LSR #6
	TAX

	LDA  $00 : CLC : ADC FOLWX,x :	STA $0300,y
	LDA  $01 : CLC : ADC #$10    :	STA $0301,y
	LDA #$09 :			STA $0302,y
	LDA #$8D : ORA $0F	     :	STA $0303,y
	INY #4

	LDA  $00 : CLC : ADC FORWX,x :	STA $0300,y
	LDA  $01 : CLC : ADC #$10    :	STA $0301,y
	LDA #$08 :			STA $0302,y
	LDA #$8D : ORA $0F	     :	STA $0303,y
	INY #4
	
	LDA  $00 : 		    STA $0300,y
	LDA  $01 : CLC : ADC #$18 : STA $0301,y
	LDA #$0C : 		    STA $0302,y
	LDA #$8D : ORA $0F	 :  STA $0303,y
	INY #4

	LDA  $00 : 		    STA $0300,y
	LDA  $01 : CLC : ADC #$10 : STA $0301,y
	LDA #$1C : 		    STA $0302,y
	LDA #$8D : ORA $0F	  : STA $0303,y
	INY #4

	LDA $0E
	TAX

	LDA  $00 : 		    STA $0300,y
	LDA  $01 : 		    STA $0301,y
	LDA #$3C : 		    STA $0302,y
	LDA #$8D : ORA $0F	 :    STA $0303,y

	PLX
	LDA #$04
	LDY #$02
	JSL $01B7B3		;Draw all tiles

	PLY
	TYA
	LSR
	LSR			
	TAY				
	LDA $0460,y		;Adjust some of them to be 8x8 instead of 16x16.
	AND #$01
	STA $0460,y
	INY	
	LDA $0460,y
	AND #$01
	STA $0460,y
	

	RTS

}





GFXFinish:
	RTS


GetXFlip:
STZ !facing
REP #$20
LDA !xpos
SEC : SBC $D1
SEP #$20
BCC +
RTS
+
LDA #$40
STA !facing
RTS