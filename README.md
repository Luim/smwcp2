### SMWCP2 Development repo ###

Welcome, this is the git repo that we will use to handle collaboration on the development of SMWCP2. Use the [Wiki](https://bitbucket.org/Luim/smwcp2/wiki/) for info about the different parts of the game, and the [bug tracker](https://bitbucket.org/Luim/smwcp2/issues?status=new&status=open) to see what needs to be done.

### How do I get set up? ###
The only thing you need is git ([Windows download here](https://git-scm.com/downloads))

When installing git, make sure you select "Use from windows command line", otherwise it won't be added to the path/

After installing git, you will need to generate an ssh key. To do so, open a cmd window and type `ssh-keygen`, then enter. Follow the prompts to create your key. It will tell you where the key is output, take note of that location and open that folder. Find the file named `id_rsa.pub` and open that in a text editor.

Now, enter your bitbucket settings and find "SSH keys" on the sidebar. Choose "Add Key", copy the `id_rsa.pub` text from before, and paste it into the box labeled "Key". Also, label the key, so you know where it came from. For instance, if it's on your laptop, name it "laptop". Just enough to differentiate it from your other devices.

Once you have created the ssh key, you need to set up your email/username, like so:

    git config --global user.email email@example.com

and

    git config --global user.name "User Name"
	
Next, you need to fork the repo into your own repo. Press the "+" on the left of the screen and click "fork this repo" to create a fork. From now, all your work will be done on this fork.

Now that that is set up, you need to clone the repo. Open a command window in the location you want the repo, the copy the text from the top of the page (Next to where it says ssh) and run `git clone <repo_location>`, example `git clone https://bitbucket.org/Luim/smwcp2.git`. Make sure you point it to your forked repo, not the main repo, otherwise you will not be allowed to push any changes. That will download all the latest source and put it on your local drive for editing.

### Contribution guidelines ###
Once you have done all that, you are ready to contribute. As of now, you are on the master branch. For source control purposes, you are not allowed to write to the master branch, only project admins are. So, you need to create a branch, then a pull request to merge back into master.

First, open your forked repo, then look for a notification that says "this repo is out of date" or such, and there should be a "sync" button there. If it exists, press that button to update your repo with the latest main repo so yours will be fully up to date.

To create a branch, run `git branch <branch_name>`. Try to give your branch name a meaningful name. For instance, if you are fixing a bug in a sprite, name your branch something like `fix/sprite-name-bug`, e.g. `fix/bute-arrows`. If you are fixing a level, name it something like `fix/level-name`, and if you are adding a new feature, name it something like `feat/new-world-2-boss` or something like that.

After creating a branch, run `git checkout <branch_name>`, and then make your changes as you see fit. Anytime you reach a good stopping point (e.g. if you are fixing a boss, each time you fix one of the broken parts), you can make a commit to save your changes. To make a commit, you must add your files to git. To add a file, run `git add <filepath>`. You can add single files or entire folders. If you want to add every file that you have changed, you can run `git add .`. If you want to look at all files that have changed, you can run `git status`. Only add the files that you want to commit.

To finally do the commit, use `git commit -m <commit message>`. Commit message is a description of what you have changed in this commit. It will help to track what has been done to the game. If one line is not enough, you can omit the `-m <commit message>` bit and it will bring up a text editor (vi by default) and you can add text there. However, vi is hard to use, and if you are reading this description because you don't know how git works, you probably don't know how vi works. If you accidentally get into vi and can't leave, press escape a bunch, then type `:q!` and enter. It will leave.

Once you have done all the commits you want, you can push the branch to the server with `git push origin <branch_name>`. Then, you can [create a pull request](https://confluence.atlassian.com/bitbucket/work-with-pull-requests-223220593.html#Workwithpullrequests-Createapullrequest) to merge your changes with the master repo, and they will be reviewed and merged if it is decided that they should be in the ROM.

Important note: When you create another branch after creating your first one, DO NOT just run `git branch <branch_name>` again. That will create a branch from your current branch, instead of from master. Instead, run `git checkout master` to switch back to master (All of your changes will disappear, but don't worry, they aren't gone, they are just stashed away. Run `git checkout <branch_name>` at any time to get them back), `git pull` to update to the latest master, then run `git branch <other_branch>` to create your new branch and continue on.

Also: It is not guaranteed that the base rom levels will be up to date with the latest levels in the repo. Always do a full level import before doing anything level related.

If these commands appear to be too difficult for you, I have created a few batch files under the "git" directory (not .git) that will automate the process. Just run "branch.bat" when you want to start a new change, "commit.bat" when you want to save something, and "push.bat" when you want to push the branch to the server. These bat commands will not be as full featured as doing it by the command line, but they should get suffice for most purposes.


If you ever need help with bitbucket realated things, they have a pretty good [help page](https://confluence.atlassian.com/bitbucket/use-your-repository-675189337.html) that you can check out.

### Who do I talk to? ###

If you need help with anything regarding contribution, talk with HuFlungDu or Lui37.