!MonitorSpriteCHR = 	$0A24

!CutsceneDBackgroundCHR1 = $01F9
!CutsceneDBackgroundCHR2 = $0203
!CutsceneDBackgroundCHR3 = $0204
!CutsceneDForegroundTilemap = $0A33
!CutsceneDBackgroundTilemap = $0A34

CutsceneDPalette:
incbin CutsceneD.rawpal
	
CSINITD:
	STZ $2121
	%StandardDMA(#$2122, CutsceneDPalette, #512, #$00) 
	
	REP #$30	
	LDA #!DocCrocGeneralExGFX		; |
	LDX #$D000>>1				; | Upload Doc Croc (one slot later than normal!)
	LDY #!CS1DocCrocExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	REP #$30
	LDA #!MonitorSpriteCHR			; \
	LDX #$E000>>1				; | Upload SP2
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	
	LDA #!CutsceneDBackgroundCHR1		; \
	LDX #!layer1ChrAddr+$2000>>1		; | Upload background GFX 3
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CutsceneDBackgroundCHR2		; \
	LDX #!layer1ChrAddr+$3000>>1		; | Upload background GFX 4
	LDY #!CS1BackgroundChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CutsceneDBackgroundCHR3		; \
	LDX #!layer1ChrAddr+$4000>>1		; | Upload background GFX 5
	LDY #!CS1BackgroundChrExGFXSize		; | Overwrites where Doc Croc would be.  Magic is used to fix this.
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CutsceneDForegroundTilemap	; \
	LDX #!layer1MapAddr>>1			; | Upload the foreground tilemap
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CutsceneDBackgroundTilemap	; \
	LDX #!layer2MapAddr>>1			; | Upload the background tilemap
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	SEP #$30
		
	LDA #$05
	STA !backgroundColor
	LDA #$06
	STA !backgroundColor+1
	LDA #$07
	STA !backgroundColor+2
	
	REP #$20
	LDA #$00D8				; \ Layer 1 X
	STA $1A					; /
	LDA #$0080				; \ Layer 1 Y
	STA $1C					; /
	LDA #$0000				; \ Layer 2 X
	STA $1E					; /
	LDA #$0060				; \ Layer 2 Y
	STA $20					; /
	SEP #$20
	
	
	LDA #$00
	STA !currentFont
	
	JSL MarioCreate
	JSL DocCrocCreate
	JSL IntercomCreate
	LDA #$98 : JSL DocCrocSetXPos
	LDA #$00 : JSL MarioSetXPos
	LDA #$55 : JSL MarioSetYPos
	LDA #$53 : JSL MarioSetTargetX

	JSL SetSpeakerToDocCroc
	JSL DocCrocSetToStanding
	
	RTL

	

CSDT1:
db $F4
%VWFASM(MarioSetToVarWalkRight)
db $FF
db $FF
db "There you are. ", !CCButton
%VWFASM(DocCrocSetArmsBehindHead)
db $FF
db "You know, Mario_sometimes, you make it hard for a man to put on a good PR face.", !CCButton, !CCClearText
%VWFASM(SetSpeakerToIntercom)
db "Doctor! "
%VWFASM(MarioSetToFacingLeft)
db "What's " 
%VWFASM(DocCrocSetArmsNormal)
db "the status on Mario?", !CCButton, !CCClearText
%VWFASM(SetSpeakerToDocCroc)
db "He's"
%VWFASM(MarioSetToFacingRight)
db " right here, boss.", !CCButton, !CCClearText
%VWFASM(MarioSetToFacingLeft)
%VWFASM(SetSpeakerToIntercom)
db "Is he dead?", !CCButton, !CCClearText
%VWFASM(SetSpeakerToDocCroc)
%VWFASM(MarioSetToFacingRight)
db "Not yet, boss.", !CCButton, !CCClearText
%VWFASM(SetSpeakerToIntercom)
%VWFASM(MarioSetToFacingLeft)
db "Is he perchance presently engaged in the process of becoming dead?", !CCButton, !CCClearText
%VWFASM(SetSpeakerToDocCroc)
%VWFASM(MarioSetToFacingRight)
db "We're just chatting now, boss. "
%VWFASM(DocCrocSetArmsBehindHead)
db "And I'm not ", !CCSetFont, $02, "that", !CCSetFont, $00, " terrible a conversationalist.", !CCButton, !CCClearText
%VWFASM(DocCrocSetArmsNormal)
%VWFASM(MarioSetToFacingLeft)
%VWFASM(SetSpeakerToIntercom)
db "And do you not think eliminating him would be a more efficient use of time and resources, Doctor?", !CCButton, !CCClearText
%VWFASM(SetSpeakerToDocCroc)
%VWFASM(DocCrocSetArmsBehindHead)
%VWFASM(MarioSetToFacingRight)
db "I've found it's best to leave the eliminating until after the chatting, boss. Doing things in the reverse order tends to make for less interesting discussions.", !CCButton, !CCClearText
%VWFASM(SetSpeakerToIntercom)
%VWFASM(MarioSetToFacingLeft)
%VWFASM(DocCrocSetArmsNormal)
db "Yes, yes, Doctor, your unfailing wit and roguish nonchalance even in a time of crisis no doubt causes the young women to swoon and the young men to seek to emulate you.  Yet may I remind you that this man has already caused our company considerable losses in capital, personnel, and stock value, and that his ultimate goal is nothing less than its complete and utter dismantling?", !CCButton, !CCClearText
db "If you wish to have-", !CCZeroSpace, $F8, "among other things-", !CCZeroSpace, $F8, "any resources left at all with which to continue your experiments, I would strongly advise you to eliminate him ", !CCSetFont, $02, "IMMEDIATELY", !CCSetFont, $00, ". Norveg out.", !CCButton, !CCClearText
%VWFASM(MarioSetToFacingRight)
db $F3
%VWFASM(SetSpeakerToDocCroc)
%VWFASM(DocCrocSetArmsBehindHead)
db "Ah_", $F8, "Well, Mario, you heard the man. ", $F4
%VWFASM(DocCrocSetArmsNormal)
db "Let's-a-go.", !CCButton, !CCClearText



db $E0

