db $42
JMP Return : JMP Return : JMP Return : JMP Start : JMP Start : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return


!Colorful = $00		;set to $00 to make the shatter brown. Otherwise, rainbow colors
;To make the grinder cut swiftly through the blocks, see note at the bottom of this file


Start:			LDA $9E,x	;\
			CMP #$B4	; |vital check. We check our sprite number
			BEQ Exec	;/
Return:			RTL

Exec:			LDA $D8,x	;\Set Y pos low & high
			AND #$F0	; |
			STA $98		; |
			LDA $14D4,x	; |
			STA $99		;/

			LDA $157C,x
			BNE leftshatter

			LDA $E4,x	;\
			AND #$F0	; |
			CLC		; |When sprite direction is right.
			ADC #$10	; |
			STA $9A		;/
			LDA $14E0,x
			ADC #$00
			STA $9B
			BRA Continue

leftshatter:		LDA $E4,x	;\
			AND #$F0	; |When the sprite direction is left
			STA $9A		;/
			LDA $14E0,x
			STA $9B

Continue:		PHB
			LDA #$02
			PHA
			PLB
			LDA #!Colorful
			JSL $028663
			PLB

			LDA #$02
			STA $9C
			JSL $00BEB0

			;LDA $157C,x
			;EOR #$01		;Uncomment these 3 lines to make the grinder
			;STA $157C,x		;cut swiftly through the blocks
			RTL