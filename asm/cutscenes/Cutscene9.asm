

!Cutscene9BackgroundCHR1 = $0277
!Cutscene9BackgroundCHR2 = $0278
!Cutscene9ForegroundTilemap = $0279
!Cutscene9BackgroundTilemap = $027A

Cutscene9Palette:
incbin Cutscene9.rawpal
	
CSINIT9:
	STZ $2121
	%StandardDMA(#$2122, Cutscene9Palette, #512, #$00) 
	
	REP #$20
	LDA #!layer1MapAddr
	STA $2116
	SEP #$20
	
	REP #$30
	LDA #!CS1HindenBirdExGFX		; \
	LDX #$C000>>1|$800			; | Upload Hindenbird GFX (SP2)
	LDY #!CS1DocCrocExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	
	
	LDA #!Cutscene9BackgroundCHR1		; \
	LDX #!layer1ChrAddr+$2000>>1		; | Upload background GFX 3
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!Cutscene9BackgroundCHR2		; \
	LDX #!layer1ChrAddr+$3000>>1		; | Upload background GFX 4
	LDY #!CS1BackgroundChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!Cutscene9ForegroundTilemap	; \
	LDX #!layer1MapAddr>>1			; | Upload the foreground tilemap
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!Cutscene9BackgroundTilemap	; \
	LDX #!layer2MapAddr>>1			; | Upload the background tilemap
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	REP #$20
	LDA #$0000				; \ Layer 1 X
	STA $1A					; /
	LDA #$0004				; \ Layer 1 Y
	STA $1C					; /
	LDA #$0000				; \ Layer 2 X
	STA $1E					; /
	LDA #$0070				; \ Layer 2 Y
	STA $20					; /
	SEP #$20
	
	
	
	
	SEP #$30
	
	LDA #$03			; \
	STA !backgroundColor		; |
	LDA #$02			; |
	STA !backgroundColor+1		; |
	LDA #$01			; |
	STA !backgroundColor+2		; /
	
	LDA #$00
	STA !currentFont
	
	JSL HindenbirdCreate
	JSL MarioCreate
	LDA #$00 : JSL MarioSetXPos
	LDA #$55 : JSL MarioSetYPos
	LDA #$58 : JSL MarioSetTargetX
	
	LDA #$94 : JSL HindenbirdSetXPosition
	LDA #$C8 : JSL HindenbirdSetYPosition
	
	
	JSL SetSpeakerToHindenbird
	
	RTL


CS9T1:
db $FF
%VWFASM(MarioSetToVarWalkRight)
db $FF
db $FF
db $FF
db $FF
%VWFASM(MarioSetToFacingLeft)
db $FF
db $F5
%VWFASM(MarioSetToFacingRight)
db $FF
db $FD
%VWFASM(MarioSetToFacingLeft)
db $FF
db $FF
db $FF
db "Admiring our proud fleet? "
%VWFASM(MarioSetToFacingRight)
db $F4
db "A fine assemblage of crafts_", $EE, "oh, but this is just the beginning. "
%VWFASMArg(HindenbirdFloatDown, #$20)
db $EF, $EB
db "Most of our security duties are assigned out to independent contractors--", $EE, "for now, at least. "
db "I never wanted to be a part of it, but it won't matter--", $EE, "soon, very soon_", $EE, "Norveg Industries will have its own, 100% private air force. "
%VWFASM(HindenbirdSetToFlyingOff)
db "Huah!", $EF, $EB
db "Control of the skies is far too essential "
%VWFASM(HindenbirdSetToFloating)
db "to leave in the hands to outside forces. "
db "When you're master of the air, you're master of the world. "
db "When you control the airspace, you control the fastest and most direct supply routes. ", $EF, $EB
db "You've got an eye on your competitors at all times; there's nothing they can truly hide from you, no matter how they scurry around inside of their pathetic little anthills. "
db "And if you don't like what they're doing_", $EF, $EB
db "You can ", $EA, $02, "bust ", $F5, "their ", $F5, !CCSetFont, $02, "bunkers", !CCSetFont, $00, $EA, $00, "!", $EF, $EB

db "And make no mistake, I've been watching you and your stache ", $EA, $02, "very", $EA, $00, " closely. "
db "And you're good, I'll give you that. But you've got one major weakness. You're a little too_", $EF, $EB
db "GROUNDED.", $EF, $EB

db "Heh. But words are cheap and we're both men of action; let's leave the blathering to the PR men and carnival barkers. "
db "As for us_", $EE, "maybe I ought to give you a ", $EA, $02, "hands-", $EE, $EA, $00, "on demonstration of the full tactical advantage of a superior air presence. "
db "I'm sure you'll find it a_", $EE, "heart-", $EE, "stopping experience. ", $EF, $EB
%VWFASM(HindenbirdSetToFlyingOff)
db "Geronimooooo! "

db $FF, $FF, $EF, $FF, $E0
